﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Dashboard.API
{
    public interface IDelegationToken
    {
        Task<string> GetDelegation(HttpContext context);
    }
}
