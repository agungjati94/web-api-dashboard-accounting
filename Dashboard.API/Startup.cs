﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Dashboard.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var config = new ConfigurationBuilder()
                                                                                .AddJsonFile(@"/home/development/appsettings.json") // prod
                                                                                                                                    // .AddJsonFile("appsettings.json")  //dev
                            .Build();

            var connectionString = config["ConnectionStrings"];
            var auth = config["AuthEndPoint"];

            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer(connectionString);
            services.AddSingleton(new SofiContext(optionsBuilder.Options));
            services.AddSingleton<SofiDBContextHolder>();

            services.AddTransient<DashboardRepository>();
            services.AddTransient<CardProcurementCommand>();
            services.AddTransient<SalesRepository>();

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddCors();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = auth;
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "api";
                    options.EnableCaching = true;
                    options.CacheDuration = TimeSpan.FromMinutes(60);
                    options.SaveToken = true;
                });

            services.AddTransient<IDelegationToken, DelegationToken>();
            services.AddSingleton<HttpClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //app.UseWelcomePage();

            app.UseValidateDelegationToken();

            var options = new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                RequireHeaderSymmetry = false
            };
            options.KnownNetworks.Clear();
            options.KnownProxies.Clear();

            app.UseForwardedHeaders(options);

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials());

            app.UseExceptionHandler(
                builder =>
                {
                    builder.Run(
                    async context =>
                    {
                        context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";
                        var ex = context.Features.Get<IExceptionHandlerFeature>();
                        if (ex != null)
                        {
                            var err = JsonConvert.SerializeObject(new Error()
                            {
                                Stacktrace = ex.Error.StackTrace,
                                Message = ex.Error.Message

                            });
                            await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(err), 0, err.Length).ConfigureAwait(false);
                        }
                    });
                }

            );
            app.UseAuthentication();
            app.UseMvc();
        }

        private class Error
        {
            public Error()
            {
            }
            public string Stacktrace { get; set; }
            public string Message { get; set; }
        }
    }
}
