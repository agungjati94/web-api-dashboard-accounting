﻿using Microsoft.EntityFrameworkCore;
using Dashboard.Database;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Dashboard.API
{
    public class SofiDBContextHolder
    {
        private SofiContext m_DefaultContext;

        public SofiDBContextHolder(SofiContext _context)
        {
            m_DefaultContext = _context;
        }

        public SofiContext CreateContext(string username)
        {
            DbContextOptionsBuilder<SofiContext> options = CreateDBOptionsBuilder(username);
            var privateContext = new SofiContext(options.Options);
            return privateContext;
        }

       
        private DbContextOptionsBuilder<SofiContext> CreateDBOptionsBuilder(string username)
        {
            username = username == "dbo" || username == "INFORSYS" ? "sa" : username;
            var serverName = m_DefaultContext.Database.GetDbConnection().DataSource;
            var password = m_DefaultContext.Database.GetDbConnection().ConnectionString
                            .Split(";")
                            .FirstOrDefault(x => x.Contains("Password"));
            password = (password != null) ? password.Split("=")[1] : "";
            var databaseName = m_DefaultContext.Database.GetDbConnection().Database;
            var options = new DbContextOptionsBuilder<SofiContext>();
            options.UseSqlServer($"Server={serverName};Database={databaseName};User Id={username}; Password={password};", builder =>
            {
                builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(0.5), null);
            });
            
            return options;
        }
    }
}
