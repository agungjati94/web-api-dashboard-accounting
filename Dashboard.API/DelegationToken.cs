﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Dashboard.API
{
    public class DelegationToken : IDelegationToken
    {
        private string GetTokenEndPoint(string authToken)
        {
            //return this._configuration.GetConnectionString(conString);

            var config = new ConfigurationBuilder()
                                                                                                                                .AddJsonFile(@"/home/development/appsettings.json") // prod
                                                                                                                                                                                    //  .AddJsonFile("appsettings.json")  //dev
                        .Build();

            return $"{config[authToken]}/connect/token";
        }

        public async Task<string> GetDelegation(HttpContext context)
        {
            var authHeader = context.Request.Headers["Authorization"].ToString();

            // assign to payload
            var payload = new { token = authHeader.Replace("Bearer ", "") };

            // request new token from gateway to auth server
            var tokenclient = new TokenClient(GetTokenEndPoint("AuthEndPoint"), "sofi_api", "secret");

            // new token from auth server
            var tokenResult = await tokenclient.RequestCustomGrantAsync("delegation", "api", payload);
            return tokenResult.AccessToken;
        }
    }

    public class ValidateDelegationToken
    {
        private readonly RequestDelegate _next;
        private IDelegationToken _delegationToken;
        private HttpClient _httpClient;

        public ValidateDelegationToken(RequestDelegate next, IDelegationToken delegationToken, HttpClient httpClient)
        {
            _next = next;
            _delegationToken = delegationToken;
            _httpClient = httpClient;
        }


        public async Task Invoke(HttpContext context)
        {
            var delegateToken = await _delegationToken.GetDelegation(context).ConfigureAwait(false);
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", delegateToken);

            await _next.Invoke(context);
        }
    }

    public static class UseValidateDelegationTokenExtention
    {
        public static IApplicationBuilder UseValidateDelegationToken(this IApplicationBuilder app)
        {
            app.UseMiddleware<ValidateDelegationToken>();
            return app;
        }
    }
}
