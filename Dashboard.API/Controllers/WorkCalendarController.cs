﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dashboard.Database;
using Dashboard.Repository;

namespace Dashboard.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkCalendarController : ControllerBase
    {
        private SofiDBContextHolder m_privateContext;

        public WorkCalendarController(SofiDBContextHolder privateContext)
        {
            m_privateContext = privateContext;
        }

        [HttpGet("GetDue")]
        public async Task<IActionResult> GetDueMPR(WorkCalendarEnum[] type, DateTime date)
        {
            var repo = CreateWorkCalendarRepository();
            var result = await repo.GetDue(type, date).ConfigureAwait(false);
            return Ok(result);
        }

        private WorkCalendarRepository CreateWorkCalendarRepository()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new WorkCalendarRepository(privateContext);
        }
        private SofiContext FindPrivateDbContext(string username)
        {
            return m_privateContext.CreateContext(username);
        }
    }
}