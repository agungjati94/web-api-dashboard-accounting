﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dashboard.Database;
using Dashboard.Repository;

namespace Dashboard.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcurementController : ControllerBase
    {
        private SofiDBContextHolder m_privateContext;

        public ProcurementController(SofiDBContextHolder privateContext)
        {
            m_privateContext = privateContext;
        }

        [HttpGet("CountMPRNeeedtoConfirm")]
        public async Task<IActionResult> GetMPRneedtoconfirm()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountMPRNeeedtoConfirm().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountMPRNeeedtoRunning")]
        public async Task<IActionResult> CountMPRNeeedtoRunning()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountMPRNeeedtoRunning().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("CountMPROutstandingAgainstPO")]
        public async Task<IActionResult> CountMPROutstandingAgainstPO()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountMPROutstandingAgainstPO().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("CountMPROutstandingAgainstGRN")]
        public async Task<IActionResult> CountMPROutstandingAgainstGRN()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountMPROutstandingAgainstGRN().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("CountPONeeedtoConfirm")]
        public async Task<IActionResult> CountPONeeedtoConfirm()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountPONeeedtoConfirm().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountPONeeedtoRunning")]
        public async Task<IActionResult> CountPONeeedtoRunning()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountPONeeedtoRunning().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountPOOutstandingAgainstGRN")]
        public async Task<IActionResult> CountPOOutstandingAgainstGRN()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountPOOutstandingAgainstGRN().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountPOOutstandingAgainstSI")]
        public async Task<IActionResult> CountPOOutstandingAgainstSI()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountPOOutstandingAgainstSI().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetProcurementDetail")]
        public async Task<IActionResult> GetProcurementDetail(ProcurementEnum type)
        {
            var repo = CreateCountReportCommand();
            var result = await repo.GetProcurementDetail(type).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpPost("SaveFilterProcurement")]
        public async Task<IActionResult> SaveFilterProcurement([FromBody] ProcurementParam param)
        {
            var repo = CreateSaveProcurementCommand();
             await repo.SaveFilterProcurement(param).ConfigureAwait(false);
            return Ok();
        }

        [HttpGet("PONumber")]
        public async Task<IActionResult> GetPONumber()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetPONumber().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("MPRNumber")]
        public async Task<IActionResult> GetMPRNumber()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetMPRNumber().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("OpenPOValue")]
        public async Task<IActionResult> GetOpenPOValue()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetOpenPOValue().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("NumberOfSupplier")]
        public async Task<IActionResult> GetNumberOfSupplier()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartOfNumberSupplier().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("TotalSupplier")]
        public async Task<IActionResult> GetTotalSupplier()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartTotalSupplier().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetChartDetail")]
        public async Task<IActionResult> GetGetChartDetail(ProcurementEnum type, string month)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartDetail(type, month).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetChartDetailOpenPOValue")]
        public async Task<IActionResult> GetChartDetailOpenPOValue(ProcurementEnum type, string month, short currencyId)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartDetailOpenPOValue(month, currencyId).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetChartDetailNumberOfActiveSupplier")]
        public async Task<IActionResult> GetChartDetailNumberOfActiveSupplier(string month, long vendorId)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartDetailNumberOfActiveSupplier(month, vendorId).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetDetailSupplierByCountry")]
        public async Task<IActionResult> GetDetailSupplierByCountry(string countryCd)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetDetailTotalSupplierByCountry(countryCd).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetDetailTotalSupplierPOByCountryVendor")]
        public async Task<IActionResult> GetDetailTotalSupplierPOByCountryVendor(long vendorId, int page, int limit)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetDetailTotalSupplierPOByCountryVendor(vendorId, page, limit).ConfigureAwait(false);
            return Ok(result);
        }

        private ChartProcurementRepository CreateChartRepository()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new ChartProcurementRepository(privateContext);
        }
        private CardProcurementCommand CreateCountReportCommand()
        {

            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new CardProcurementCommand(privateContext);
        }
        private SaveProcurementCommand CreateSaveProcurementCommand()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new SaveProcurementCommand(privateContext);
        }
        private SofiContext FindPrivateDbContext(string username)
        {
            return m_privateContext.CreateContext(username);
        }
    }
}