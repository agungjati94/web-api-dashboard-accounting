﻿using Microsoft.AspNetCore.Mvc;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockMonitoringController : ControllerBase
    {
        private SofiDBContextHolder m_privateContext;

        public StockMonitoringController(SofiDBContextHolder privateContext)
        {
            m_privateContext = privateContext;
        }

        [HttpPost("GetStock")]
        public async Task<IActionResult> GetStock([FromBody]StockMonitoringParam filter)
        {
            var repo = CreateStockMonitoringRepository();
            var result = await repo.GetReportStockMonitoring(filter).ConfigureAwait(false);
            return Ok(result);
        }

        private StockMonitoringRepository CreateStockMonitoringRepository()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new StockMonitoringRepository(privateContext);
        }
        private SofiContext FindPrivateDbContext(string username)
        {
            return m_privateContext.CreateContext(username);
        }
    }
}