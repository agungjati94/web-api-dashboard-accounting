﻿using Microsoft.AspNetCore.Mvc;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private SofiDBContextHolder m_privateContext;

        public DashboardController(SofiDBContextHolder privateContext)
        {
            m_privateContext = privateContext;
        }

        [HttpGet("dashboardprocurement")]
        public async Task<IActionResult> GetSetupProcurement(ProcurementEnum param)
        {
            var repo = CreateDashboardRepository();
            var result = await repo.GetSetupProcurement(param).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("dashboardsales")]
        public async Task<IActionResult> GetSetupSales(SalesEnum param)
        {
            var repo = CreateDashboardRepository();
            var result = await repo.GetSetupSales(param).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("test")]
        public IActionResult GetTest()
        {
            return Ok("Horee");
        }

        private DashboardRepository CreateDashboardRepository()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            var mprRepo = new DashboardRepository(privateContext);
            return mprRepo;
        }
        private SofiContext FindPrivateDbContext(string username)
        {
            return m_privateContext.CreateContext(username);
        }
    }
}