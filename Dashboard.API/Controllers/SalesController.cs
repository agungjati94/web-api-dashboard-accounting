﻿using Microsoft.AspNetCore.Mvc;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private SofiDBContextHolder m_privateContext;

        public SalesController(SofiDBContextHolder privateContext)
        {
            m_privateContext = privateContext;
        }

        [HttpGet("CountSQNeeedtoConfirm")]
        public async Task<IActionResult> GetSQNeedToConfirm()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSQNeedToConfirm().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountSQNeeedtoRunning")]
        public async Task<IActionResult> CountSQNeedToRunning()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSQNeedToRunning().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("CountSQOutstandingAgainstSO")]
        public async Task<IActionResult> CountSQOutstandingAgainstSO()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSQOutstandingAgainstSO().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("CountSQOutstandingAgainstDO")]
        public async Task<IActionResult> CountSQOutstandingAgainstDO()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSQOutstandingAgainstDO().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("CountSONeeedtoConfirm")]
        public async Task<IActionResult> CountSONeeedtoConfirm()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSONeedToConfirm().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountSONeeedtoRunning")]
        public async Task<IActionResult> CountPONeeedtoRunning()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSONeedToRunning().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountSOOutstandingAgainstDO")]
        public async Task<IActionResult> CountSOOutstandingAgainstDO()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSOOutstandingAgainstDO().ConfigureAwait(false);
            return Ok(result);
        }
        [HttpGet("CountSOOutstandingAgainstInvoice")]
        public async Task<IActionResult> CountSOOutstandingAgainstInvoice()
        {
            var repo = CreateCountReportCommand();
            var result = await repo.CountSOOutstandingAgainstInvoice().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetSalesDetail")]
        public async Task<IActionResult> GetSalesDetail(SalesEnum type)
        {
            var repo = CreateCountReportCommand();
            var result = await repo.GetSalesDetail(type).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpPost("SaveFilterSales")]
        public async Task<IActionResult> SaveFilterSales([FromBody] SalesParam param)
        {
            var repo = CreateSaveProcurementCommand();
             await repo.SaveFilterSales(param).ConfigureAwait(false);
            return Ok();
        }

        [HttpGet("SONumber")]
        public async Task<IActionResult> GetSONumber()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetSONumber().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("SQNumber")]
        public async Task<IActionResult> GetSQNumber()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetSQNumber().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("OpenSOValue")]
        public async Task<IActionResult> GetOpenSOValue()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetOpenSOValue().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("NumberOfCustomer")]
        public async Task<IActionResult> GetChartOfNumberCustomer()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartOfNumberCustomer().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("TotalCustomer")]
        public async Task<IActionResult> GetChartTotalCustomer()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartTotalCustomer().ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetChartDetail")]
        public async Task<IActionResult> GetChartDetail(SalesEnum type, string month)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartDetail(type, month).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetChartDetailOpenSOValue")]
        public async Task<IActionResult> GetChartDetailOpenPOValue(SalesEnum type, string month, short currencyId)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartDetailOpenSOValue(type, month, currencyId).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetChartDetailNumberOfActiveCustomer")]
        public async Task<IActionResult> GetChartDetailNumberOfActiveCustomer(string month, long vendorId)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartDetailNumberOfActiveCustomer(month, vendorId).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetDetailCustomerByCountry")]
        public async Task<IActionResult> GetDetailCustomerByCountry(string countryCd)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetDetailTotalCustomerByCountry(countryCd).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("GetDetailTotalCustomerSOByCountryVendor")]
        public async Task<IActionResult> GetDetailTotalCustomerSOByCountryVendor(long vendorId, int page, int limit)
        {
            var repo = CreateChartRepository();
            var result = await repo.GetDetailTransactionSOVendor(vendorId, page, limit).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("SalesSummary")]
        public async Task<IActionResult> GetSalesSummary()
        {
            var repo = CreateChartRepository();
            var result = await repo.GetChartSalesSummary().ConfigureAwait(false);
            return Ok(result);
        }

        private ChartSalesRepository CreateChartRepository()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new ChartSalesRepository(privateContext);
        }
        private SalesRepository CreateCountReportCommand()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new SalesRepository(privateContext);
        }
        private SaveProcurementCommand CreateSaveProcurementCommand()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "username");
            if (user == null) throw new UnauthorizedAccessException("Unauthorized access.");
            var privateContext = FindPrivateDbContext(user.Value);
            return new SaveProcurementCommand(privateContext);
        }
        private SofiContext FindPrivateDbContext(string username)
        {
            return m_privateContext.CreateContext(username);
        }
    }
}