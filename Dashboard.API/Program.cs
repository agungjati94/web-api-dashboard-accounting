﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Dashboard.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
              WebHost.CreateDefaultBuilder(args)
                   .UseStartup<Startup>()
                  .UseUrls("http://localhost:50801")
                  .UseStartup<Startup>();
    }
}
