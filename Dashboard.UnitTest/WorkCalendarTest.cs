﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
    public class WorkCalendarTest
    {
        [TestMethod]
        public async Task GetDueMPR()
        {
            var dueDate = DateTime.Today;
            var repo = new WorkCalendarRepository(new SofiContext());
            var result = await repo.GetDue(new WorkCalendarEnum[] { WorkCalendarEnum.DuePO }, dueDate).ConfigureAwait(false);
        }
    }
}
