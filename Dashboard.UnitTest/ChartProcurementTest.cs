﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
    public class ChartProcurementTest
    {
        [TestMethod]
        public void CreatePeriods()
        {
            var today = DateTime.Today;
            var previous6Month = today.AddMonths(-6);

            var periods = Enumerable.Range(0, 6)
                        .Select(s => today.AddMonths(-s).ToString("MMMM")).ToList();

        }
        [TestMethod]
        public async Task ChartPONumberTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer("Server=167.71.220.1;Database=SOFI_ERP_021019;User Id=sa;Password=agungjati;");
            var repo = new ChartProcurementRepository(new SofiContext(optionsBuilder.Options));
            var result = await repo.GetPONumber().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartMPRNumberTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer("Server=167.71.220.1;Database=MADEIRA_DEMO;User Id=sa; Password=agungjati;");
            var repo = new ChartProcurementRepository(new SofiContext(optionsBuilder.Options));
            var result = await repo.GetMPRNumber().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartOpenPOValue()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer("Server=167.71.220.1;Database=SOFI_ERP_021019;User Id=sa;Password=agungjati;");
            var repo = new ChartProcurementRepository(new SofiContext(optionsBuilder.Options));
            var result = await repo.GetOpenPOValue().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartOfNumberSuppliers()
        {
            var repo = new ChartProcurementRepository(new SofiContext());
            var result = await repo.GetChartOfNumberSupplier().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartTotalSupplier()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            //optionsBuilder.UseSqlServer("Server=128.199.215.86;Database=SOFIXPBE_RAJAWALI;User Id=sa;Password=agungjati;");
            optionsBuilder.UseSqlServer("Server=167.99.64.25;Database=MADEIRA;User Id=sa;Password=agungjati;");
            
            var repo = new ChartProcurementRepository(new SofiContext(optionsBuilder.Options));
            var result = await repo.GetChartTotalSupplier().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ProcurementDetail()
        {
            var repo = new ChartProcurementRepository(new SofiContext());
            var result = await repo.GetChartDetail(ProcurementEnum.OutstandingMPRAgainstGRN, "").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task NumberofActiveSupplierDetail()
        {
            var vendorId = 273;
            var repo = new ChartProcurementRepository(new SofiContext());
            var result = await repo.GetChartDetailNumberOfActiveSupplier("January", vendorId).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetDetailTotalSupplierPOByCountryVendor()
        {
            var vendorId = 174;
            var repo = new ChartProcurementRepository(new SofiContext());
            var result = await repo.GetDetailTotalSupplierPOByCountryVendor(vendorId, 3, 20).ConfigureAwait(false);
        }

        [TestMethod]
       public async Task GetDetailSupplier()
        {
            var countryId = "";
            var repo = new ChartProcurementRepository(new SofiContext());
            var result = await repo.GetDetailTotalSupplierByCountry(countryId).ConfigureAwait(false);
        }
    }
}
