﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
    public class SalesTest
    {
        private SofiContext context;

        [TestInitialize]
        public void Init()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer("Server=167.99.64.25;Database=MADEIRA;User Id=sa; Password=agungjati;");
            this.context = new SofiContext(optionsBuilder.Options);
        }

        [TestMethod]
        public async Task SQNeedToConfirm()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSQNeedToConfirm().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SQNeedToRunning()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSQNeedToRunning().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SONeedToConfirm()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSONeedToConfirm().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SONeedToRunning()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSONeedToRunning().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountSQOutstandingAgainstSO()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSQOutstandingAgainstSO().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountSQOutstandingAgainstDO()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSQOutstandingAgainstDO().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountSOOutstandingAgainstDO()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSOOutstandingAgainstDO().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountSOOutstandingAgainstInvoice()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.CountSOOutstandingAgainstInvoice().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartSQNumberTest()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetSQNumber().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartSONumberTest()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetSONumber().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartOpenPOValue()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetOpenSOValue().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartOfNumberCustomer()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetChartOfNumberCustomer().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ChartTotalCustomer()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetChartTotalCustomer().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SalesCardDetail()
        {
            var repo = new SalesRepository(this.context);
            var result = await repo.GetSalesDetail(SalesEnum.OutstandingSOAgainstDO).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetChartDetail()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetChartDetail(SalesEnum.NumberofSO, "October").ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetChartDetailOpenSOValue()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetChartDetailOpenSOValue(SalesEnum.OpenSOValue, "October", 3).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetChartSalesSummary()
        {
            var repo = new ChartSalesRepository(this.context);
            var result = await repo.GetChartSalesSummary().ConfigureAwait(false);
        }




    }
}
