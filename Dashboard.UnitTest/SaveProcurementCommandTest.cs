﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
    public class SaveProcurementCommandTest
    {
        private SofiContext context = new SofiContext();

        [TestMethod]
        public async Task SaveMPRneedtoConfirmTest()
        {
            var param = new ProcurementParam { Type = ProcurementEnum.MPRNeedToConfirm };
            var repo = new SaveProcurementCommand(context);
            await repo.SaveFilterProcurement(param).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SaveMPRneedtoRunningTest()
        {
            var param = new ProcurementParam { Type = ProcurementEnum.MPRNeedToRunning };
            var repo = new SaveProcurementCommand(context);
            await repo.SaveFilterProcurement(param).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SavePOneedtoConfirmTest()
        {
            var param = new ProcurementParam { Type = ProcurementEnum.PONeedtoConfirm, DivisionId = 2 };
            var repo = new SaveProcurementCommand(context);
            await repo.SaveFilterProcurement(param).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SavePOneedtoRunningTest()
        {
            var param = new ProcurementParam { Type = ProcurementEnum.PONeedtoRunning, DivisionId = 2 };
            var repo = new SaveProcurementCommand(context);
            await repo.SaveFilterProcurement(param).ConfigureAwait(false);
        }
    }
}
