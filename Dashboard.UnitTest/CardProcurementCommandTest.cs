﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
     public class CountReportCommandTest
    {
        private SofiContext context;

        [TestInitialize]
        public void Init()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer("Server=128.199.215.86;Database=SOFIXPBE_RAJAWALI;User Id=sa;Password=agungjati;");
            // optionsBuilder.UseSqlServer("Server=167.71.220.1;Database=SOFI_ERP_021019;User Id=sa;Password=agungjati;");
            //optionsBuilder.UseSqlServer("Server=167.99.64.25;Database=MADEIRA;User Id=sa; Password=agungjati;");
            this.context = new SofiContext(optionsBuilder.Options);
        }

        [TestMethod]
        public async Task CountMPRneedtoConfirmTest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountMPRNeeedtoConfirm().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountMPRneedtoRunningTest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountMPRNeeedtoRunning().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountMPROutstandingAgainstPOTest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountMPROutstandingAgainstPO().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountMPROutstandingAgainstGRNTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer("Server=128.199.215.86;Database=SOFIXPBE_RAJAWALI;User Id=sa;Password=agungjati;");
            //optionsBuilder.UseSqlServer("Server=167.99.64.25;Database=MADEIRA;User Id=sa;Password=agungjati;");
            var repo = new CardProcurementCommand(new SofiContext(optionsBuilder.Options));
            var result = await repo.CountMPROutstandingAgainstGRN().ConfigureAwait(false);
        }


        [TestMethod]
        public async Task CountPOneedtoConfirmTest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountPONeeedtoConfirm().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountPOneedtoRunningTest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountPONeeedtoRunning().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountPOOutstandingAgainstGRNTest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountPOOutstandingAgainstGRN().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task CountPOOutstandingAgainstSITest()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.CountPOOutstandingAgainstSI().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task ProcurementDetail()
        {
            var repo = new CardProcurementCommand(context);
            var result = await repo.GetProcurementDetail(ProcurementEnum.OutstandingPOAgainstGRN).ConfigureAwait(false);
        }
    }
}
