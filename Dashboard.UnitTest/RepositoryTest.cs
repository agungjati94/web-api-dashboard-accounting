using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
    public class RepositoryTest
    {
        private SofiContext context = new SofiContext();
        [TestMethod]
        public void ConnectionDatabaseTest()
        {
            SofiContext context = new SofiContext();
        }

        [TestMethod]
        public void InitializeRepositoryTest()
        {
            SofiContext context = new SofiContext();
            var repo = new DashboardRepository(context);
        }

        [TestMethod]
        public async Task GetDivision()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetDivision().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetDepartment()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetDepartment().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetProject()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetProject().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetRequestor()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetRequestor().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task getSetupTest()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetSetupProcurement(ProcurementEnum.MPRNeedToConfirm).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task getPOType()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetPOType().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetPartGroup()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetPartGroup().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetPartCat()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetPartCat().ConfigureAwait(false);
        }

        [TestMethod]
        public async Task GetSupplier()
        {
            var repo = new DashboardRepository(context);
            var result = await repo.GetSupplier().ConfigureAwait(false);
        }

    }
}
