﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Database;
using Dashboard.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.UnitTest
{
    [TestClass]
    public class StockMonitoringTest
    {
        private SofiContext _sofiContext;
        [TestInitialize]
        public void TestInit()
        {
            var config = new ConfigurationBuilder()
                           .AddJsonFile("appsettings.json") 
                           .Build();
            var connectionString = "Server=167.99.64.25;Database=Madeira;User Id=sa; Password=agungjati;";// config["ConnectionStrings"];

            var optionsBuilder = new DbContextOptionsBuilder<SofiContext>();
            optionsBuilder.UseSqlServer(connectionString);

            _sofiContext = new SofiContext(optionsBuilder.Options);
        }

        [TestMethod]
        public async Task GetDataStock()
        {
            var repo = new StockMonitoringRepository(_sofiContext);
            var filter = new StockMonitoringParam
            {
                PartCD = null,
                DivisionCD = null,
                DepartmentCD = null,
                ProjectCD = null,
                WarehouseCD = null,
                GroupingCD = null,
                PartCat1CD = null,
                PartCat2CD = null,
                PartCat3CD = null,
                VendorCD = null,
                Priority = null,
                TransDate = null,
                Page =1,
                Limit = 10
            };

            var result = await repo.GetReportStockMonitoring(filter).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task DisposeContext()
        {
           
                var repo = new StockMonitoringRepository(_sofiContext);
                var filter = new StockMonitoringParam
                {
                    PartCD = null,
                    DivisionCD = null,
                    DepartmentCD = null,
                    ProjectCD = null,
                    WarehouseCD = null,
                    GroupingCD = null,
                    PartCat1CD = null,
                    PartCat2CD = null,
                    PartCat3CD = null,
                    VendorCD = null,
                    Priority = null,
                    TransDate = null,
                    Page = 1,
                    Limit = 10
                };

                var result = await repo.GetReportStockMonitoring(filter).ConfigureAwait(false);
           
        }
    }
}
