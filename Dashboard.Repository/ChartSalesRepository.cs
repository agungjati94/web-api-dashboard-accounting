﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Dashboard.Database;

namespace Dashboard.Repository
{
    public class ChartSalesRepository
    {
        private SofiContext _context;
        private IEnumerable<string> periods;
        public ChartSalesRepository(SofiContext context)
        {
            _context = context;
            var today = DateTime.Today;
            this.periods = Enumerable.Range(0, 6)
                        .Select(s => today.AddMonths(-s).ToString("MMMM"));

           
        }

        public async Task<object> GetSQNumber()
        {
            using (_context)
            {
                var listMonth = await this.CreateQueryChartSQ(SalesEnum.NumberofSQ).ConfigureAwait(false);
                var queryResult = from queryRes in listMonth
                                  group queryRes by queryRes.SqtDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().SqtDate.ToString("MMMM"),
                                      Value = g.Count()
                                  };

                var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsSQFiltered(SalesEnum.NumberofSQ).ConfigureAwait(false);
                var list = periods.Select(p =>
                {
                    var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                    return new { Name = p, Value = value };
                }).ToList();

                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        private async Task<IQueryable<TblSalesQuo>> CreateQueryChartSQ(SalesEnum type)
        {
            var today = DateTime.Today;
            var previous6Month = today.AddMonths(-6);
            var query = from sq in _context.TblSalesQuo
                        where sq.Deleted == false && sq.SqtDate >= previous6Month && sq.SqtDate <= today && sq.StatusId == (short)StatusEnum.Running
                        select sq;

            return query;
        }

        private async Task<bool> IsSQFiltered(SalesEnum type)
        {
            var queryFil = from setup in _context.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null || setup.PotypeId != null || setup.CurrencyId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }



        public async Task<object> GetSONumber()
        {
            using (_context)
            {
                var listMonth = await this.CreateQueryChartSO(SalesEnum.NumberofSO).ConfigureAwait(false);
                var queryResult = from queryRes in listMonth
                                  group queryRes by queryRes.SoDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().SoDate.ToString("MMMM"),
                                      Value = g.Count()
                                  };

                var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsSOFiltered(SalesEnum.NumberofSO).ConfigureAwait(false);
                var list = periods.Select(p =>
                {
                    var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                    return new { Name = p, Value = value };
                }).ToList();

                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        public async Task<object> GetChartTotalCustomer()
        {
            using (_context)
            {
                var vndTypeIds = new[] { 0, 1 };
                var query = from supplier in _context.TblVendor
                            join country in _context.TblCountry on supplier.CountryId equals country.CountryId
                            where supplier.Deleted == false && country.Deleted == false && vndTypeIds.Contains(supplier.VndTypeId)
                            select new
                            {
                                supplier.VendorId,
                                country.CountryId,
                                country.CountryCd
                            } into s
                            group s by s.CountryId into g
                            select new
                            {
                                Name = g.FirstOrDefault().CountryCd,
                                Value = g.Select(x => x.VendorId).Count()
                            };

                var totalSupp = await query.LongCountAsync().ConfigureAwait(false);

                var list = await query.ToListAsync().ConfigureAwait(false);
                return new { Total = totalSupp, Data = list };
            }
        }

        public async Task<object> GetChartSalesSummary()
        {
            var today = DateTime.Today;
            var periodsYear = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

            var result = await this.CreateQueryChartSalesSummary().ConfigureAwait(false);

            var querySetup = from model in _context.TblDashboardProcurement
                                 where model.Id == (long)SalesEnum.SalesSummary
                                 select model;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.StatusId != null)
                result = result.Where(x => x.StatusId == setupProcurement.StatusId);
            if (setupProcurement.CurrencyId != null)
                result = result.Where(x => x.CurrencyId == setupProcurement.CurrencyId);

            var queryResult = from queryRes in result
                              group queryRes by queryRes.IvoDate.Month into g
                              let gs = g.FirstOrDefault()
                              join curr in _context.TblCurrency on gs.CurrencyId equals curr.CurrencyId into currencyx
                              from currency in currencyx.DefaultIfEmpty()
                              select new
                              {
                                  Name = g.FirstOrDefault().IvoDate.ToString("MMMM"),
                                  Value = g.Sum(x => x.IvoTotNetAmt),
                                  Currency = currency.CurrencyCd
                              };

            var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
            var isFiltered = await this.IsSalesSummaryFiltered(SalesEnum.SalesSummary).ConfigureAwait(false);
            
            var list = periodsYear.Select(p =>
            {

                var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                return new { Name = p, Value = value};
            }).ToList();
            var currencyName = (resultList.Count() == 0) ? "" : resultList.FirstOrDefault().Currency;
            return new { Name = currencyName, Series = list, IsFiltered = isFiltered };

        }

        private async Task<bool> IsSalesSummaryFiltered(SalesEnum type)
        {
            var queryFil = from setup in _context.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.CurrencyId != null ||
                           setup.StatusId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }

        private async Task<IQueryable<TblInvoice>> CreateQueryChartSalesSummary()
        {
            var thisYear = DateTime.Today.Year;
            var nextYear = DateTime.Today.Year + 1;
            var query = from summ in _context.TblInvoice
                        where summ.Deleted == false && summ.IvoDate.Year >= thisYear && summ.IvoDate.Year < nextYear
                        select summ;

            return query;
        }

        public async Task<object> GetChartDetail(SalesEnum type, string month)
        {
            using (_context)
            {
                if (type == SalesEnum.NumberofSQ)
                {
                    var query = await CreateQueryChartDetailSQ(type, month).ConfigureAwait(false);
                    var queryList = from model in query
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.SalesQuoId,
                                        Cd = model.SalesQuoCd,
                                        Date = model.SqtDate,
                                        Supplier = vendor.VndName,
                                        Status = status.StaName,
                                        Employee = employee.EmpName
                                    };

                    return await queryList.OrderBy(x => x.Cd).ToListAsync().ConfigureAwait(false);
                }
                else if (type == SalesEnum.NumberofSO)
                {
                    var query = await CreateQueryChartDetailSO(type, month).ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from division in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from department in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from project in proj.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    join employees in _context.TblEmployee on model.EmployeeId equals employees.EmployeeId into employeess
                                    from employee in employeess.DefaultIfEmpty()
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.Soid,
                                        Cd = model.Socd,
                                        Date = model.SoDate,
                                        Division = division.DivName,
                                        Department = department.DepName,
                                        Project = project.ProName,
                                        Status = status.StaName,
                                        Employee = employee.EmpName,
                                        Supplier = vendor.VndName
                                    };

                    return await queryList.OrderBy(x => x.Cd).ToListAsync().ConfigureAwait(false);
                }
                else if (type == SalesEnum.NumberofCustomer)
                {
                    var query = await CreateQueryChartDetailSO(type, month).ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from division in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from department in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from project in proj.DefaultIfEmpty()
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join currency in _context.TblCurrency on model.CurrencyId equals currency.CurrencyId
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        model.VendorId,
                                        Amount = model.SoNetAmt,
                                        Supplier = vendor.VndName,
                                        currency.CurrencyCd
                                    } into s
                                    group s by s.VendorId into g
                                    select new
                                    {
                                        g.FirstOrDefault().VendorId,
                                        g.FirstOrDefault().Supplier,
                                        Amount = g.Sum(x => x.Amount),
                                        Month = month,
                                        g.FirstOrDefault().CurrencyCd
                                    };

                    return await queryList.OrderByDescending(x => x.Amount).ToListAsync().ConfigureAwait(false);
                }
                else if(type == SalesEnum.SalesSummary)
                {
                    var query = await CreateQueryChartSalesSummaryDetail(type, month).ConfigureAwait(false);
                    var queryList = from model in query
                                    join modelPartx in _context.TblIvoPart on model.InvoiceId equals modelPartx.InvoiceId into modelPartxx
                                    from modelPart in modelPartxx.DefaultIfEmpty()
                                    join partx in _context.TblPart on modelPart.PartId equals partx.PartId into partxx
                                    from part in partxx.DefaultIfEmpty()
                                    join currx in _context.TblCurrency on model.CurrencyId equals currx.CurrencyId into currxx
                                    from curr in currxx.DefaultIfEmpty()
                                    group new { modelPart, part, curr } by modelPart.PartId into g
                                    let data = g.FirstOrDefault()
                                    select new
                                    {
                                        data.part.PrtViewName,
                                        Amount = g.Sum(x => x.modelPart.IvpAmount),
                                        Quantity = g.Sum(x => x.modelPart.IvpQuantity),
                                        data.curr.CurSymbol
                                    };

                    return await queryList.OrderByDescending(x => x.Amount).Take(50).ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    throw new ApplicationException("Unsupported Type.");
                }
            }
        }

        private async Task<IQueryable<TblInvoice>> CreateQueryChartSalesSummaryDetail(SalesEnum type, string month)
        {
            var query = from model in _context.TblInvoice
                        where model.IvoDate.ToString("MMMM") == month && model.Deleted == false && model.StatusId == (short)StatusEnum.Running
                        select model;
            return await SetupFilterSalesSummary(query, type).ConfigureAwait(false);
        }

        private async Task<IQueryable<TblInvoice>> SetupFilterSalesSummary(IQueryable<TblInvoice> query, SalesEnum type)
        {
            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)type
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.StatusId != null)
                query = query.Where(x => x.StatusId == setupProcurement.StatusId);
            if (setupProcurement.CurrencyId != null)
                query = query.Where(x => x.CurrencyId == setupProcurement.CurrencyId);
            return query;
        }



        public async Task<object> GetChartOfNumberCustomer()
        {
            using (_context)
            {
                var query = await this.CreateQueryChartSO(SalesEnum.NumberofCustomer);
                var queryResult = from queryRes in query
                                  group queryRes by queryRes.SoDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().SoDate.ToString("MMMM"),
                                      Value = g.Select(x => x.VendorId).Distinct().Count()
                                  };

                var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsSOFiltered(SalesEnum.NumberofSO).ConfigureAwait(false);
                var list = periods.Select(p =>
                {
                    var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                    return new { Name = p, Value = value };
                }).ToList();

                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        public async Task<object> GetOpenSOValue()
        {
            using (_context)
            {
                var query = await this.CreateQueryChartSO(SalesEnum.OpenSOValue);
                var queryResult = from queryRes in query
                                  join currency in _context.TblCurrency on queryRes.CurrencyId equals currency.CurrencyId
                                  select new
                                  {
                                      queryRes.CurrencyId,
                                      currency.CurrencyCd,
                                      queryRes.SoNetAmt,
                                      queryRes.SoDate
                                  } into s
                                  group s by s.SoDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().SoDate.ToString("MMMM"),
                                      Series = g.GroupBy(
                                          s => s.CurrencyId,
                                          s => s,
                                          (key, gg) => new { CurrencyId = key, Name = gg.FirstOrDefault().CurrencyCd, Value = gg.Sum(x => x.SoNetAmt) }
                                          ).ToList()
                                  };

                var list = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsSOFiltered(SalesEnum.OpenSOValue).ConfigureAwait(false);
                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        private async Task<IQueryable<TblSo>> CreateQueryChartSO(SalesEnum type)
        {
            var today = DateTime.Today;
            var previous6Month = today.AddMonths(-6);
            var query = from so in _context.TblSo
                        where so.Deleted == false && so.SoDate >= previous6Month && so.SoDate <= today && so.StatusId == (short)StatusEnum.Running
                        select so;

            return await SetupFilterSO(query, type).ConfigureAwait(false);
        }

        private async Task<bool> IsSOFiltered(SalesEnum type)
        {
            var queryFil = from setup in _context.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null || setup.CurrencyId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }

        private async Task<IQueryable<TblSalesQuo>> CreateQueryChartDetailSQ(SalesEnum type, string month)
        {
            var query = from model in _context.TblSalesQuo
                        where model.SqtDate.ToString("MMMM") == month && model.Deleted == false && model.StatusId == (short)StatusEnum.Running
                        select model;
            return await SetupFilterSQ(query, type).ConfigureAwait(false);
        }

        private async Task<IQueryable<TblSalesQuo>> SetupFilterSQ(IQueryable<TblSalesQuo> query, SalesEnum type)
        {
            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)type
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.CurrencyId != null)
                query = query.Where(x => x.CurrencyId == setupProcurement.CurrencyId);
            return query;
        }

        private async Task<IQueryable<TblSo>> CreateQueryChartDetailSO(SalesEnum type, string month)
        {
            var query = from model in _context.TblSo
                        where model.SoDate.ToString("MMMM") == month && model.Deleted == false && model.StatusId == (short)StatusEnum.Running
                        select model;
            return await SetupFilterSO(query, type).ConfigureAwait(false);
        }

        private async Task<IQueryable<TblSo>> SetupFilterSO(IQueryable<TblSo> query, SalesEnum type)
        {
            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)type
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.CurrencyId != null)
                query = query.Where(x => x.CurrencyId == setupProcurement.CurrencyId);
            return query;
        }

        public async Task<object> GetChartDetailNumberOfActiveCustomer(string month, long vendorId)
        {
            using (_context)
            {
                var vndTypeIds = new[] { 0, 1 };
                var query = await CreateQueryChartDetailSO(SalesEnum.NumberofCustomer, month).ConfigureAwait(false);
                var queryList = from model in query
                                join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                from divs in divv.DefaultIfEmpty()
                                join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                from deps in depp.DefaultIfEmpty()
                                join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                from projs in proj.DefaultIfEmpty()
                                join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                from vendor in vendr.DefaultIfEmpty()
                                join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                from stats in stat.DefaultIfEmpty()
                                where model.Deleted == false
                                where vendor != null
                                where  vndTypeIds.Contains(vendor.VndTypeId)
                                select new
                                {
                                    Id = model.Soid,
                                    Cd = model.Socd,
                                    Date = model.SoDate,
                                    Division = divs.DivName,
                                    Department = deps.DepName,
                                    Project = projs.ProName,
                                    Supplier = vendor.VndName,
                                    Status = stats.StaName,
                                    Amount = model.SoNetAmt
                                };

                return await queryList.OrderByDescending(x => x.Amount).ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetChartDetailOpenSOValue(SalesEnum type, string month, short currencyId)
        {
            using (_context)
            {
                var queryRes = from model in _context.TblSo
                               where model.SoDate.ToString("MMMM") == month && model.Deleted == false && model.StatusId == (short)StatusEnum.Running && model.CurrencyId == currencyId
                               select model;
                var query = await SetupFilterSO(queryRes, type).ConfigureAwait(false);
                var queryList = from model in query
                                join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                from divs in divv.DefaultIfEmpty()
                                join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                from deps in depp.DefaultIfEmpty()
                                join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                from projs in proj.DefaultIfEmpty()
                                join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                from vendor in vendr.DefaultIfEmpty()
                                join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                from stats in stat.DefaultIfEmpty()
                                join currency in _context.TblCurrency on model.CurrencyId equals currency.CurrencyId
                                select new
                                {
                                    Id = model.Soid,
                                    Cd = model.Socd,
                                    Date = model.SoDate,
                                    Supplier = vendor.VndName,
                                    CurrencyCd = currency.CurrencyCd,
                                    Amount = model.SoNetAmt
                                };

                return await queryList.OrderByDescending(x => x.Amount).ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetDetailTotalCustomerByCountry(string countryCd)
        {
            using (_context)
            {
                var vndTypeIds = new[] { 0, 1 };
                var query = from supplier in _context.TblVendor
                            join country in _context.TblCountry on supplier.CountryId equals country.CountryId
                            where supplier.Deleted == false && country.Deleted == false && country.CountryCd == countryCd && vndTypeIds.Contains(supplier.VndTypeId)
                            select new
                            {
                                supplier.VendorId,
                                supplier.VndName,
                                supplier.VndAddress,
                                supplier.VndPhone,
                                supplier.VndContact
                            };

                return await query.OrderBy(x => x.VndName).ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetDetailTransactionSOVendor(long vendorId, int page, int limit)
        {
            using (_context)
            {
                var queryList = from model in _context.TblSo
                                join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                from divs in divv.DefaultIfEmpty()
                                join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                from deps in depp.DefaultIfEmpty()
                                join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                from projs in proj.DefaultIfEmpty()
                                join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                from vendor in vendr.DefaultIfEmpty()
                                join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                from stats in stat.DefaultIfEmpty()
                                where model.Deleted == false && model.VendorId == vendorId && model.StatusId == (long)StatusEnum.Running
                                select new
                                {
                                    Id = model.Soid,
                                    Cd = model.Socd,
                                    Date = model.SoDate,
                                    Division = divs.DivName,
                                    Department = deps.DepName,
                                    Project = projs.ProName,
                                    Supplier = vendor.VndName,
                                    Status = stats.StaName
                                };

                var total = await queryList.LongCountAsync().ConfigureAwait(false);
                var Data = await queryList.OrderBy(x => x.Cd).Skip((page - 1) * limit).Take(limit).ToListAsync().ConfigureAwait(false);
                var Header = new { Total = total, Limit = limit, Page = page };
                return new { Data, Header };
            }
        }

    }
}
