﻿using Microsoft.EntityFrameworkCore;
using Dashboard.Database;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Repository
{
    public class DashboardRepository
    {
        private SofiContext mDBcontext;

        public DashboardRepository(SofiContext context)
        {
            this.mDBcontext = context;
        }

        public async Task<object>  GetDivision()
        {
            var query = from div in mDBcontext.TblDivision
                        where div.Deleted == false
                        select new { Name = div.DivName, Id = div.DivisionId };

            return await query.OrderBy(x => x.Name).ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetDepartment()
        {
            var query = from div in mDBcontext.TblDepartment
                        where div.Deleted == false
                        select new { Name = div.DepName, Id = div.DepartmentId};

            return await query.OrderBy(x => x.Name).ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetProject()
        {
            var query = from div in mDBcontext.TblProject
                        where div.Deleted == false
                        select new { Name = div.ProName, Id = div.ProjectId };

            return await query.OrderBy(x => x.Name).ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetRequestor()
        {
            var query = from div in mDBcontext.TblEmployee
                        where div.Deleted == false
                        select new { Name = div.EmpName, Id = div.EmployeeId };

            return await query.ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetSetupProcurement(ProcurementEnum param)
        {
            using (mDBcontext)
            {
                var query = from setup in mDBcontext.TblDashboardProcurement
                            where setup.Id == (long)param
                            select setup;

                return await query.FirstOrDefaultAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetSetupSales(SalesEnum param)
        {
            using (mDBcontext)
            {
                var query = from setup in mDBcontext.TblDashboardProcurement
                            where setup.Id == (long)param
                            select setup;

                return await query.FirstOrDefaultAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetPOType()
         {
            var query = from po in mDBcontext.TblPotype
                        where  po.Deleted == false
                        select new { Name = po.PotName, Id = po.PotypeId };

            return await query.ToListAsync().ConfigureAwait(false);
         }

        public async Task<object> GetCurrency()
        {
            var query = from po in mDBcontext.TblCurrency
                        where po.Deleted == false
                        select new { Name = po.CurrencyCd, Id = po.CurrencyId };

            return await query.ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetPartGroup()
        {
            var query = from po in mDBcontext.TblGrouping
                        where po.Deleted == false
                        select new { Name = po.GngName, Id = po.GroupingId };

            return await query.OrderBy(x => x.Name).ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetPartCat()
        {
            var query = from po in mDBcontext.TblPartCat
                        where po.Deleted == false
                        select new { Name = po.PacName, Id = po.PartCatId };

            return await query.OrderBy(x => x.Name).ToListAsync().ConfigureAwait(false);
        }

        public async Task<object> GetSupplier()
        {
            var query = from po in mDBcontext.TblVendor
                        where po.Deleted == false
                        select new { Name = po.VndName, Id = po.VendorId };

            return await query.OrderBy(x => x.Name).ToListAsync().ConfigureAwait(false);
        }

    }
}
