﻿using System;

namespace Dashboard.Repository
{
    public class StockMonitoringParam
    {
        public string PartCD { get; set; }
        public string DivisionCD { get; set; }
        public string DepartmentCD { get; set; }
        public string ProjectCD { get; set; }
        public string WarehouseCD { get; set; }
        public string GroupingCD { get; set; }
        public string PartCat1CD { get; set; }
        public string PartCat2CD { get; set; }
        public string PartCat3CD { get; set; }
        public string VendorCD { get; set; }
        public PriorityEnum? Priority { get; set; }
        public DateTime? TransDate { get; set; }

        public int Page { get; set; }
        public int Limit { get; set; }
    }

    public enum PriorityEnum
    {
        Low = 0,
        Medium,
        High
    }
}
