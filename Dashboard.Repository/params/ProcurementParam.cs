﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dashboard.Repository
{
    public class ProcurementParam
    {
        public ProcurementEnum Type { get; set; }
        public long? DivisionId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ProjectId { get; set; }
        public long? RequestorId { get; set; }
        public long? PoTypeId { get; set; }
        public long? CurrencyId { get; set; }
    }
}
