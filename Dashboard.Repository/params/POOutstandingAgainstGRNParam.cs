﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dashboard.Repository
{
    public class POOutstandingAgainstGRNParam
    {
        public long Poid { get;  set; }
        public string Pocd { get;  set; }
        public DateTime PoDate { get;  set; }
        public short DivisionId { get;  set; }
        public int? DepartmentId { get;  set; }
        public int EmployeeId { get;  set; }
        public long ProjectId { get;  set; }
        public short StatusId { get;  set; }
        public short PotypeId { get;  set; }
        public decimal POQty { get;  set; }
        public decimal GRNQty { get;  set; }
        public decimal POOsQty { get;  set; }
        public bool IsOutstanding { get;  set; }
        public int VendorId { get;  set; }
    }
}
