﻿namespace Dashboard.Repository
{
    public class SalesParam
    {
        public SalesEnum Type { get; set; }
        public long? DivisionId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ProjectId { get; set; }
        public long? RequestorId { get; set; }
        public long? CurrencyId { get; set; }
        public long? StatusId { get;  set; }
    }
}
