﻿using System;

namespace Dashboard.Repository
{
    public class SQOutstandingAgaintsSOParam
    {
        public long SalesQuoId { get;  set; }
        public string SalesQuoCd { get;  set; }
        public DateTime SqtDate { get;  set; }
        public int EmployeeId { get;  set; }
        public short StatusId { get;  set; }
        public bool IsOutstanding { get;  set; }
        public int? DivisionId { get;  set; }
        public int? DepartmentId { get;  set; }
        public long? ProjectId { get;  set; }
    }
}
