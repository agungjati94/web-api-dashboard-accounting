﻿namespace Dashboard.Repository
{
    public enum StatusEnum
    {

        Entry = 0,
        Confirm,
        Running,
        Revise,
        Pending,
        Cancel,
        Close,
        Postpone
    }
}
