﻿using System;

namespace Dashboard.Repository
{
    public class SOOutstandingAgaintsDO
    {
        public long Soid { get;  set; }
        public string Socd { get;  set; }
        public DateTime SoDate { get;  set; }
        public short DivisionId { get;  set; }
        public int? DepartmentId { get;  set; }
        public long ProjectId { get;  set; }
        public int EmployeeId { get;  set; }
        public short StatusId { get;  set; }
        public int VendorId { get;  set; }
        public bool IsOutstanding { get;  set; }
    }
}
