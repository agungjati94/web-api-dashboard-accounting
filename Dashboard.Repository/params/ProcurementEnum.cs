﻿namespace Dashboard.Repository
{
    public enum ProcurementEnum
    {
        MPRNeedToConfirm = 1,
        MPRNeedToRunning,
        OutstandingMPRAgainstPO,
        OutstandingMPRAgainstGRN,
        PONeedtoConfirm,
        PONeedtoRunning,
        OutstandingPOAgainstGRN,
        OutstandingPOAgainstSI,
        NumberofPO,
        OpenPOValue,
        NumberofSupplier,
        NumberofMPR
    }
}
