﻿namespace Dashboard.Repository
{
    public enum SalesEnum
    {
        SQNeedToConfirm = 13,
        SQNeedToRunning,
        SONeedToConfirm,
        SONeedToRunning,
        OutstandingSQAgainstSO,
        OutstandingSQAgainstDO,
        OutstandingSOAgainstDO,
        OutstandingSOAgainstInvoice,
        NumberofSQ,
        NumberofSO,
        OpenSOValue,
        NumberofCustomer,
        SalesSummary
    }
}
