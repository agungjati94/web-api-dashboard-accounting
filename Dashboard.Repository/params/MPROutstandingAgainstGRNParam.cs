﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dashboard.Repository
{
    public class MPROutstandingAgainstGRNParam
    {
        public long PurchReqId { get;  set; }
        public int DivisionId { get;  set; }
        public int DepartmentId { get;  set; }
        public long ProjectId { get;  set; }
        public int EmployeeId { get;  set; }
        public decimal MPRQty { get;  set; }
        public decimal GRNOsQty { get;  set; }
        public decimal GRNQty { get;  set; }
        public bool IsOutstanding { get;  set; }
        public int StatusId { get;  set; }
        public string PurchReqCd { get;  set; }
        public DateTime PrqDate { get;  set; }
        public decimal POQty { get; set; }
        public decimal POOsQty { get; set; }
    }
}
