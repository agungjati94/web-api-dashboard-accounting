﻿using System;

namespace Dashboard.Repository
{
    public class POOutstandingAgainstSIParam
    {
        public long Poid { get;  set; }
        public string Pocd { get;  set; }
        public DateTime PoDate { get;  set; }
        public short DivisionId { get;  set; }
        public int? DepartmentId { get;  set; }
        public long ProjectId { get;  set; }
        public int EmployeeId { get;  set; }
        public short StatusId { get;  set; }
        public decimal POQty { get;  set; }
        public decimal SIOsQty { get;  set; }
        public decimal SIQty { get;  set; }
        public bool IsOutstanding { get;  set; }
        public short POType { get;  set; }
        public int VendorID { get;  set; }
    }
}
