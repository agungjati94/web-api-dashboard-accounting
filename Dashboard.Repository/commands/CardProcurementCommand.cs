﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Dashboard.Database;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Dashboard.Repository
{
    public class CardProcurementCommand
    {
        private SofiContext mDBcontext;

        public CardProcurementCommand(SofiContext context)
        {
            this.mDBcontext = context;
        }

        public async Task<object> CountMPRNeeedtoConfirm()
        {
            using (mDBcontext)
            {
                IQueryable<TblPurchReq> query = await CreateQueryMPRNeedtoConfirm().ConfigureAwait(false);
                bool IsFiltered = await IsMPRFiltered(ProcurementEnum.MPRNeedToConfirm).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountMPROutstandingAgainstGRN()
        {
            using (mDBcontext)
            {
                IQueryable<MPROutstandingAgainstGRNParam> query = await CreateQueryMPROutstandingAgainstGRN().ConfigureAwait(false);
                bool IsFiltered = await IsMPRFiltered(ProcurementEnum.OutstandingMPRAgainstGRN).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountMPRNeeedtoRunning()
        {
            using (mDBcontext)
            {
                IQueryable<TblPurchReq> query = await CreateQueryMPRNeeedtoRunning().ConfigureAwait(false);
                bool IsFiltered = await IsMPRFiltered(ProcurementEnum.MPRNeedToRunning).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };

            }
        }

        public async Task<object> CountMPROutstandingAgainstPO()
        {
            using (mDBcontext)
            {
                IQueryable<MPROutstandingAgainstGRNParam> query = await CreateQueryMPROutstandingAgainstPO().ConfigureAwait(false);
                bool IsFiltered = await IsMPRFiltered(ProcurementEnum.OutstandingMPRAgainstPO).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }
        public async Task<object> CountPOOutstandingAgainstGRN()
        {
            using (mDBcontext)
            {
                IQueryable<POOutstandingAgainstGRNParam> query = await CreateQueryPOOutstandingAgainstGRN().ConfigureAwait(false);
                bool IsFiltered = await IsPOFiltered(ProcurementEnum.OutstandingPOAgainstGRN).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }


        public async Task<object> CountPOOutstandingAgainstSI()
        {
            using (mDBcontext)
            {
                IQueryable<POOutstandingAgainstSIParam> query = await CreateQueryPOOutstandingAgainstSI().ConfigureAwait(false);
                bool IsFiltered = await IsPOFiltered(ProcurementEnum.OutstandingPOAgainstSI).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountPONeeedtoRunning()
        {
            using (mDBcontext)
            {
                IQueryable<TblPo> query = await CreateQueryPONeedtoRunning().ConfigureAwait(false);
                bool IsFiltered = await IsPOFiltered(ProcurementEnum.PONeedtoRunning).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountPONeeedtoConfirm()
        {
            using (mDBcontext)
            {
                IQueryable<TblPo> query = await CreateQueryPONeedtoConfirm().ConfigureAwait(false);
                bool IsFiltered = await IsPOFiltered(ProcurementEnum.PONeedtoConfirm).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> GetProcurementDetail(ProcurementEnum param)
        {
            using (mDBcontext)
            {
                if (param == ProcurementEnum.MPRNeedToConfirm)
                {
                    var query = await this.CreateQueryMPRNeedtoConfirm().ConfigureAwait(false);
                    var queryList = from pr in query
                                    join div in mDBcontext.TblDivision on pr.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on pr.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on pr.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in mDBcontext.TblEmployee on pr.EmployeeId equals emp.EmployeeId into empl
                                    from emply in empl.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on pr.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = pr.PurchReqId,
                                        Cd = pr.PurchReqCd,
                                        Date = pr.PrqDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Requestor = emply.EmpName,
                                        Status = stats.StaName,
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.MPRNeedToRunning)
                {
                    var query = await this.CreateQueryMPRNeeedtoRunning().ConfigureAwait(false);
                    var queryList = from pr in query
                                    join div in mDBcontext.TblDivision on pr.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on pr.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on pr.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in mDBcontext.TblEmployee on pr.EmployeeId equals emp.EmployeeId into empl
                                    from emply in empl.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on pr.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = pr.PurchReqId,
                                        Cd = pr.PurchReqCd,
                                        Date = pr.PrqDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Requestor = emply.EmpName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.OutstandingMPRAgainstPO)
                {
                    var query = await this.CreateQueryMPROutstandingAgainstPO().ConfigureAwait(false);
                    var queryList = from pr in query
                                    join div in mDBcontext.TblDivision on pr.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on pr.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on pr.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in mDBcontext.TblEmployee on pr.EmployeeId equals emp.EmployeeId into empl
                                    from emply in empl.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on pr.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = pr.PurchReqId,
                                        Cd = pr.PurchReqCd,
                                        Date = pr.PrqDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Requestor = emply.EmpName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.OutstandingMPRAgainstGRN)
                {
                    var query = await this.CreateQueryMPROutstandingAgainstGRN().ConfigureAwait(false);
                    var queryList = from pr in query
                                    join div in mDBcontext.TblDivision on pr.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on pr.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on pr.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in mDBcontext.TblEmployee on pr.EmployeeId equals emp.EmployeeId into empl
                                    from emply in empl.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on pr.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = pr.PurchReqId,
                                        Cd = pr.PurchReqCd,
                                        Date = pr.PrqDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Requestor = emply.EmpName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.PONeedtoConfirm)
                {
                    var query = await this.CreateQueryPONeedtoConfirm().ConfigureAwait(false);
                    var queryList = from po in query
                                    join div in mDBcontext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in mDBcontext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on po.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = po.Poid,
                                        Cd = po.Pocd,
                                        Date = po.PoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.PONeedtoRunning)
                {
                    var query = await this.CreateQueryPONeedtoRunning().ConfigureAwait(false);
                    var queryList = from po in query
                                    join div in mDBcontext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in mDBcontext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on po.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = po.Poid,
                                        Cd = po.Pocd,
                                        Date = po.PoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.OutstandingPOAgainstGRN)
                {
                    var query = await this.CreateQueryPOOutstandingAgainstGRN().ConfigureAwait(false);
                    var queryList = from po in query
                                    join div in mDBcontext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in mDBcontext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on po.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = po.Poid,
                                        Cd = po.Pocd,
                                        Date = po.PoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == ProcurementEnum.OutstandingPOAgainstSI)
                {
                    var query = await this.CreateQueryPOOutstandingAgainstSI().ConfigureAwait(false);
                    var queryList = from po in query
                                    join div in mDBcontext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBcontext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBcontext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in mDBcontext.TblVendor on po.VendorID equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in mDBcontext.TblStatus on po.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = po.Poid,
                                        Cd = po.Pocd,
                                        Date = po.PoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    throw new ApplicationException("Unsupported type");
                }
            }
        }

        private async Task<IQueryable<MPROutstandingAgainstGRNParam>> CreateQueryMPROutstandingAgainstGRN()
        {
            var query = from mpr in mDBcontext.TblPurchReq
                        join mprParts in mDBcontext.TblPurchReqPart on mpr.PurchReqId equals mprParts.PurchReqId into mprPartss
                        from mprPart in mprPartss.DefaultIfEmpty()

                        join poParts in mDBcontext.TblPopart on mprPart.SeqId equals poParts.RefSeqId into poPartss
                        from poPart in poPartss.DefaultIfEmpty()
                        join pos in mDBcontext.TblPo on poPart.Poid equals pos.Poid into poss
                        from po in poss.DefaultIfEmpty()

                        join grnParts in mDBcontext.TblInvPart on poPart.SeqId equals grnParts.RefSeqId into grnPartss
                        from grnPart in grnPartss.DefaultIfEmpty()
                        join grns in mDBcontext.TblInventory on grnPart.InventoryId equals grns.InventoryId into grnss
                        from grn in grnss.DefaultIfEmpty()

                        where mpr.Deleted == false && mpr.StatusId == (short)StatusEnum.Running
                        let isMprPartNull = mprPart == null
                        let isPoPartNull = poPart == null
                        let isGrnPartNull = grnPart == null
                        select new
                        {
                            mpr.PurchReqId,
                            mpr.PurchReqCd,
                            mpr.PrqDate,
                            mpr.DivisionId,
                            mpr.DepartmentId,
                            mpr.ProjectId,
                            mpr.EmployeeId,
                            mpr.StatusId,
                            MPRPartId = isMprPartNull ? 0 : mprPart.PartId,
                            MPRQty = isMprPartNull ? 0 : mprPart.PrpQuantity,

                            IsPOValid = isPoPartNull ? false : (po.Deleted == false && po.StatusId == (short)StatusEnum.Running),

                            GRNQty = isGrnPartNull ? 0 : grnPart.InpQuantity,
                            IsGRNValid = isGrnPartNull ? false : (grn.Deleted == false && grn.StatusId == (short)StatusEnum.Running),

                            // };

                            /// var debug = await query.ToListAsync().ConfigureAwait(false);

                        }
                       into sTransaction
                        group sTransaction by sTransaction.PurchReqId into sTransactionG
                        let qtyMpr = sTransactionG.FirstOrDefault().MPRQty
                        let qtyGrn = sTransactionG.Where(w => w.IsGRNValid && w.IsPOValid).Sum(g => g.GRNQty)
                        let osGrn = qtyMpr - qtyGrn
                        let mprHeader = sTransactionG.FirstOrDefault()
                        select new MPROutstandingAgainstGRNParam
                        {
                            PurchReqId = mprHeader.PurchReqId,
                            PurchReqCd = mprHeader.PurchReqCd,
                            PrqDate = mprHeader.PrqDate,
                            DivisionId = mprHeader.DivisionId,
                            DepartmentId = mprHeader.DepartmentId,
                            ProjectId = mprHeader.ProjectId,
                            EmployeeId = mprHeader.EmployeeId,
                            StatusId = mprHeader.StatusId,
                            MPRQty = qtyMpr,
                            GRNOsQty = osGrn,
                            GRNQty = qtyGrn,
                            IsOutstanding = osGrn > 0
                        };

    query = query.Where(w => w.IsOutstanding == true);




            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.OutstandingMPRAgainstGRN
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);

            if (setupProcurement.DivisionId != null)
                query = query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<TblPurchReq>> CreateQueryMPRNeedtoConfirm()
        {
            var query = from mpr in mDBcontext.TblPurchReq
                        join mprPart in mDBcontext.TblPurchReqPart on mpr.PurchReqId equals mprPart.PurchReqId
                        where mprPart != null
                        where mpr.Deleted == false
                        where mpr.StatusId == (short)StatusEnum.Entry || (mpr.StatusId == (short)StatusEnum.Revise && mpr.LastStatusId == (short)StatusEnum.Confirm)
                        group mpr by mpr.PurchReqId into g
                        select g.FirstOrDefault();


          

            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                    where setup.Id == (long)ProcurementEnum.MPRNeedToConfirm
                    select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

       

        private async Task<IQueryable<TblPurchReq>> CreateQueryMPRNeeedtoRunning()
        {
            var query = from mpr in mDBcontext.TblPurchReq
                        where mpr.Deleted == false
                        where (mpr.StatusId == (short)StatusEnum.Confirm) || (mpr.StatusId == (short)StatusEnum.Revise && mpr.LastStatusId == (short)StatusEnum.Running)
                        group mpr by mpr.PurchReqId into g
                        select g.FirstOrDefault();

            var debug = await query.ToListAsync().ConfigureAwait(false);

            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.MPRNeedToRunning
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }
       
        private async Task<IQueryable<MPROutstandingAgainstGRNParam>> CreateQueryMPROutstandingAgainstPO()
        {
            var query = from mpr in mDBcontext.TblPurchReq
                        join mprParts in mDBcontext.TblPurchReqPart on mpr.PurchReqId equals mprParts.PurchReqId into mprPartss
                        from mprPart in mprPartss.DefaultIfEmpty()

                        join poParts in mDBcontext.TblPopart on mprPart.SeqId equals poParts.RefSeqId into poPartss
                        from poPart in poPartss.DefaultIfEmpty()
                        join pos in mDBcontext.TblPo on poPart.Poid equals pos.Poid into poss
                        from po in poss.DefaultIfEmpty()

                        where mpr.Deleted == false && mpr.StatusId == (short)StatusEnum.Running
                        let isMprPartNull = mprPart == null
                        let isPoPartNull = poPart == null
                        select new
                        {
                            mpr.PurchReqId,
                            mpr.PurchReqCd,
                            mpr.PrqDate,
                            mpr.DivisionId,
                            mpr.DepartmentId,
                            mpr.ProjectId,
                            mpr.EmployeeId,
                            mpr.StatusId,
                            MPRPartId = isMprPartNull ? 0 : mprPart.PartId,
                            MPRQty = isMprPartNull ? 0 : mprPart.PrpQuantity,
                            IsPOValid = isPoPartNull ? false : (po.Deleted == false && po.StatusId == (short)StatusEnum.Running),
                            POQty = isPoPartNull ? 0 : poPart.PopQuantity
                        } into sTransaction
                        group sTransaction by sTransaction.PurchReqId into sTransactionG
                        let qtyMpr = sTransactionG.FirstOrDefault().MPRQty
                        let qtyPO = sTransactionG.Where(w => w.IsPOValid).Sum(g => g.POQty)
                        let osPO = qtyMpr - sTransactionG.Sum(x => x.POQty)
                        let mprHeader = sTransactionG.FirstOrDefault()
                        select new MPROutstandingAgainstGRNParam
                        {
                            PurchReqId = mprHeader.PurchReqId,
                            PurchReqCd = mprHeader.PurchReqCd,
                            PrqDate = mprHeader.PrqDate,
                            DivisionId = mprHeader.DivisionId,
                            DepartmentId = mprHeader.DepartmentId,
                            ProjectId = mprHeader.ProjectId,
                            EmployeeId = mprHeader.EmployeeId,
                            StatusId = mprHeader.StatusId,
                            MPRQty = qtyMpr,
                            POQty = qtyPO,
                            POOsQty = osPO,
                            IsOutstanding = osPO > 0
                        };

            query = query.Where(w => w.IsOutstanding == true);

            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.OutstandingMPRAgainstPO
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

       

        private async Task<IQueryable<TblPo>> CreateQueryPONeedtoRunning()
        {
            var query = from PO in mDBcontext.TblPo
                        where PO.Deleted == false
                        where (PO.StatusId == (short)StatusEnum.Confirm) || (PO.StatusId == (short)StatusEnum.Revise && PO.LastStatusId == (short)StatusEnum.Running)
                        group PO by PO.Poid into g
                        select g.FirstOrDefault();

            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.PONeedtoRunning
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.PotypeId != null)
                query = query.Where(x => x.PotypeId == setupProcurement.PotypeId);

            return query;
        }
        

        private async Task<IQueryable<TblPo>> CreateQueryPONeedtoConfirm()
        {
            var query = from PO in mDBcontext.TblPo
                        join POPart in mDBcontext.TblPopart on PO.Poid equals POPart.Poid
                        where POPart != null
                        where PO.Deleted == false
                        where PO.StatusId == (short)StatusEnum.Entry || (PO.StatusId == (short)StatusEnum.Revise && PO.LastStatusId == (short)StatusEnum.Confirm)
                        group PO by PO.Poid into g
                        select g.FirstOrDefault();

            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.PONeedtoConfirm
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.PotypeId != null)
                query = query.Where(x => x.PotypeId == setupProcurement.PotypeId);

            return query;
        }

        

        private async Task<IQueryable<POOutstandingAgainstSIParam>> CreateQueryPOOutstandingAgainstSI()
        {
            var query = from po in mDBcontext.TblPo
                        join poParts in mDBcontext.TblPopart on po.Poid equals poParts.Poid into poPartss
                        from poPart in poPartss.DefaultIfEmpty()

                        join grnParts in mDBcontext.TblInvPart on poPart.SeqId equals grnParts.RefSeqId into grnPartss
                        from grnPart in grnPartss.DefaultIfEmpty()
                        join grns in mDBcontext.TblInventory on grnPart.InventoryId equals grns.InventoryId into grnss
                        from grn in grnss.DefaultIfEmpty()

                        join siParts in mDBcontext.TblRecIvoPart on grnPart.SeqId equals siParts.RefSeqId into siPartss
                        from siPart in siPartss.DefaultIfEmpty()
                        join siHeaders in mDBcontext.TblRecInvoice on siPart.RecInvoiceId equals siHeaders.RecInvoiceId into siHeaderss
                        from siHeader in siHeaderss.DefaultIfEmpty()

                        where po.Deleted == false && po.StatusId == (short)StatusEnum.Running
                        let isPoPartNull = poPart == null
                        let isGrnPartNull = grnPart == null
                        let isSiPartNull = siPart == null
                        select new
                        {
                            po.Poid,
                            po.Pocd,
                            po.PoDate,
                            po.DivisionId,
                            po.DepartmentId,
                            po.ProjectId,
                            po.EmployeeId,
                            po.StatusId,
                            po.PotypeId,
                            po.VendorId,
                            POPartId = isPoPartNull ? 0 : poPart.PartId,
                            POQty = isPoPartNull ? 0 : poPart.PopQuantity,

                            IsGRNValid = isGrnPartNull ? false : (grn.Deleted == false && grn.StatusId == (short)StatusEnum.Running),

                            SIQty = isSiPartNull ? 0 : siPart.RipQuantity,
                            IsSIValid = isSiPartNull ? false : (siHeader.Deleted == false && siHeader.StatusId == (short)StatusEnum.Running),

                        } into sTransaction

                        group sTransaction by sTransaction.Poid into sTransactionG
                        let qtyPO = sTransactionG.FirstOrDefault().POQty
                        let qtySi = sTransactionG.Where(w => w.IsSIValid && w.IsGRNValid).Sum(g => g.SIQty)
                        let osSi = qtyPO - qtySi
                        let poHeader = sTransactionG.FirstOrDefault()
                        select new POOutstandingAgainstSIParam
                        {
                            Poid = poHeader.Poid,
                            Pocd = poHeader.Pocd,
                            PoDate = poHeader.PoDate,
                            DivisionId = poHeader.DivisionId,
                            DepartmentId = poHeader.DepartmentId,
                            ProjectId = poHeader.ProjectId,
                            EmployeeId = poHeader.EmployeeId,
                            StatusId = poHeader.StatusId,
                            POQty = qtyPO,
                            SIOsQty = osSi,
                            SIQty = qtySi,
                            IsOutstanding = osSi > 0,
                            POType = poHeader.PotypeId,
                            VendorID = poHeader.VendorId
                        };

            query = query.Where(w => w.IsOutstanding == true);

            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.OutstandingPOAgainstSI
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);

            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.PotypeId != null)
                query = query.Where(x => x.POType == setupProcurement.PotypeId);

            return query;
        }

        private async Task<IQueryable<POOutstandingAgainstGRNParam>> CreateQueryPOOutstandingAgainstGRN()
        {
          

            var query = from po in mDBcontext.TblPo
                        join poParts in mDBcontext.TblPopart on po.Poid equals poParts.Poid into poPartss
                        from poPart in poPartss.DefaultIfEmpty()

                        join grnParts in mDBcontext.TblInvPart on poPart.SeqId equals grnParts.RefSeqId into grnPartss
                        from grnPart in grnPartss.DefaultIfEmpty()
                        join grns in mDBcontext.TblInventory on grnPart.InventoryId equals grns.InventoryId into grnss
                        from grn in grnss.DefaultIfEmpty()

                        where po.Deleted == false && po.StatusId == (short)StatusEnum.Running
                        let isPOPartNull = poPart == null
                        let isGrnPartNull = grnPart == null
                        select new
                        {
                            po.Poid,
                            po.Pocd,
                            po.PoDate,
                            po.DivisionId,
                            po.DepartmentId,
                            po.ProjectId,
                            po.EmployeeId,
                            po.StatusId,
                            po.PotypeId,
                            po.VendorId,
                            POPartId = isPOPartNull ? 0 : poPart.PartId,
                            POQty = isPOPartNull ? 0 : poPart.PopQuantity,
                            IsGRNValid = isGrnPartNull ? false : (grn.Deleted == false && grn.StatusId == (short)StatusEnum.Running),
                            GRNQty = isGrnPartNull ? 0 : grnPart.InpQuantity
                        } into sTransaction
                        group sTransaction by sTransaction.Poid into sTransactionG
                        let qtyPo = sTransactionG.FirstOrDefault().POQty
                        let qtyGrn = sTransactionG.Where(w => w.IsGRNValid).Sum(g => g.GRNQty)
                        let osPO = qtyPo - qtyGrn
                        let poHeader = sTransactionG.FirstOrDefault()
                        select new POOutstandingAgainstGRNParam
                        {
                            Poid = poHeader.Poid,
                            Pocd = poHeader.Pocd,
                            PoDate = poHeader.PoDate,
                            DivisionId = poHeader.DivisionId,
                            DepartmentId = poHeader.DepartmentId,
                            ProjectId = poHeader.ProjectId,
                            EmployeeId = poHeader.EmployeeId,
                            PotypeId = poHeader.PotypeId,
                            StatusId = poHeader.StatusId,
                            VendorId = poHeader.VendorId,
                            POQty = qtyPo,
                            GRNQty = qtyGrn,
                            POOsQty = osPO,
                            IsOutstanding = osPO > 0
                        };

            query = query.Where(w => w.IsOutstanding == true);
            var querySetup = from setup in mDBcontext.TblDashboardProcurement
                             where setup.Id == (long)ProcurementEnum.OutstandingPOAgainstGRN
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.PotypeId != null)
                query = query.Where(x => x.PotypeId == setupProcurement.PotypeId);

            return query;
        }

        private async Task<bool> IsMPRFiltered(ProcurementEnum type)
        {
            var queryFil = from setup in mDBcontext.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }

        private async Task<bool> IsPOFiltered(ProcurementEnum type)
        {
            var queryFil = from setup in mDBcontext.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null || setup.PotypeId !=  null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }
    }

    public class GRNpartCount
    {
        public long GRNId { get;  set; }
        public Guid GRNSeqId { get;  set; }
        public Guid GRNRefSeqId { get;  set; }
        public decimal GRNQty { get;  set; }
    }

    public class POpartCount
    {
        public long POId { get;  set; }
        public Guid POSeqId { get;  set; }
        public Guid PORefSeqId { get;  set; }
        public decimal POQty { get;  set; }
        public List<GRNpartCount> Grn { get;  set; }
    }

    public class MPRpartCount
    {
        public long MPRId { get;  set; }
        public Guid MPRSeqId { get;  set; }
        public decimal MPRQty { get;  set; }
        public List<POpartCount> Po { get;  set; }
    }

   
}
