﻿using Microsoft.EntityFrameworkCore;
using Dashboard.Database;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Repository
{
    public class SaveProcurementCommand
    {
        private SofiContext mDBcontext;

        public SaveProcurementCommand(SofiContext context)
        {
            this.mDBcontext = context;
        }

        public async Task SaveFilterProcurement(ProcurementParam procurementParam)
        {
            using (mDBcontext)
            {
                var query = from setup in mDBcontext.TblDashboardProcurement
                            where setup.Id == (long)procurementParam.Type
                            select setup;

                var result = await query.FirstOrDefaultAsync().ConfigureAwait(false);
                result.DivisionId = procurementParam.DivisionId;
                result.DepartmentId = procurementParam.DepartmentId;
                result.ProjectId = procurementParam.ProjectId;
                result.RequestorId = procurementParam.RequestorId;
                result.PotypeId = procurementParam.PoTypeId;
                result.CurrencyId = procurementParam.CurrencyId;

                await mDBcontext.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task SaveFilterSales(SalesParam salesParam)
        {
            using (mDBcontext)
            {
                var query = from setup in mDBcontext.TblDashboardProcurement
                            where setup.Id == (long)salesParam.Type
                            select setup;

                var result = await query.FirstOrDefaultAsync().ConfigureAwait(false);
                result.DivisionId = salesParam.DivisionId;
                result.DepartmentId = salesParam.DepartmentId;
                result.ProjectId = salesParam.ProjectId;
                result.RequestorId = salesParam.RequestorId;
                result.CurrencyId = salesParam.CurrencyId;
                result.StatusId = salesParam.StatusId;

                await mDBcontext.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}
