﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Dashboard.Database;

namespace Dashboard.Repository
{
    public class WorkCalendarRepository
    {
        private SofiContext mDBContext { get; set; }
        public WorkCalendarRepository(SofiContext sofiContext)
        {
            this.mDBContext = sofiContext;
        }

        public async Task<object> GetDue(WorkCalendarEnum[] type, DateTime duedate)
        {
            using (mDBContext)
            {
                if (type.Contains(WorkCalendarEnum.DueMPR) && type.Contains(WorkCalendarEnum.DuePO))
                {
                    return await GetDueMPRandPO(duedate).ConfigureAwait(false);
                }
                else if (type.Contains(WorkCalendarEnum.DuePO))
                {
                    return await GetDuePO(duedate).ConfigureAwait(false);
                }
                else if (type.Contains(WorkCalendarEnum.DueMPR))
                {
                    return await GetDueMPR(duedate).ConfigureAwait(false);
                }
                else
                {
                    throw new ApplicationException("Unsopperted Type");
                }
            }
        }

        private async Task<object> GetDueMPRandPO(DateTime duedate)
        {
            var queryPO  = from po in mDBContext.TblPo
                           join part in mDBContext.TblPopart on po.Poid equals part.Poid 
                           join supplier in mDBContext.TblVendor on po.VendorId equals supplier.VendorId
                           join status in mDBContext.TblStatus on po.StatusId equals status.StatusId
                           join employee in mDBContext.TblEmployee on po.EmployeeId equals employee.EmployeeId
                           where po.PoDate.Month == duedate.Month && po.Deleted == false
                           select new
                           {
                               Start = po.PoDate.Date,
                               End = po.PoDate.Date,
                               Title = "PO - " + po.Pocd + " - " + po.PoDate.ToString("dd MMMM yyyy") + " - " + supplier.VndName + " - " + employee.EmpName + " - " + status.StaName,
                               Color = new { Primary = "#ad2121", Secondary = "#FAE3E3" },
                               Id = po.Poid,
                               RefId = part.RefId,
                               Type = "PO"
                           } into s
                           group s by s.Id into g
                           select g.FirstOrDefault();

            var poList =  await queryPO.ToListAsync().ConfigureAwait(false);

            var queryMPR = from mpr in mDBContext.TblPurchReq
                           join status in mDBContext.TblStatus on mpr.StatusId equals status.StatusId
                           join employee in mDBContext.TblEmployee on mpr.EmployeeId equals employee.EmployeeId
                           where mpr.PrqReqDate.Month == duedate.Month && mpr.Deleted == false
                        select new
                        {
                            Start = mpr.PrqReqDate.Date,
                            End = mpr.PrqReqDate.Date,
                            Title = $"MPR - {mpr.PurchReqCd} - {mpr.PrqDate.ToString("dd MMMM yyyy")} - {employee.EmpName} - {status.StaName}",
                            Color =  new { Primary = "#1e90ff", Secondary = "#1e90ff" },
                            Id = mpr.PurchReqId,
                            RefId = 0L,
                            Type = "MPR"
                        };

            var mprList = await queryMPR.ToListAsync().ConfigureAwait(false);

            return poList.Concat(mprList);
        }

        private async Task<object> GetDuePO(DateTime duedate)
        {
            var query = from po in mDBContext.TblPo
                        join part in mDBContext.TblPopart on po.Poid equals part.Poid
                        join supplier in mDBContext.TblVendor on po.VendorId equals supplier.VendorId
                        join status in mDBContext.TblStatus on po.StatusId equals status.StatusId
                        join employee in mDBContext.TblEmployee on po.EmployeeId equals employee.EmployeeId
                        where po.PoDate.Month == duedate.Month && po.Deleted == false
                        select new
                        {
                            Start = po.PoDate.Date,
                            End = po.PoDate.Date,
                            Title = "PO - " + po.Pocd + " - " + po.PoDate.ToString("dd MMMM yyyy") + " - " + supplier.VndName + " - " + employee.EmpName + " - " + status.StaName,
                            Color = new { Primary = "#ad2121", Secondary = "#FAE3E3" },
                            Id = po.Poid,
                            RefId = part.RefId,
                            Type = "PO"
                        } into s
                        group s by s.Id into g
                        select g.FirstOrDefault();

            return await query.ToListAsync().ConfigureAwait(false);
        }

        private async Task<object> GetDueMPR(DateTime duedate)
        {
            var query = from mpr in mDBContext.TblPurchReq
                        join status in mDBContext.TblStatus on mpr.StatusId equals status.StatusId
                        join employee in mDBContext.TblEmployee on mpr.EmployeeId equals employee.EmployeeId
                        where mpr.PrqReqDate.Month == duedate.Month && mpr.Deleted == false
                        select new
                        {
                            Start = mpr.PrqReqDate.Date,
                            End = mpr.PrqReqDate.Date,
                            Title = $"MPR - {mpr.PurchReqCd} - {mpr.PrqDate.ToString("dd MMMM yyyy")} - {employee.EmpName} - {status.StaName}",
                            Color = new { Primary = "#1e90ff", Secondary = "#1e90ff" },
                            Id = mpr.PurchReqId,
                            Type = "MPR"
                        };

            return await query.ToListAsync().ConfigureAwait(false);
        }
    }
}
