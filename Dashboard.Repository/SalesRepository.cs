﻿using Microsoft.EntityFrameworkCore;
using Dashboard.Database;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Repository
{
    public class SalesRepository
    {
        private SofiContext _context;
        public SalesRepository(SofiContext context)
        {
            _context = context;
        }

        public async Task<object> CountSQNeedToConfirm()
        {
            using (_context)
            {
                IQueryable<TblSalesQuo> query = await CreateQuerySQNeedtoConfirm().ConfigureAwait(false);
                bool IsFiltered = await IsSQFiltered(SalesEnum.SQNeedToConfirm).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSQNeedToRunning()
        {
            using (_context)
            {
                IQueryable<TblSalesQuo> query = await CreateQuerySQNeedtoRunning().ConfigureAwait(false);
                bool IsFiltered = await IsSQFiltered(SalesEnum.SQNeedToRunning).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSONeedToConfirm()
        {
            using (_context)
            {
                IQueryable<TblSo> query = await CreateQuerySONeedtoConfirm().ConfigureAwait(false);
                bool IsFiltered = await IsSOFiltered(SalesEnum.SONeedToConfirm).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSONeedToRunning()
        {
            using (_context)
            {
                IQueryable<TblSo> query = await CreateQuerySONeedtoRunning().ConfigureAwait(false);
                bool IsFiltered = await IsSOFiltered(SalesEnum.SQNeedToRunning).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSQOutstandingAgainstSO()
        {
            using (_context)
            {
                IQueryable<SQOutstandingAgaintsSOParam> query = await CreateQuerySQOutstandingAgainstSO().ConfigureAwait(false);
                bool IsFiltered = await IsSQFiltered(SalesEnum.OutstandingSQAgainstSO).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSQOutstandingAgainstDO()
        {
            using (_context)
            {
                IQueryable<SQOutstandingAgaintsSOParam> query = await CreateQuerySQOutstandingAgainstDO().ConfigureAwait(false);
                bool IsFiltered = await IsSQFiltered(SalesEnum.OutstandingSQAgainstDO).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSOOutstandingAgainstDO()
        {
            using (_context)
            {
                IQueryable<SOOutstandingAgaintsDO> query = await CreateQuerySOOutstandingAgainstDO().ConfigureAwait(false);
                bool IsFiltered = await IsSQFiltered(SalesEnum.OutstandingSOAgainstDO).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        public async Task<object> CountSOOutstandingAgainstInvoice()
        {
            using (_context)
            {
                IQueryable<SOOutstandingAgaintsDO> query = await CreateQuerySOOutstandingAgainstInvoice().ConfigureAwait(false);
                bool IsFiltered = await IsSQFiltered(SalesEnum.OutstandingSOAgainstInvoice).ConfigureAwait(false);
                var count = await query.LongCountAsync().ConfigureAwait(false);
                return new
                {
                    Count = count,
                    IsFiltered
                };
            }
        }

        private async Task<IQueryable<TblSalesQuo>> CreateQuerySQNeedtoConfirm()
        {
            var query = from sq in _context.TblSalesQuo
                        join sqPart in _context.TblSalesQuoPart on sq.SalesQuoId equals sqPart.SalesQuoId
                        where sqPart != null
                        where sq.Deleted == false
                        where sq.StatusId == (short)StatusEnum.Entry || (sq.StatusId == (short)StatusEnum.Revise && sq.LastStatusId == (short)StatusEnum.Confirm)
                        group sq by sq.SalesQuoId into g
                        select g.FirstOrDefault();

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.SQNeedToConfirm
                             select setup;

            var salesProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (salesProcurement.DivisionId != null)
                query = query = query.Where(x => x.DivisionId == salesProcurement.DivisionId);
            if (salesProcurement.DepartmentId != null)
                query = query = query.Where(x => x.DepartmentId == salesProcurement.DepartmentId);
            if (salesProcurement.ProjectId != null)
                query = query = query.Where(x => x.ProjectId == salesProcurement.ProjectId);
            if (salesProcurement.RequestorId != null)
                query = query = query.Where(x => x.EmployeeId == salesProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<TblSalesQuo>> CreateQuerySQNeedtoRunning()
        {
            var query = from sq in _context.TblSalesQuo
                        where sq.Deleted == false && (sq.StatusId == (short)StatusEnum.Confirm) || (sq.StatusId == (short)StatusEnum.Revise && sq.LastStatusId == (short)StatusEnum.Running)
                        group sq by sq.SalesQuoId into g
                        select g.FirstOrDefault();

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.SQNeedToRunning
                             select setup;

            var salesProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (salesProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == salesProcurement.DivisionId);
            if (salesProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == salesProcurement.DepartmentId);
            if (salesProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == salesProcurement.ProjectId);
            if (salesProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == salesProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<TblSo>> CreateQuerySONeedtoConfirm()
        {
            var query = from so in _context.TblSo
                        join soPart in _context.TblSopart on so.Soid equals soPart.Soid
                        where soPart != null
                        where so.Deleted == false
                        where so.StatusId == (short)StatusEnum.Entry || (so.StatusId == (short)StatusEnum.Revise && so.LastStatusId == (short)StatusEnum.Confirm)
                        group so by so.Soid into g
                        select g.FirstOrDefault();

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.SONeedToConfirm
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<TblSo>> CreateQuerySONeedtoRunning()
        {
            var query = from so in _context.TblSo
                        where so.Deleted == false && (so.StatusId == (short)StatusEnum.Confirm) || (so.StatusId == (short)StatusEnum.Revise && so.LastStatusId == (short)StatusEnum.Running)
                        group so by so.Soid into g
                        select g.FirstOrDefault();

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.SONeedToRunning
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<SQOutstandingAgaintsSOParam>> CreateQuerySQOutstandingAgainstSO()
        {
            var query = from sq in _context.TblSalesQuo
                        join sqParts in _context.TblSalesQuoPart on sq.SalesQuoId equals sqParts.SalesQuoId into sqPartss
                        from sqPart in sqPartss.DefaultIfEmpty()

                        join soParts in _context.TblSopart on sqPart.SeqId equals soParts.RefSeqId into soPartss
                        from soPart in soPartss.DefaultIfEmpty()
                        join soHeaderx in _context.TblSo on soPart.Soid equals soHeaderx.Soid into soHeaderxx
                        from soHeader in soHeaderxx.DefaultIfEmpty()

                        where sq.Deleted == false && sq.StatusId == (short)StatusEnum.Running
                        let isSqPartNull = sqPart == null
                        let isSoPartNull = soPart == null
                        select new
                        {
                            sq.SalesQuoId,
                            sq.SalesQuoCd,
                            sq.SqtDate,
                            sq.EmployeeId,
                            sq.StatusId,
                            SQPartId = isSqPartNull ? 0 : sqPart.PartId,
                            SQQty = isSqPartNull ? 0 : sqPart.SqpQuantity,
                            IsSOValid = isSoPartNull ? false : (soHeader.Deleted == false && soHeader.StatusId == (short)StatusEnum.Running),
                            SOQty = isSoPartNull ? 0 : soPart.SopQuantity,
                            sq.DivisionId,
                            sq.DepartmentId,
                            sq.ProjectId,
                        } into sTransaction
                        group sTransaction by sTransaction.SalesQuoId into sTransactionG
                        let qtySQ = sTransactionG.FirstOrDefault().SQQty
                        let qtySO = sTransactionG.Where(w => w.IsSOValid).Sum(g => g.SOQty)
                        let osSO = qtySO - sTransactionG.Sum(x => x.SOQty)
                        let sqHeader = sTransactionG.FirstOrDefault()
                        select new SQOutstandingAgaintsSOParam
                        {
                            SalesQuoId = sqHeader.SalesQuoId,
                            SalesQuoCd = sqHeader.SalesQuoCd,
                            SqtDate = sqHeader.SqtDate,
                            EmployeeId = sqHeader.EmployeeId,
                            StatusId = sqHeader.StatusId,
                            DivisionId = sqHeader.DivisionId,
                            DepartmentId = sqHeader.DepartmentId,
                            ProjectId = sqHeader.ProjectId,
                            IsOutstanding = osSO > 0
                        };

            query = query.Where(w => w.IsOutstanding == true);

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.OutstandingSQAgainstSO
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<SQOutstandingAgaintsSOParam>> CreateQuerySQOutstandingAgainstDO()
        {
            var query = from sq in _context.TblSalesQuo
                        join sqParts in _context.TblSalesQuoPart on sq.SalesQuoId equals sqParts.SalesQuoId into sqPartss
                        from sqPart in sqPartss.DefaultIfEmpty()

                        join soParts in _context.TblSopart on sqPart.SeqId equals soParts.RefSeqId into soPartss
                        from soPart in soPartss.DefaultIfEmpty()
                        join soHeaders in _context.TblSo on soPart.Soid equals soHeaders.Soid into soHeaderss
                        from soHeader in soHeaderss.DefaultIfEmpty()

                        join doParts in _context.TblInvPart on soPart.SeqId equals doParts.RefSeqId into doPartss
                        from doPart in doPartss.DefaultIfEmpty()
                        join doHeaders in _context.TblInventory on doPart.InventoryId equals doHeaders.InventoryId into doHeaderss
                        from doHeader in doHeaderss.DefaultIfEmpty()

                        where sq.Deleted == false && sq.StatusId == (short)StatusEnum.Running && doHeader.InventoryTypeId == 6

                        let isSQPartNull = sqPart == null
                        let isSOPartNull = soPart == null
                        let isDOPartNull = doPart == null
                        select new
                        {
                            sq.SalesQuoId,
                            sq.SalesQuoCd,
                            sq.SqtDate,
                            sq.EmployeeId,
                            sq.StatusId,
                            SQPartId = isSQPartNull ? 0 : sqPart.PartId,
                            SQQty = isSQPartNull ? 0 : sqPart.SqpQuantity,
                            IsSOValid = isSOPartNull ? false : (soHeader.Deleted == false && soHeader.StatusId == (short)StatusEnum.Running),
                            DOQty = isDOPartNull ? 0 : doPart.InpQuantity,
                            IsDOValid = isDOPartNull ? false : (doHeader.Deleted == false && doHeader.StatusId == (short)StatusEnum.Running),

                        } into sTransaction

                        group sTransaction by sTransaction.SalesQuoId into sTransactionG
                        let qtySQ = sTransactionG.FirstOrDefault().SQQty
                        let qtyDO = sTransactionG.Where(w => w.IsDOValid && w.IsSOValid).Sum(g => g.DOQty)
                        let osDO = qtySQ - qtyDO
                        let sqHeader = sTransactionG.FirstOrDefault()
                        select new SQOutstandingAgaintsSOParam
                        {
                            SalesQuoId = sqHeader.SalesQuoId,
                            SalesQuoCd = sqHeader.SalesQuoCd,
                            SqtDate = sqHeader.SqtDate,
                            EmployeeId = sqHeader.EmployeeId,
                            StatusId = sqHeader.StatusId,
                            IsOutstanding = osDO > 0
                        };

            query = query.Where(w => w.IsOutstanding == true);

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.OutstandingSQAgainstDO
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<SOOutstandingAgaintsDO>> CreateQuerySOOutstandingAgainstDO()
        {
            var query = from so in _context.TblSo
                        join soParts in _context.TblSopart on so.Soid equals soParts.Soid into soPartss
                        from soPart in soPartss.DefaultIfEmpty()

                        join doParts in _context.TblInvPart on soPart.SeqId equals doParts.RefSeqId into doPartss
                        from doPart in doPartss.DefaultIfEmpty()
                        join doHeaders in _context.TblInventory on doPart.InventoryId equals doHeaders.InventoryId into doHeaderss
                        from doHeader in doHeaderss.DefaultIfEmpty()
                        where so.Deleted == false && so.StatusId == (short)StatusEnum.Running && doHeader.InventoryTypeId == 6
                         
                        let isSOPartNull = soPart == null
                        let isDOPartNull = doPart == null
                        select new
                        {
                            so.Soid,
                            so.Socd,
                            so.SoDate,
                            so.DivisionId,
                            so.DepartmentId,
                            so.ProjectId,
                            so.EmployeeId,
                            so.StatusId,
                            so.VendorId,
                            SOPartId = isSOPartNull ? 0 : soPart.PartId,
                            SOQty = isSOPartNull ? 0 : soPart.SopQuantity,
                            IsDOValid = isDOPartNull ? false : (doHeader.Deleted == false && doHeader.StatusId == (short)StatusEnum.Running),
                            DOQty = isDOPartNull ? 0 : doPart.InpQuantity
                        } into sTransaction
                        group sTransaction by sTransaction.Soid into sTransactionG
                        let qtySO = sTransactionG.FirstOrDefault().SOQty
                        let qtyDO = sTransactionG.Where(w => w.IsDOValid).Sum(g => g.DOQty)
                        let osSO = qtySO - qtyDO
                        let soHeader = sTransactionG.FirstOrDefault()
                        select new SOOutstandingAgaintsDO
                        {
                            Soid = soHeader.Soid,
                            Socd = soHeader.Socd,
                            SoDate = soHeader.SoDate,
                            DivisionId = soHeader.DivisionId,
                            DepartmentId = soHeader.DepartmentId,
                            ProjectId = soHeader.ProjectId,
                            EmployeeId = soHeader.EmployeeId,
                            StatusId = soHeader.StatusId,
                            VendorId = soHeader.VendorId,
                            IsOutstanding = osSO > 0
                        };

            query = query.Where(x => x.IsOutstanding);

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.OutstandingSOAgainstDO
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        private async Task<IQueryable<SOOutstandingAgaintsDO>> CreateQuerySOOutstandingAgainstInvoice()
        {
            var query = from so in _context.TblSo
                        join soParts in _context.TblSopart on so.Soid equals soParts.Soid into soPartss
                        from soPart in soPartss.DefaultIfEmpty()

                        join doParts in _context.TblInvPart on soPart.SeqId equals doParts.RefSeqId into doPartss
                        from doPart in doPartss.DefaultIfEmpty()
                        join doHeaders in _context.TblInventory on doPart.InventoryId equals doHeaders.InventoryId into doHeaderss
                        from doHeader in doHeaderss.DefaultIfEmpty()

                        join ivoParts in _context.TblIvoPart on doPart.SeqId equals ivoParts.RefSeqId into ivoPartss
                        from ivoPart in ivoPartss.DefaultIfEmpty()
                        join ivoHeaders in _context.TblInvoice on ivoPart.InvoiceId equals ivoHeaders.InvoiceId into ivoHeaderss
                        from ivoHeader in ivoHeaderss.DefaultIfEmpty()

                        where so.Deleted == false && so.StatusId == (short)StatusEnum.Running
                        let isSOPartNull = soPart == null
                        let isDOPartNull = doPart == null
                        let isInvoicePartNull = ivoPart == null
                        select new
                        {
                            so.Soid,
                            so.Socd,
                            so.SoDate,
                            so.DivisionId,
                            so.DepartmentId,
                            so.ProjectId,
                            so.EmployeeId,
                            so.StatusId,
                            so.VendorId,
                            SOPartId = isSOPartNull ? 0 : soPart.PartId,
                            SOQty = isSOPartNull ? 0 : soPart.SopQuantity,
                            IsDOValid = isDOPartNull ? false : (doHeader.Deleted == false && doHeader.StatusId == (short)StatusEnum.Running),

                            InvoiceQty = isInvoicePartNull ? 0 : ivoPart.IvpQuantity,
                            IsInvoiceValid = isInvoicePartNull ? false : (ivoHeader.Deleted == false && ivoHeader.StatusId == (short)StatusEnum.Running),

                        } into sTransaction

                        group sTransaction by sTransaction.Soid into sTransactionG
                        let qtySO = sTransactionG.FirstOrDefault().SOQty
                        let qtyInvoice = sTransactionG.Where(w => w.IsInvoiceValid&& w.IsDOValid).Sum(g => g.InvoiceQty)
                        let osInvoice = qtySO - qtyInvoice
                        let soHeader = sTransactionG.FirstOrDefault()
                        select new SOOutstandingAgaintsDO
                        {
                            Soid = soHeader.Soid,
                            Socd = soHeader.Socd,
                            SoDate = soHeader.SoDate,
                            DivisionId = soHeader.DivisionId,
                            DepartmentId = soHeader.DepartmentId,
                            ProjectId = soHeader.ProjectId,
                            EmployeeId = soHeader.EmployeeId,
                            StatusId = soHeader.StatusId,
                            VendorId = soHeader.VendorId,
                            IsOutstanding = osInvoice > 0
                        };

                query = query.Where(w => w.IsOutstanding == true);

            var querySetup = from setup in _context.TblDashboardProcurement
                             where setup.Id == (long)SalesEnum.OutstandingSOAgainstInvoice
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);

            return query;
        }

        public async Task<object> GetSalesDetail(SalesEnum param)
        {
            using (_context)
            {
                if (param == SalesEnum.SQNeedToConfirm)
                {
                    var query = await this.CreateQuerySQNeedtoConfirm().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on ((long)model.ProjectId) equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.SalesQuoId,
                                        Cd = model.SalesQuoCd,
                                        Date = model.SqtDate,
                                        Requestor = employee.EmpName,
                                        Status = status.StaName,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.SQNeedToRunning)
                {
                    var query = await this.CreateQuerySQNeedtoRunning().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on ((long) model.ProjectId) equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.SalesQuoId,
                                        Cd = model.SalesQuoCd,
                                        Date = model.SqtDate,
                                        Requestor = employee.EmpName,
                                        Status = status.StaName,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.OutstandingSQAgainstSO)
                {
                    var query = await this.CreateQuerySQOutstandingAgainstSO().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.SalesQuoId,
                                        Cd = model.SalesQuoCd,
                                        Date = model.SqtDate,
                                        Requestor = employee.EmpName,
                                        Status = status.StaName,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.OutstandingSQAgainstDO)
                {
                    var query = await this.CreateQuerySQOutstandingAgainstDO().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.SalesQuoId,
                                        Cd = model.SalesQuoCd,
                                        Date = model.SqtDate,
                                        Requestor = employee.EmpName,
                                        Status = status.StaName,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.SONeedToConfirm)
                {
                    var query = await this.CreateQuerySONeedtoConfirm().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.Soid,
                                        Cd = model.Socd,
                                        Date = model.SoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.SONeedToRunning)
                {
                    var query = await this.CreateQuerySONeedtoRunning().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.Soid,
                                        Cd = model.Socd,
                                        Date = model.SoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.OutstandingSOAgainstDO)
                {
                    var query = await this.CreateQuerySOOutstandingAgainstDO().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from division in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from department in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from project in proj.DefaultIfEmpty()
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from status in stat.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    select new 
                                    {
                                        Id = model.Soid,
                                        Cd = model.Socd,
                                        Date = model.SoDate,
                                        Division = division.DivName,
                                        Department = department.DepName,
                                        Project = project.ProName,
                                        Supplier = vendor.VndName,
                                        Status = status.StaName,
                                        Employee = employee.EmpName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else if (param == SalesEnum.OutstandingSOAgainstInvoice)
                {
                    var query = await this.CreateQuerySOOutstandingAgainstInvoice().ConfigureAwait(false);
                    var queryList = from model in query
                                    join div in _context.TblDivision on model.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in _context.TblDepartment on model.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in _context.TblProject on model.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in _context.TblVendor on model.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in _context.TblStatus on model.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    join emp in _context.TblEmployee on model.EmployeeId equals emp.EmployeeId into empl
                                    from employee in empl.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = model.Soid,
                                        Cd = model.Socd,
                                        Date = model.SoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName,
                                        Employee = employee.EmpName
                                    };

                    return await queryList.ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    throw new ApplicationException("Unsupported type");
                }
            }
        }

        private async Task<bool> IsSQFiltered(SalesEnum type)
        {
            var queryFil = from setup in _context.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }

        private async Task<bool> IsSOFiltered(SalesEnum type)
        {
            var queryFil = from setup in _context.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }
    }
}
