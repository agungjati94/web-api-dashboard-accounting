﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Dashboard.Database;

namespace Dashboard.Repository
{
    public class StockMonitoringRepository
    {
        private SofiContext mDBContext;

        public StockMonitoringRepository(SofiContext sofiContext)
        {
            mDBContext = sofiContext;
        }

        public async Task<object> GetReportStockMonitoring(StockMonitoringParam filter)
        {
            using (mDBContext)
            {
                var offset = (filter.Page - 1) * filter.Limit;
                var query = mDBContext.sfnStockMonitoring.FromSql($@"SELECT * from dbo.sfnStockMonitoring(
                    {filter.PartCD},
                    {filter.DivisionCD},
                    {filter.DepartmentCD},
                    {filter.ProjectCD},
                    {filter.WarehouseCD},
                    {filter.GroupingCD},
                    {filter.PartCat1CD},
                    {filter.PartCat2CD},
                    {filter.PartCat3CD},
                    {filter.VendorCD},
                    {filter.Priority},
                    {filter.TransDate}
                    )");
                var report = await query.OrderBy(x => x.partName)
                                    .Skip(offset)
                                    .Take(filter.Limit)
                                    .ToListAsync().ConfigureAwait(false);

                //var count = await query.CountAsync().ConfigureAwait(false);

                return new
                {
                    report,
                   // count
                };
            }
           
        }
    }
}
