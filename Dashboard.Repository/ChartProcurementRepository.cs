﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Dashboard.Database;

namespace Dashboard.Repository
{
    public class ChartProcurementRepository
    {
        private SofiContext mDBContext { get; set; }
        private IEnumerable<string>  periods;
        public ChartProcurementRepository(SofiContext sofiContext)
        {
            this.mDBContext = sofiContext;
            var today = DateTime.Today;
            this.periods = Enumerable.Range(0, 6)
                        .Select(s => today.AddMonths(-s).ToString("MMMM"));
        }

         

        public async Task<object> GetPONumber()
        {
            using (mDBContext)
            {
                var listMonth = await this.CreateQueryChartPO(ProcurementEnum.NumberofPO).ConfigureAwait(false);
                var queryResult = from queryRes in listMonth
                                  group queryRes by queryRes.PoDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().PoDate.ToString("MMMM"),
                                      Value = g.Count()
                                  };

                var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsPOFiltered(ProcurementEnum.NumberofPO).ConfigureAwait(false);
                var list = periods.Select(p =>
                {
                    var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                    return new { Name = p, Value = value };
                }).ToList();

                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        public async Task<object> GetMPRNumber()
        {
            using (mDBContext)
            {
                var listMonth = await this.CreateQueryChartMPR(ProcurementEnum.NumberofMPR).ConfigureAwait(false);
                var queryResult = from queryRes in listMonth
                                  group queryRes by queryRes.PrqDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().PrqDate.ToString("MMMM"),
                                      Value = g.Count()
                                  };

                var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsMPRFiltered(ProcurementEnum.NumberofMPR).ConfigureAwait(false);
                var list = periods.Select(p =>
                {
                    var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                    return new { Name = p, Value = value };
                }).ToList();

                return new { Data = list, IsFiltered = isFiltered };
            }
        }



        public async Task<object> GetChartTotalSupplier()
        {
            using (mDBContext)
            {
                var vndTypeIds = new[] { 0, 2 };
                var query = from supplier in mDBContext.TblVendor
                            join country in mDBContext.TblCountry on supplier.CountryId equals country.CountryId
                            where supplier.Deleted == false && country.Deleted == false && vndTypeIds.Contains(supplier.VndTypeId)
                            select new
                            {
                                supplier.VendorId,
                                country.CountryId,
                                country.CountryCd
                            } into s
                            group s by s.CountryId into g
                            select new
                            {
                                Name = g.FirstOrDefault().CountryCd,
                                Value = g.Select(x => x.VendorId).Count()
                            };

                var querySupp = from supplier in mDBContext.TblVendor
                                join country in mDBContext.TblCountry on supplier.CountryId equals country.CountryId
                                where supplier.Deleted == false && country.Deleted == false && vndTypeIds.Contains(supplier.VndTypeId)
                                select new
                                {
                                    supplier.VendorId,
                                    country.CountryId,
                                    country.CountryCd
                                };

                var totalSupp = await querySupp.LongCountAsync().ConfigureAwait(false);

                var list = await query.ToListAsync().ConfigureAwait(false);
                return new { Total = totalSupp, Data = list };
            }
        }

        

        public async Task<object> GetChartOfNumberSupplier()
        {
            using (mDBContext)
            {
                var query = await this.CreateQueryChartPO(ProcurementEnum.NumberofSupplier);
                var queryResult = from queryRes in query
                                  group queryRes by queryRes.PoDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().PoDate.ToString("MMMM"),
                                      Value = g.Select(x => x.VendorId).Distinct().Count()
                                  };

                var resultList = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsPOFiltered(ProcurementEnum.NumberofSupplier).ConfigureAwait(false);
                var list = periods.Select(p =>
                {
                    var value = (resultList.FirstOrDefault(r => r.Name == p) != null) ? resultList.FirstOrDefault(r => r.Name == p).Value : 0;
                    return new { Name = p, Value = value };
                }).ToList();

                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        public async Task<object> GetOpenPOValue()
        {
            using (mDBContext)
            {
                var query = await this.CreateQueryChartPO(ProcurementEnum.OpenPOValue);
                var queryResult = from queryRes in query
                                  join currency in mDBContext.TblCurrency on queryRes.CurrencyId equals currency.CurrencyId
                                  select new
                                  {
                                      queryRes.CurrencyId,
                                      currency.CurrencyCd,
                                      queryRes.PoNetAmt,
                                      queryRes.PoDate
                                  } into s
                                  group s by s.PoDate.Month into g
                                  select new
                                  {
                                      Name = g.FirstOrDefault().PoDate.ToString("MMMM"),
                                      Series = g.GroupBy(
                                          s => s.CurrencyId,
                                          s => s,
                                          (key, gg) => new { CurrencyId = key, Name = gg.FirstOrDefault().CurrencyCd, Value = gg.Sum(x => x.PoNetAmt) }
                                          ).ToList()
                                  };

                var list = await queryResult.ToListAsync().ConfigureAwait(false);
                var isFiltered = await this.IsPOFiltered(ProcurementEnum.OpenPOValue).ConfigureAwait(false);
                return new { Data = list, IsFiltered = isFiltered };
            }
        }

        private async Task<IQueryable<TblPo>> CreateQueryChartPO(ProcurementEnum type)
        {
            var today = DateTime.Today;
            var previous6Month = today.AddMonths(-6);
            var query = from po in mDBContext.TblPo
                        where po.Deleted == false && po.PoDate >= previous6Month && po.PoDate <= today && po.StatusId == (short)StatusEnum.Running
                        select po;

            return await SetupFilterPO(query, type).ConfigureAwait(false);
        }

        private async Task<IQueryable<TblPo>> SetupFilterPO(IQueryable<TblPo> query, ProcurementEnum type)
        {
            var querySetup = from setup in mDBContext.TblDashboardProcurement
                             where setup.Id == (long)type
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.PotypeId != null)
                query = query.Where(x => x.PotypeId == setupProcurement.PotypeId);
            if (setupProcurement.CurrencyId != null)
                query = query.Where(x => x.CurrencyId == setupProcurement.CurrencyId);
            return query;
        }

        private async Task<IQueryable<TblPurchReq>> CreateQueryChartMPR(ProcurementEnum type)
        {
            var today = DateTime.Today;
            var previous6Month = today.AddMonths(-6);
            var query = from mpr in mDBContext.TblPurchReq
                        where mpr.Deleted == false && mpr.PrqDate >= previous6Month && mpr.PrqDate <= today && mpr.StatusId == (short)StatusEnum.Running
                        select mpr;

            var debug = await query.ToListAsync().ConfigureAwait(false);
            return await SetupFilterMPR(query, type).ConfigureAwait(false);
        }

        private async Task<IQueryable<TblPurchReq>> SetupFilterMPR(IQueryable<TblPurchReq> query, ProcurementEnum type)
        {
            var querySetup = from setup in mDBContext.TblDashboardProcurement
                             where setup.Id == (long)type
                             select setup;

            var setupProcurement = await querySetup.FirstOrDefaultAsync().ConfigureAwait(false);
            if (setupProcurement.DivisionId != null)
                query = query.Where(x => x.DivisionId == setupProcurement.DivisionId);
            if (setupProcurement.DepartmentId != null)
                query = query.Where(x => x.DepartmentId == setupProcurement.DepartmentId);
            if (setupProcurement.ProjectId != null)
                query = query.Where(x => x.ProjectId == setupProcurement.ProjectId);
            if (setupProcurement.RequestorId != null)
                query = query.Where(x => x.EmployeeId == setupProcurement.RequestorId);
            if (setupProcurement.CurrencyId != null)
                query = query.Where(x => x.CurrencyId == setupProcurement.CurrencyId);
            return query;
        }



        private async Task<IQueryable<TblPo>> CreateQueryChartDetailPO(ProcurementEnum type, string month)
        {
            var query = from po in mDBContext.TblPo
                        where po.PoDate.ToString("MMMM") == month && po.Deleted == false && po.StatusId == (short)StatusEnum.Running
                        select po;
           return await SetupFilterPO(query, type).ConfigureAwait(false);
        }

        private async Task<IQueryable<TblPurchReq>> CreateQueryChartDetailMPR(ProcurementEnum type, string month)
        {
            var query = from mpr in mDBContext.TblPurchReq
                        where mpr.PrqDate.ToString("MMMM") == month && mpr.Deleted == false && mpr.StatusId == (short)StatusEnum.Running
                        select mpr;
            return await SetupFilterMPR(query, type).ConfigureAwait(false);
        }

        public async Task<object> GetChartDetail(ProcurementEnum type, string month)
        {
            using (mDBContext)
            {
                if (type == ProcurementEnum.NumberofPO)
                {
                    var query = await CreateQueryChartDetailPO(type, month).ConfigureAwait(false);
                    var queryList = from po in query
                                    join div in mDBContext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBContext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBContext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in mDBContext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in mDBContext.TblStatus on po.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = po.Poid,
                                        Cd = po.Pocd,
                                        Date = po.PoDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Supplier = vendor.VndName,
                                        Status = stats.StaName
                                    };

                    return await queryList.OrderBy(x => x.Cd).ToListAsync().ConfigureAwait(false);
                }
                else if (type == ProcurementEnum.NumberofMPR)
                {
                    var query = await CreateQueryChartDetailMPR(type, month).ConfigureAwait(false);
                    var queryList = from mpr in query
                                    join div in mDBContext.TblDivision on mpr.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBContext.TblDepartment on mpr.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBContext.TblProject on mpr.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join sta in mDBContext.TblStatus on mpr.StatusId equals sta.StatusId into stat
                                    from stats in stat.DefaultIfEmpty()
                                    join employees in mDBContext.TblEmployee on mpr.EmployeeId equals employees.EmployeeId into employeess
                                    from employee in employeess.DefaultIfEmpty()
                                    select new
                                    {
                                        Id = mpr.PurchReqId,
                                        Cd = mpr.PurchReqCd,
                                        Date = mpr.PrqDate,
                                        Division = divs.DivName,
                                        Department = deps.DepName,
                                        Project = projs.ProName,
                                        Status = stats.StaName,
                                        Employee = employee.EmpName
                                    };

                    return await queryList.OrderBy(x => x.Cd).ToListAsync().ConfigureAwait(false);
                }
                else if (type == ProcurementEnum.NumberofSupplier)
                {
                    var query = await CreateQueryChartDetailPO(type, month).ConfigureAwait(false);
                    var queryList = from po in query
                                    join div in mDBContext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                    from divs in divv.DefaultIfEmpty()
                                    join dep in mDBContext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                    from deps in depp.DefaultIfEmpty()
                                    join pro in mDBContext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                    from projs in proj.DefaultIfEmpty()
                                    join vend in mDBContext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                    from vendor in vendr.DefaultIfEmpty()
                                    join sta in mDBContext.TblStatus on po.StatusId equals sta.StatusId into stat
                                    join currency in mDBContext.TblCurrency on po.CurrencyId equals currency.CurrencyId
                                    from stats in stat.DefaultIfEmpty()
                                    select new
                                    {
                                        po.VendorId,
                                        Amount = po.PoNetAmt,
                                        Supplier = vendor.VndName,
                                        currency.CurrencyCd
                                    } into s
                                    group s by s.VendorId into g
                                    select new
                                    {
                                        g.FirstOrDefault().VendorId,
                                        g.FirstOrDefault().Supplier,
                                        Amount = g.Sum(x => x.Amount),
                                        Month = month,
                                        g.FirstOrDefault().CurrencyCd
                                    };

                    return await queryList.OrderByDescending(x => x.Amount).ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    throw new ApplicationException("Unsupported Type.");
                }
            }
        }

        public async Task<object> GetChartDetailOpenPOValue(string month, short currencyId)
        {
            using (mDBContext)
            {
                var queryRes = from po in mDBContext.TblPo
                               where po.PoDate.ToString("MMMM") == month && po.Deleted == false && po.StatusId == (short)StatusEnum.Running && po.CurrencyId == currencyId
                               select po;

                var query = await SetupFilterPO(queryRes, ProcurementEnum.OpenPOValue).ConfigureAwait(false);

                var queryList = from po in query
                                join div in mDBContext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                from divs in divv.DefaultIfEmpty()
                                join dep in mDBContext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                from deps in depp.DefaultIfEmpty()
                                join pro in mDBContext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                from projs in proj.DefaultIfEmpty()
                                join vend in mDBContext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                from vendor in vendr.DefaultIfEmpty()
                                join sta in mDBContext.TblStatus on po.StatusId equals sta.StatusId into stat
                                from stats in stat.DefaultIfEmpty()
                                join currency in mDBContext.TblCurrency on po.CurrencyId equals currency.CurrencyId
                                select new
                                {
                                    Id = po.Poid,
                                    Cd = po.Pocd,
                                    Date = po.PoDate,
                                    Supplier = vendor.VndName,
                                    CurrencyCd = currency.CurrencyCd,
                                    Amount = po.PoNetAmt
                                };

                return await queryList.OrderByDescending(x => x.Amount).ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetChartDetailNumberOfActiveSupplier(string month, long vendorId)
        {
            using (mDBContext)
            {
                var query = await CreateQueryChartDetailPO(ProcurementEnum.NumberofSupplier, month).ConfigureAwait(false);
                var queryList = from po in query
                                join div in mDBContext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                from divs in divv.DefaultIfEmpty()
                                join dep in mDBContext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                from deps in depp.DefaultIfEmpty()
                                join pro in mDBContext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                from projs in proj.DefaultIfEmpty()
                                join vend in mDBContext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                from vendor in vendr.DefaultIfEmpty()
                                join sta in mDBContext.TblStatus on po.StatusId equals sta.StatusId into stat
                                from stats in stat.DefaultIfEmpty()
                                where po.VendorId == vendorId
                                select new
                                {
                                    Id = po.Poid,
                                    Cd = po.Pocd,
                                    Date = po.PoDate,
                                    Division = divs.DivName,
                                    Department = deps.DepName,
                                    Project = projs.ProName,
                                    Supplier = vendor.VndName,
                                    Status = stats.StaName,
                                    Amount = po.PoNetAmt
                                };

                return await queryList.OrderByDescending(x => x.Amount).ToListAsync().ConfigureAwait(false);
            }
        }

        private async Task<bool> IsPOFiltered(ProcurementEnum type)
        {
            var queryFil = from setup in mDBContext.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null || setup.PotypeId != null || setup.CurrencyId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }

        private async Task<bool> IsMPRFiltered(ProcurementEnum type)
        {
            var queryFil = from setup in mDBContext.TblDashboardProcurement
                           where setup.Id == (long)type && (setup.DivisionId != null ||
                           setup.DepartmentId != null || setup.ProjectId != null || setup.RequestorId != null || setup.CurrencyId != null)
                           select setup;
            var IsFiltered = await queryFil.AnyAsync().ConfigureAwait(false);
            return IsFiltered;
        }

        public async Task<object> GetDetailTotalSupplierByCountry(string countryCd)
        {
            using (mDBContext)
            {
                var vndTypeIds = new[] { 0, 2 };
                var query = from supplier in mDBContext.TblVendor
                            join country in mDBContext.TblCountry on supplier.CountryId equals country.CountryId
                            where supplier.Deleted == false && country.Deleted == false && country.CountryCd == countryCd
                            where vndTypeIds.Contains(supplier.VndTypeId)
                            select new
                            {
                                supplier.VendorId,
                                supplier.VndName,
                                supplier.VndAddress,
                                supplier.VndPhone,
                                supplier.VndContact
                            };

                return await query.OrderBy(x => x.VndName).ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<object> GetDetailTotalSupplierPOByCountryVendor(long vendorId, int page, int limit)
        {
            using (mDBContext)
            {
                var queryList = from po in mDBContext.TblPo
                                join div in mDBContext.TblDivision on po.DivisionId equals div.DivisionId into divv
                                from divs in divv.DefaultIfEmpty()
                                join dep in mDBContext.TblDepartment on po.DepartmentId equals dep.DepartmentId into depp
                                from deps in depp.DefaultIfEmpty()
                                join pro in mDBContext.TblProject on po.ProjectId equals pro.ProjectId into proj
                                from projs in proj.DefaultIfEmpty()
                                join vend in mDBContext.TblVendor on po.VendorId equals vend.VendorId into vendr
                                from vendor in vendr.DefaultIfEmpty()
                                join sta in mDBContext.TblStatus on po.StatusId equals sta.StatusId into stat
                                from stats in stat.DefaultIfEmpty()
                                where po.Deleted == false && po.VendorId == vendorId && po.StatusId == (long)StatusEnum.Running
                                select new
                                {
                                    Id = po.Poid,
                                    Cd = po.Pocd,
                                    Date = po.PoDate,
                                    Division = divs.DivName,
                                    Department = deps.DepName,
                                    Project = projs.ProName,
                                    Supplier = vendor.VndName,
                                    Status = stats.StaName
                                };

                var total = await queryList.LongCountAsync().ConfigureAwait(false);
                var Data = await queryList.OrderBy(x => x.Cd).Skip((page - 1) * limit).Take(limit).ToListAsync().ConfigureAwait(false);
                var Header = new { Total = total, Limit = limit, Page = page };
                return new { Data, Header };
            }
        }
    }
}
