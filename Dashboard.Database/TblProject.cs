﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblProject
    {
        public long ProjectId { get; set; }
        public string ProjectCd { get; set; }
        public string ProName { get; set; }
        public int? ProCatId1 { get; set; }
        public int? ProCatId2 { get; set; }
        public int? ProCatId3 { get; set; }
        public DateTime ProDate { get; set; }
        public DateTime ProStartDate { get; set; }
        public DateTime ProEndDate { get; set; }
        public string ProNote { get; set; }
        public int EmployeeId { get; set; }
        public string VndName { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public bool? Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string ProViewName { get; set; }
        public bool? Budget { get; set; }
        public short PartControl { get; set; }
    }
}
