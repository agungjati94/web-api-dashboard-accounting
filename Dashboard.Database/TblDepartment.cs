﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblDepartment
    {
        public int DepartmentId { get; set; }
        public string DepartmentCd { get; set; }
        public string DepName { get; set; }
        public string DepNote { get; set; }
        public bool? DepDiscontinue { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public int? DivisionId { get; set; }
        public string DepViewName { get; set; }
    }
}
