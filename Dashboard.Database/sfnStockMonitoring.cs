﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Database
{
    public class sfnStockMonitoring
    {
        [Key]
        public string partCD {get; set;}
        public long RowNum { get; set; }
        public string partName {get; set; }
        public string DivisionCD{get; set;}
        public string DepartmentCD{get; set;}
        public string ProjectCD{get; set;}
        public string Priority{get; set;}
        public string WarehouseCD{get; set;}
        public string GroupingCD{get; set;}
        public string PartCat1{get; set;}
        public string PartCat2{get; set;}
        public string PartCat3{get; set;}
        public string VendorCD{get; set;}
        public decimal? prvMinOrder{get; set;}
        public decimal? pslMinStockQty{get; set;}
        public decimal? pslMaxStockQty{get; set;}
        public decimal? InvQty {get; set;}
        public decimal? OrderQty{get; set;}
        public decimal? BookQty{get; set;} // int
        public string UnitCD{get; set;}
        public decimal? StockExpected{get; set;}
        public decimal?  ProdAvailable{get; set;}
    }
}
