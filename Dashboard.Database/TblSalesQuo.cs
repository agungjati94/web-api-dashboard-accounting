﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblSalesQuo
    {
        public long SalesQuoId { get; set; }
        public string SalesQuoCd { get; set; }
        public short BranchId { get; set; }
        public DateTime SqtDate { get; set; }
        public int VendorId { get; set; }
        public string SqtAttn { get; set; }
        public string SqtCc { get; set; }
        public string SqtLeadTime { get; set; }
        public byte SqtLeadTimeType { get; set; }
        public short TermPmtId { get; set; }
        public short TermDelId { get; set; }
        public string SqtValidity { get; set; }
        public byte SqtValidityType { get; set; }
        public string SqtWarranty { get; set; }
        public int EmployeeId { get; set; }
        public short CurrencyId { get; set; }
        public decimal SqtGrossAmt { get; set; }
        public decimal SqtDiscPct { get; set; }
        public decimal SqtDiscPctAmt { get; set; }
        public decimal SqtDiscAmt { get; set; }
        public string SqtDiscFml { get; set; }
        public string SqtDiscFmlDisp { get; set; }
        public decimal SqtDiscFmlAmt { get; set; }
        public decimal SqtDiscount { get; set; }
        public decimal SqtTaxAmt1st { get; set; }
        public decimal SqtTaxAmt2nd { get; set; }
        public decimal SqtTaxAmt { get; set; }
        public decimal SqtChargeAmt { get; set; }
        public decimal SqtNetAmt { get; set; }
        public short TaxId { get; set; }
        public string SqtNote { get; set; }
        public string SqtDocNo1 { get; set; }
        public string SqtDocNo2 { get; set; }
        public string SqtDocNo3 { get; set; }
        public DateTime? SqtDocDate1 { get; set; }
        public DateTime? SqtDocDate2 { get; set; }
        public DateTime? SqtDocDate3 { get; set; }
        public bool SqtDisableTrig { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public bool Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public int? ProjectId { get; set; }
        public int? DepartmentId { get; set; }
        public int? DivisionId { get; set; }
        public string EntryName { get; set; }
        public string ConfirmName { get; set; }
        public string VerifyName { get; set; }
        public string RunName { get; set; }
        public DateTime? EntryTime { get; set; }
        public DateTime? ConfirmTime { get; set; }
        public DateTime? VerifyTime { get; set; }
        public DateTime? RunTime { get; set; }
        public string EntryCompName { get; set; }
        public string ConfirmCompName { get; set; }
        public string VerifyCompName { get; set; }
        public string RunCompName { get; set; }
    }
}
