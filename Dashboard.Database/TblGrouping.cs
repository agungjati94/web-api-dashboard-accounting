﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblGrouping
    {
        public short GroupingId { get; set; }
        public string GroupingCd { get; set; }
        public string GngName { get; set; }
        public byte GngType { get; set; }
        public byte GngLevel { get; set; }
        public short ParentId { get; set; }
        public string GngBitCodePrefix { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string GngViewName { get; set; }
    }
}
