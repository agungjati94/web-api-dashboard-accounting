﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblInvoice
    {
        public long InvoiceId { get; set; }
        public string InvoiceCd { get; set; }
        public short InvoiceTypeId { get; set; }
        public short? JournalTypeId { get; set; }
        public string JournalCd { get; set; }
        public short BranchId { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int DepartmentId { get; set; }
        public int VendorId { get; set; }
        public int BillVendorId { get; set; }
        public int Coaid { get; set; }
        public int EmployeeId { get; set; }
        public short CurrencyId { get; set; }
        public short TermPmtId { get; set; }
        public short TermDelId { get; set; }
        public short ShipAgentId { get; set; }
        public long? IvoReference { get; set; }
        public string IvoDeliverTo { get; set; }
        public string IvoRequestor { get; set; }
        public DateTime IvoDate { get; set; }
        public DateTime IvoDocDate { get; set; }
        public DateTime? IvoDueDate { get; set; }
        public DateTime? IvoEarlyDiscDate { get; set; }
        public decimal IvoEarlyDiscPct { get; set; }
        public decimal IvoEarlyDiscAmt { get; set; }
        public string IvoTaxInvoiceNo { get; set; }
        public DateTime? IvoTaxInvoiceDate { get; set; }
        public decimal IvoGrossAmt { get; set; }
        public decimal IvoDiscPct { get; set; }
        public decimal IvoDiscPctAmt { get; set; }
        public decimal IvoDiscAmt { get; set; }
        public string IvoDiscFml { get; set; }
        public string IvoDiscFmlDisp { get; set; }
        public decimal IvoDiscFmlAmt { get; set; }
        public decimal IvoDiscount { get; set; }
        public decimal IvoTaxAmt1st { get; set; }
        public decimal IvoTaxAmt2nd { get; set; }
        public decimal IvoTaxAmt { get; set; }
        public decimal IvoChargeAmt { get; set; }
        public decimal IvoPrepaidAmt { get; set; }
        public decimal IvoNetAmt { get; set; }
        public decimal IvoTotNetAmt { get; set; }
        public short TaxId { get; set; }
        public string IvoNote { get; set; }
        public string IvoDocNo1 { get; set; }
        public string IvoDocNo2 { get; set; }
        public string IvoDocNo3 { get; set; }
        public DateTime? IvoDocDate1 { get; set; }
        public DateTime? IvoDocDate2 { get; set; }
        public DateTime? IvoDocDate3 { get; set; }
        public long? TimeBillId { get; set; }
        public bool? IvoDisableTrig { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public bool? Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public decimal IvoInstallmentAmt { get; set; }
    }
}
