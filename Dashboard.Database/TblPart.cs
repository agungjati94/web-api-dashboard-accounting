﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPart
    {
        public int PartId { get; set; }
        public short PartTypeId { get; set; }
        public string PartCd { get; set; }
        public string DrawingId { get; set; }
        public short GroupingId { get; set; }
        public int? PrtCatId1 { get; set; }
        public int? PrtCatId2 { get; set; }
        public int? PrtCatId3 { get; set; }
        public string PrtName { get; set; }
        public string PrtBuyName { get; set; }
        public string PrtSellName { get; set; }
        public string PrtNumber { get; set; }
        public string PrtDimension { get; set; }
        public decimal? PrtDimPitch { get; set; }
        public decimal? PrtDimWide { get; set; }
        public decimal? PrtDimThick { get; set; }
        public decimal? PrtDimWeight { get; set; }
        public short? FreightChgId { get; set; }
        public short? ChgUnitId { get; set; }
        public short? WarehouseId { get; set; }
        public decimal PrtCubic { get; set; }
        public decimal PrtWeight { get; set; }
        public short? ChgCurrencyId { get; set; }
        public decimal PrtChgRate { get; set; }
        public long? PrtLot { get; set; }
        public string PrtSpecification { get; set; }
        public short UnitId { get; set; }
        public short PounitId { get; set; }
        public short SounitId { get; set; }
        public short ViewUnitId { get; set; }
        public short CurrencyId { get; set; }
        public decimal PrtPoprice { get; set; }
        public decimal PrtSafetyStock { get; set; }
        public decimal? PrtMaxStock { get; set; }
        public string PrtSerialNo { get; set; }
        public string PrtRegisteredNo { get; set; }
        public decimal? PrtReject { get; set; }
        public decimal? PrtMinOrder { get; set; }
        public int? PrtBoxContent { get; set; }
        public decimal PrtLeadTime { get; set; }
        public string PrtNote { get; set; }
        public byte? PrtCostMethod { get; set; }
        public int PrtSalesCoaid { get; set; }
        public int PrtSalesDiscCoaid { get; set; }
        public int PrtSalesRetCoaid { get; set; }
        public int PrtPurchCoaid { get; set; }
        public int PrtPurchDiscCoaid { get; set; }
        public int PrtInvCoaid { get; set; }
        public int PrtWipcoaid { get; set; }
        public int PrtCogscoaid { get; set; }
        public int PrtIssueCoaid { get; set; }
        public int PrtReceiptCoaid { get; set; }
        public int PrtConsignmentInCoaid { get; set; }
        public int PrtConsignmentOutCoaid { get; set; }
        public bool? PrtTaxable { get; set; }
        public bool? PrtSerialize { get; set; }
        public byte? PrtRating { get; set; }
        public int? PrtRefId { get; set; }
        public bool? PrtDiscontinue { get; set; }
        public string PrtExtField1 { get; set; }
        public string PrtExtField2 { get; set; }
        public string PrtExtField3 { get; set; }
        public string PrtExtField4 { get; set; }
        public string PrtExtField5 { get; set; }
        public string PrtBomversionId { get; set; }
        public bool? PrtDobitControl { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public short? WarehouseDefault { get; set; }
        public long? Uweight { get; set; }
        public long? MatType { get; set; }
        public string PrtViewName { get; set; }
    }
}
