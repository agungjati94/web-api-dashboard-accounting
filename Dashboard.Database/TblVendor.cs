﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblVendor
    {
        public int VendorId { get; set; }
        public string VendorCd { get; set; }
        public short VndTypeId { get; set; }
        public int? VndCatId1 { get; set; }
        public int? VndCatId2 { get; set; }
        public int? VndCatId3 { get; set; }
        public int? VndCatId4 { get; set; }
        public int? VndCatId5 { get; set; }
        public string VndName { get; set; }
        public string VndAddress { get; set; }
        public string VndQuoteAddress { get; set; }
        public string VndOrderAddress { get; set; }
        public string VndShipAddress { get; set; }
        public string VndBillAddress { get; set; }
        public string VndMailAddress { get; set; }
        public short CityId { get; set; }
        public short ProvinceId { get; set; }
        public short ZoneId { get; set; }
        public string VndZip { get; set; }
        public short CountryId { get; set; }
        public string VndPhZone { get; set; }
        public string VndPhone { get; set; }
        public string VndContact { get; set; }
        public string VndAttn { get; set; }
        public string VndCc { get; set; }
        public string VndFax { get; set; }
        public string VndEmail { get; set; }
        public string VndWebSite { get; set; }
        public string VndNpp { get; set; }
        public string VndNpwp { get; set; }
        public bool? VndIsWapu { get; set; }
        public short? TaxId { get; set; }
        public short? TermPmtId { get; set; }
        public short? TermDelId { get; set; }
        public short? CurrencyId { get; set; }
        public int? EmployeeId { get; set; }
        public short? ShipAgentId { get; set; }
        public short? PriceCatId { get; set; }
        public decimal VndDiscPct { get; set; }
        public string VndDiscFml { get; set; }
        public string VndDiscFmlDisp { get; set; }
        public byte VndCreditLimitType { get; set; }
        public decimal VndCreditLimit { get; set; }
        public bool? VndCreditTerm { get; set; }
        public short? BankId { get; set; }
        public string VndBankAccNo { get; set; }
        public short? PmtTypeId { get; set; }
        public DateTime? VndDob { get; set; }
        public bool? VndDobalert { get; set; }
        public byte? VndSex { get; set; }
        public string VndOccupation { get; set; }
        public string VndMobile { get; set; }
        public bool? VndNotifyByEmail { get; set; }
        public byte? VndMemberType { get; set; }
        public string VndPassword { get; set; }
        public byte? VndRating { get; set; }
        public string VndPhoto { get; set; }
        public string VndNote { get; set; }
        public short? WarehouseId { get; set; }
        public bool? VndDiscontinue { get; set; }
        public string VndExtField1 { get; set; }
        public string VndExtField2 { get; set; }
        public string VndExtField3 { get; set; }
        public string VndExtField4 { get; set; }
        public string VndExtField5 { get; set; }
        public string VndBitCodePrefix { get; set; }
        public bool? VndPartLimit { get; set; }
        public byte VndSalesPriceControl { get; set; }
        public bool? VndPurchAutoMarkup { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string VndPlaceBirth { get; set; }
        public string VndIdentityNo { get; set; }
        public string VndViewName { get; set; }
    }
}
