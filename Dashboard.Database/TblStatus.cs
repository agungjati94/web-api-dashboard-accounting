﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblStatus
    {
        public short StatusId { get; set; }
        public string StaName { get; set; }
        public bool StaPrint { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
    }
}
