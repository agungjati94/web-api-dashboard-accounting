﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPotype
    {
        public short PotypeId { get; set; }
        public string PotypeCd { get; set; }
        public string PotName { get; set; }
        public bool? PotConsignment { get; set; }
        public bool? Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string PotViewName { get; set; }
        public string AnsPrefix { get; set; }
        public int AnsStart { get; set; }
        public byte AnsDigit { get; set; }
        public byte AnsInitType { get; set; }
        public byte AnsMode { get; set; }
        public short AnsLoc { get; set; }
    }
}
