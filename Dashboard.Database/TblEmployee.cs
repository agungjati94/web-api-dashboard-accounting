﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblEmployee
    {
        public int EmployeeId { get; set; }
        public string EmployeeCd { get; set; }
        public string EmpName { get; set; }
        public int EmpParentId { get; set; }
        public string EmpJobTitle { get; set; }
        public string EmpEmail { get; set; }
        public string EmpPhone { get; set; }
        public bool? EmpSales { get; set; }
        public bool? EmpPurch { get; set; }
        public bool? EmpStore { get; set; }
        public bool? EmpProject { get; set; }
        public bool? EmpDiscontinue { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string EmpViewName { get; set; }
    }
}
