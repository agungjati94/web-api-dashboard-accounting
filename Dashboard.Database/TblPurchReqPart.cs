﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPurchReqPart
    {
        public long PurchReqId { get; set; }
        public Guid SeqId { get; set; }
        public short? PrpSeq { get; set; }
        public int PartId { get; set; }
        public string PrpName { get; set; }
        public short UnitId { get; set; }
        public decimal PrpQuantity { get; set; }
        public decimal PrpAppQty { get; set; }
        public decimal PrpPrice { get; set; }
        public DateTime? PrpReqDate { get; set; }
        public string PrpNote { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
    }
}
