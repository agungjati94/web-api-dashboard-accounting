﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblIvoPart
    {
        public long InvoiceId { get; set; }
        public Guid SeqId { get; set; }
        public short? IvpSeq { get; set; }
        public long RefId { get; set; }
        public Guid RefSeqId { get; set; }
        public long LotId { get; set; }
        public int PartId { get; set; }
        public int Coaid { get; set; }
        public short UnitId { get; set; }
        public int RoutingId { get; set; }
        public short WarehouseId { get; set; }
        public decimal IvpQuantity { get; set; }
        public decimal IvpBaseQuantity { get; set; }
        public decimal IvpPrice { get; set; }
        public decimal IvpDiscPct { get; set; }
        public decimal IvpDiscPctAmt { get; set; }
        public decimal IvpDiscAmt { get; set; }
        public string IvpDiscFml { get; set; }
        public string IvpDiscFmlDisp { get; set; }
        public decimal? IvpDiscFmlAmt { get; set; }
        public decimal IvpTaxAmt { get; set; }
        public decimal IvpDiscount { get; set; }
        public decimal IvpChargeAmt { get; set; }
        public decimal IvpPortion { get; set; }
        public decimal IvpAdjAmt { get; set; }
        public decimal IvpAmount { get; set; }
        public decimal IvpGrossAmt { get; set; }
        public string IvpExtField1 { get; set; }
        public string IvpExtField2 { get; set; }
        public string IvpExtField3 { get; set; }
        public string IvpExtField4 { get; set; }
        public string IvpExtField5 { get; set; }
        public string IvpNote { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
        public long? UweightId { get; set; }
        public string IvopViwPartName { get; set; }
    }
}
