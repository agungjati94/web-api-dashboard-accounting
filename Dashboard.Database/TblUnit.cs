﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblUnit
    {
        public short UnitId { get; set; }
        public string UnitCd { get; set; }
        public string UniName { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string UniViewName { get; set; }
    }
}
