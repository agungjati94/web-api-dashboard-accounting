﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblCurrency
    {
        public short CurrencyId { get; set; }
        public string CurrencyCd { get; set; }
        public string CurName { get; set; }
        public string CurSymbol { get; set; }
        public short CurDecPlace { get; set; }
        public short CurRoundType { get; set; }
        public short CurRoundTo { get; set; }
        public short CurRoundDigit { get; set; }
        public decimal CurUnit { get; set; }
        public short CurAdjustVarLmt { get; set; }
        public bool CurBase { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string CurViewName { get; set; }
    }
}
