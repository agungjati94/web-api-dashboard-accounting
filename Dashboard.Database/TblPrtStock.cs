﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPrtStock
    {
        public long PrtStockId { get; set; }
        public string MonthId { get; set; }
        public int Bompart { get; set; }
        public int Bomrouting { get; set; }
        public int PartId { get; set; }
        public int RoutingId { get; set; }
        public long LotId { get; set; }
        public long BatchId { get; set; }
        public long Mpsid { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int DepartmentId { get; set; }
        public short WarehouseId { get; set; }
        public byte RefType { get; set; }
        public short RefTypeId { get; set; }
        public long RefId { get; set; }
        public Guid RefSeqId { get; set; }
        public DateTime PrsDate { get; set; }
        public decimal PrsIn { get; set; }
        public decimal PrsOut { get; set; }
        public decimal PrsUse { get; set; }
        public decimal PrsResult { get; set; }
        public decimal? PrsBalance { get; set; }
        public string PrsRemark { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public long? UweightId { get; set; }
    }
}
