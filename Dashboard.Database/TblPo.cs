﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPo
    {
        public long Poid { get; set; }
        public short BranchId { get; set; }
        public string Pocd { get; set; }
        public short PotypeId { get; set; }
        public short? JournalTypeId { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int? DepartmentId { get; set; }
        public int VendorId { get; set; }
        public int? Coaid { get; set; }
        public int EmployeeId { get; set; }
        public int RequestId { get; set; }
        public short TermPmtId { get; set; }
        public short TermDelId { get; set; }
        public short ShipAgentId { get; set; }
        public string PoDeliverTo { get; set; }
        public string PoContact { get; set; }
        public DateTime PoDate { get; set; }
        public DateTime PoReqDate { get; set; }
        public string PoReqDesc { get; set; }
        public long PurchQuoId { get; set; }
        public short CurrencyId { get; set; }
        public decimal PoGrossAmt { get; set; }
        public decimal PoDiscPct { get; set; }
        public decimal PoDiscPctAmt { get; set; }
        public decimal PoDiscAmt { get; set; }
        public string PoDiscFml { get; set; }
        public string PoDiscFmlDisp { get; set; }
        public decimal PoDiscFmlAmt { get; set; }
        public decimal PoDiscount { get; set; }
        public short TaxId { get; set; }
        public decimal PoTaxAmt1st { get; set; }
        public decimal PoTaxAmt2nd { get; set; }
        public decimal PoTaxAmt { get; set; }
        public decimal PoChargeAmt { get; set; }
        public decimal PoPrepaidAmt { get; set; }
        public decimal PoNetAmt { get; set; }
        public decimal PoTotNetAmt { get; set; }
        public string PoDocNo1 { get; set; }
        public string PoDocNo2 { get; set; }
        public string PoDocNo3 { get; set; }
        public DateTime? PoDocDate1 { get; set; }
        public DateTime? PoDocDate2 { get; set; }
        public DateTime? PoDocDate3 { get; set; }
        public bool? PoDisableTrig { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public string PoNote { get; set; }
        public bool? Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public string EntryName { get; set; }
        public string ConfirmName { get; set; }
        public string RunName { get; set; }
        public DateTime? EntryTime { get; set; }
        public DateTime? ConfirmTime { get; set; }
        public DateTime? RunTime { get; set; }
        public string VerifyName { get; set; }
        public DateTime? VerifyTime { get; set; }
        public string EntryCompName { get; set; }
        public string ConfirmCompName { get; set; }
        public string VerifyCompName { get; set; }
        public string RunCompName { get; set; }
    }
}
