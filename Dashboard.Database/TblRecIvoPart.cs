﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblRecIvoPart
    {
        public long RecInvoiceId { get; set; }
        public Guid SeqId { get; set; }
        public int? RipSeq { get; set; }
        public long RefId { get; set; }
        public Guid RefSeqId { get; set; }
        public long LotId { get; set; }
        public int PartId { get; set; }
        public int Coaid { get; set; }
        public short UnitId { get; set; }
        public int RoutingId { get; set; }
        public short WarehouseId { get; set; }
        public decimal RipQuantity { get; set; }
        public decimal RipBaseQuantity { get; set; }
        public decimal RipPrice { get; set; }
        public decimal RipDiscPct { get; set; }
        public decimal RipDiscPctAmt { get; set; }
        public decimal RipDiscAmt { get; set; }
        public string RipDiscFml { get; set; }
        public string RipDiscFmlDisp { get; set; }
        public decimal RipDiscFmlAmt { get; set; }
        public decimal RipTaxAmt { get; set; }
        public decimal RipDiscount { get; set; }
        public decimal RipChargeAmt { get; set; }
        public decimal RipAddChargeAmt { get; set; }
        public decimal RipPortion { get; set; }
        public decimal RipAdjAmt { get; set; }
        public decimal RipAmount { get; set; }
        public decimal RipGrossAmt { get; set; }
        public short? FreightChgId { get; set; }
        public decimal RipFreightChgAmt { get; set; }
        public short? Insur1ChgId { get; set; }
        public decimal RipInsur1ChgAmt { get; set; }
        public short? Insur2ChgId { get; set; }
        public decimal RipInsur2ChgAmt { get; set; }
        public short? Duty1ChgId { get; set; }
        public decimal RipDuty1ChgAmt { get; set; }
        public short? Duty2ChgId { get; set; }
        public decimal RipDuty2ChgAmt { get; set; }
        public short? Tax1ChgId { get; set; }
        public decimal RipTax1ChgAmt { get; set; }
        public short? Tax2ChgId { get; set; }
        public decimal RipTax2ChgAmt { get; set; }
        public short? Oth1ChgId { get; set; }
        public decimal RipOth1ChgAmt { get; set; }
        public short? Oth2ChgId { get; set; }
        public decimal RipOth2ChgAmt { get; set; }
        public decimal? RipTotChgAmt { get; set; }
        public string RipExtField1 { get; set; }
        public string RipExtField2 { get; set; }
        public string RipExtField3 { get; set; }
        public string RipExtField4 { get; set; }
        public string RipExtField5 { get; set; }
        public string RipNote { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
        public long? UweightId { get; set; }
        public string RipViwPartName { get; set; }
    }
}
