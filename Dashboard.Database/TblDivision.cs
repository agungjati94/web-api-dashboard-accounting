﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblDivision
    {
        public short DivisionId { get; set; }
        public string DivisionCd { get; set; }
        public string DivName { get; set; }
        public int? DepartmentId { get; set; }
        public bool? DivDiscontinue { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string DivViewName { get; set; }
    }
}
