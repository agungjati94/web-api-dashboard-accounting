﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblDashboardProcurement
    {
        public long Id { get; set; }
        public string Cd { get; set; }
        public long? DivisionId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ProjectId { get; set; }
        public long? RequestorId { get; set; }
        public long? PotypeId { get; set; }
        public long? CurrencyId { get; set; }
        public long? StatusId { get; set; }
    }
}
