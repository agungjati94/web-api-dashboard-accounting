﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblInventory
    {
        public long InventoryId { get; set; }
        public short BranchId { get; set; }
        public string InventoryCd { get; set; }
        public short InventoryTypeId { get; set; }
        public short? JournalTypeId { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int DepartmentId { get; set; }
        public int VendorId { get; set; }
        public int BillVendorId { get; set; }
        public int Coaid { get; set; }
        public int EmployeeId { get; set; }
        public DateTime InvDate { get; set; }
        public DateTime? InvDocDate { get; set; }
        public string InvDeliverTo { get; set; }
        public long? InvReference { get; set; }
        public string InvDocument1 { get; set; }
        public string InvDocument2 { get; set; }
        public string InvDocument3 { get; set; }
        public short TermPmtId { get; set; }
        public short TermDelId { get; set; }
        public short CurrencyId { get; set; }
        public short ShipAgentId { get; set; }
        public DateTime? InvDueDate { get; set; }
        public DateTime? InvEarlyDiscDate { get; set; }
        public decimal InvEarlyDiscPct { get; set; }
        public decimal InvEarlyDiscAmt { get; set; }
        public decimal InvGrossAmt { get; set; }
        public decimal InvDiscPct { get; set; }
        public decimal InvDiscPctAmt { get; set; }
        public decimal InvDiscAmt { get; set; }
        public string InvDiscFml { get; set; }
        public string InvDiscFmlDisp { get; set; }
        public decimal InvDiscFmlAmt { get; set; }
        public decimal InvDiscount { get; set; }
        public short TaxId { get; set; }
        public decimal InvTaxAmt1st { get; set; }
        public decimal InvTaxAmt2nd { get; set; }
        public decimal InvTaxAmt { get; set; }
        public decimal InvChargeAmt { get; set; }
        public decimal InvPrepaidAmt { get; set; }
        public decimal InvNetAmt { get; set; }
        public decimal InvTotNetAmt { get; set; }
        public decimal InvPaidAmt { get; set; }
        public decimal InvChangeAmt { get; set; }
        public string InvNote { get; set; }
        public string InvDocNo1 { get; set; }
        public string InvDocNo2 { get; set; }
        public string InvDocNo3 { get; set; }
        public string InvDocNo4 { get; set; }
        public string InvDocNo5 { get; set; }
        public string InvDocNo6 { get; set; }
        public string InvDocNo7 { get; set; }
        public string InvDocNo8 { get; set; }
        public string InvDocNo9 { get; set; }
        public string InvDocNo10 { get; set; }
        public DateTime? InvDocDate1 { get; set; }
        public DateTime? InvDocDate2 { get; set; }
        public DateTime? InvDocDate3 { get; set; }
        public DateTime? InvDocDate4 { get; set; }
        public DateTime? InvDocDate5 { get; set; }
        public DateTime? InvDocDate6 { get; set; }
        public DateTime? InvDocDate7 { get; set; }
        public DateTime? InvDocDate8 { get; set; }
        public DateTime? InvDocDate9 { get; set; }
        public DateTime? InvDocDate10 { get; set; }
        public bool? InvDisableTrig { get; set; }
        public long? CustAddressId { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public int? CutId { get; set; }
        public DateTime? CutDate { get; set; }
        public string CutNumber { get; set; }
        public decimal? InvAddCostAmt { get; set; }
        public string RefDocType { get; set; }
        public string EntryName { get; set; }
        public string ConfirmName { get; set; }
        public string VerifyName { get; set; }
        public string RunName { get; set; }
        public DateTime? EntryTime { get; set; }
        public DateTime? ConfirmTime { get; set; }
        public DateTime? VerifyTime { get; set; }
        public DateTime? RunTime { get; set; }
        public string EntryCompName { get; set; }
        public string ConfirmCompName { get; set; }
        public string VerifyCompName { get; set; }
        public string RunCompName { get; set; }
    }
}
