﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Dashboard.Database
{
    public partial class SofiContext : DbContext
    {
        public SofiContext()
        {
        }

        [DbFunction("sfnGetOSPOQty")]
        public static decimal sfnGetOSPOQty(long poid, Guid seqId)
        {
            throw new Exception();
        }
        [DbFunction("sfnGetOSPRQty")]
        public static decimal sfnGetOSPRQty(long id, Guid seqId)
        {
            throw new Exception();
        }

        [DbFunction("sfnGetOSRecDOQty")]
        public static decimal sfnGetOSRecDOQty(long iventoryid, Guid seqId)
        {
            throw new Exception();
        }

        public SofiContext(DbContextOptions<SofiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<sfnStockMonitoring> sfnStockMonitoring { get; set; }
        public virtual DbSet<TblCountry> TblCountry { get; set; }
        public virtual DbSet<TblCurrency> TblCurrency { get; set; }
        public virtual DbSet<TblDashboardProcurement> TblDashboardProcurement { get; set; }
        public virtual DbSet<TblDepartment> TblDepartment { get; set; }
        public virtual DbSet<TblDivision> TblDivision { get; set; }
        public virtual DbSet<TblEmployee> TblEmployee { get; set; }
        public virtual DbSet<TblGrouping> TblGrouping { get; set; }
        public virtual DbSet<TblInvPart> TblInvPart { get; set; }
        public virtual DbSet<TblInventory> TblInventory { get; set; }
        public virtual DbSet<TblInvoice> TblInvoice { get; set; }
        public virtual DbSet<TblIvoPart> TblIvoPart { get; set; }
        public virtual DbSet<TblPart> TblPart { get; set; }
        public virtual DbSet<TblPartCat> TblPartCat { get; set; }
        public virtual DbSet<TblPo> TblPo { get; set; }
        public virtual DbSet<TblPopart> TblPopart { get; set; }
        public virtual DbSet<TblPotype> TblPotype { get; set; }
        public virtual DbSet<TblProject> TblProject { get; set; }
        public virtual DbSet<TblPrtStock> TblPrtStock { get; set; }
        public virtual DbSet<TblPurchReq> TblPurchReq { get; set; }
        public virtual DbSet<TblPurchReqPart> TblPurchReqPart { get; set; }
        public virtual DbSet<TblRecInvoice> TblRecInvoice { get; set; }
        public virtual DbSet<TblRecIvoPart> TblRecIvoPart { get; set; }
        public virtual DbSet<TblSalesQuo> TblSalesQuo { get; set; }
        public virtual DbSet<TblSalesQuoPart> TblSalesQuoPart { get; set; }
        public virtual DbSet<TblSo> TblSo { get; set; }
        public virtual DbSet<TblSopart> TblSopart { get; set; }
        public virtual DbSet<TblStatus> TblStatus { get; set; }
        public virtual DbSet<TblVendor> TblVendor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TblCountry>(entity =>
            {
                entity.HasKey(e => e.CountryId);

                entity.ToTable("tblCountry");

                entity.HasIndex(e => e.CountryCd);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.CountryCd)
                    .IsRequired()
                    .HasColumnName("CountryCD")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.CtrName)
                    .IsRequired()
                    .HasColumnName("ctrName")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CtrViewName)
                    .HasColumnName("ctrViewName")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(convert(varchar,[ctrName]))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");
            });

            modelBuilder.Entity<TblCurrency>(entity =>
            {
                entity.HasKey(e => e.CurrencyId);

                entity.ToTable("tblCurrency");

                entity.HasIndex(e => e.CurrencyCd);

                entity.Property(e => e.CurrencyId).HasColumnName("CurrencyID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.CurAdjustVarLmt)
                    .HasColumnName("curAdjustVarLmt")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.CurBase).HasColumnName("curBase");

                entity.Property(e => e.CurDecPlace)
                    .HasColumnName("curDecPlace")
                    .HasDefaultValueSql("(2)");

                entity.Property(e => e.CurName)
                    .IsRequired()
                    .HasColumnName("curName")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CurRoundDigit)
                    .HasColumnName("curRoundDigit")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.CurRoundTo)
                    .HasColumnName("curRoundTo")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.CurRoundType)
                    .HasColumnName("curRoundType")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.CurSymbol)
                    .IsRequired()
                    .HasColumnName("curSymbol")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CurUnit)
                    .HasColumnName("curUnit")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.CurViewName)
                    .IsRequired()
                    .HasColumnName("curViewName")
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([curName] + ' - ' + [CurrencyCD])");

                entity.Property(e => e.CurrencyCd)
                    .IsRequired()
                    .HasColumnName("CurrencyCD")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");
            });

            modelBuilder.Entity<TblDashboardProcurement>(entity =>
            {
                entity.ToTable("tblDashboardProcurement");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Cd)
                    .IsRequired()
                    .HasColumnName("CD")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyId).HasColumnName("CurrencyID");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.PotypeId).HasColumnName("POTypeID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RequestorId).HasColumnName("RequestorID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");
            });

            modelBuilder.Entity<TblDepartment>(entity =>
            {
                entity.HasKey(e => e.DepartmentId);

                entity.ToTable("tblDepartment");

                entity.HasIndex(e => e.DepartmentCd);

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepDiscontinue)
                    .IsRequired()
                    .HasColumnName("depDiscontinue")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepName)
                    .IsRequired()
                    .HasColumnName("depName")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DepNote)
                    .HasColumnName("depNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.DepViewName)
                    .IsRequired()
                    .HasColumnName("depViewName")
                    .HasMaxLength(223)
                    .IsUnicode(false)
                    .HasComputedColumnSql("((isnull(CONVERT([varchar](200),[depName],0),'')+' - ')+isnull(CONVERT([varchar](20),[DepartmentCD],0),''))");

                entity.Property(e => e.DepartmentCd)
                    .IsRequired()
                    .HasColumnName("DepartmentCD")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");
            });

            modelBuilder.Entity<TblDivision>(entity =>
            {
                entity.HasKey(e => e.DivisionId);

                entity.ToTable("tblDivision");

                entity.HasIndex(e => e.DivisionCd);

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DivDiscontinue)
                    .IsRequired()
                    .HasColumnName("divDiscontinue")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DivName)
                    .IsRequired()
                    .HasColumnName("divName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DivViewName)
                    .IsRequired()
                    .HasColumnName("divViewName")
                    .HasMaxLength(265)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(convert(varchar(7),[DivisionCD]),'') + ' - ' + isnull(convert(varchar(255),[divName]),''))");

                entity.Property(e => e.DivisionCd)
                    .IsRequired()
                    .HasColumnName("DivisionCD")
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");
            });

            modelBuilder.Entity<TblEmployee>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("tblEmployee");

                entity.HasIndex(e => e.EmployeeCd);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpDiscontinue)
                    .IsRequired()
                    .HasColumnName("empDiscontinue")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpEmail)
                    .HasColumnName("empEmail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpJobTitle)
                    .HasColumnName("empJobTitle")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpName)
                    .IsRequired()
                    .HasColumnName("empName")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EmpParentId)
                    .HasColumnName("empParentID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpPhone)
                    .HasColumnName("empPhone")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EmpProject)
                    .IsRequired()
                    .HasColumnName("empProject")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpPurch)
                    .IsRequired()
                    .HasColumnName("empPurch")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpSales)
                    .IsRequired()
                    .HasColumnName("empSales")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpStore)
                    .IsRequired()
                    .HasColumnName("empStore")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmpViewName)
                    .HasColumnName("empViewName")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(convert(varchar,[empName]))");

                entity.Property(e => e.EmployeeCd)
                    .IsRequired()
                    .HasColumnName("EmployeeCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");
            });

            modelBuilder.Entity<TblGrouping>(entity =>
            {
                entity.HasKey(e => e.GroupingId);

                entity.ToTable("tblGrouping");

                entity.HasIndex(e => e.GroupingCd);

                entity.Property(e => e.GroupingId).HasColumnName("GroupingID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.GngBitCodePrefix)
                    .HasColumnName("gngBitCodePrefix")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.GngLevel).HasColumnName("gngLevel");

                entity.Property(e => e.GngName)
                    .IsRequired()
                    .HasColumnName("gngName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GngType)
                    .HasColumnName("gngType")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.GngViewName)
                    .HasColumnName("gngViewName")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(convert(varchar,[gngName]))");

                entity.Property(e => e.GroupingCd)
                    .IsRequired()
                    .HasColumnName("GroupingCD")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.ParentId)
                    .HasColumnName("ParentID")
                    .HasDefaultValueSql("((-1))");
            });

            modelBuilder.Entity<TblInvPart>(entity =>
            {
                entity.HasKey(e => new { e.InventoryId, e.SeqId });

                entity.ToTable("tblInvPart");

                entity.HasIndex(e => e.PartId);

                entity.HasIndex(e => e.RefId);

                entity.HasIndex(e => e.RefSeqId);

                entity.HasIndex(e => e.SubId);

                entity.HasIndex(e => e.SubSeqId);

                entity.HasIndex(e => e.UnitId);

                entity.HasIndex(e => e.WarehouseId);

                entity.Property(e => e.InventoryId).HasColumnName("InventoryID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.BatchId)
                    .HasColumnName("BatchID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullBatchID'))");

                entity.Property(e => e.Coaid)
                    .HasColumnName("COAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('setNullDepartmentID'))");

                entity.Property(e => e.DivisionId)
                    .HasColumnName("DivisionID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('setNullDivisionID'))");

                entity.Property(e => e.InpAddChargeAmt)
                    .HasColumnName("inpAddChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpAdjAmt)
                    .HasColumnName("inpAdjAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpAmount)
                    .HasColumnName("inpAmount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpBaseQuantity)
                    .HasColumnName("inpBaseQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpChargeAmt)
                    .HasColumnName("inpChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpDiscAmt)
                    .HasColumnName("inpDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpDiscFml)
                    .HasColumnName("inpDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.InpDiscFmlAmt)
                    .HasColumnName("inpDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpDiscFmlDisp)
                    .HasColumnName("inpDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpDiscPct)
                    .HasColumnName("inpDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpDiscPctAmt)
                    .HasColumnName("inpDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpDiscount)
                    .HasColumnName("inpDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpDivisionSub).HasColumnName("inpDivisionSub");

                entity.Property(e => e.InpExtField1)
                    .HasColumnName("inpExtField1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField10)
                    .HasColumnName("inpExtField10")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField2)
                    .HasColumnName("inpExtField2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField3)
                    .HasColumnName("inpExtField3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField4)
                    .HasColumnName("inpExtField4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField5)
                    .HasColumnName("inpExtField5")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField6)
                    .HasColumnName("inpExtField6")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField7)
                    .HasColumnName("inpExtField7")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField8)
                    .HasColumnName("inpExtField8")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpExtField9)
                    .HasColumnName("inpExtField9")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InpGrossAmt)
                    .HasColumnName("inpGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpNote)
                    .HasColumnName("inpNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.InpPortion)
                    .HasColumnName("inpPortion")
                    .HasColumnType("decimal(5, 4)")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.InpPrice)
                    .HasColumnName("inpPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpQuantity)
                    .HasColumnName("inpQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InpSeq).HasColumnName("inpSeq");

                entity.Property(e => e.InpTaxAmt)
                    .HasColumnName("inpTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvViwPartName)
                    .HasColumnName("invViwPartName")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetInvPartName]([PartID]))");

                entity.Property(e => e.LotId)
                    .HasColumnName("LotID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullLotID'))");

                entity.Property(e => e.MachineId)
                    .HasColumnName("machineID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullMachineID'))");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId)
                    .HasColumnName("ProjectID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('setNullProjectID'))");

                entity.Property(e => e.RefId)
                    .HasColumnName("RefID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRefID'))");

                entity.Property(e => e.RefSeqId)
                    .HasColumnName("RefSeqID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldGUID]('NullRefSeqID'))");

                entity.Property(e => e.RoutingId)
                    .HasColumnName("RoutingID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRouting'))");

                entity.Property(e => e.SubId)
                    .HasColumnName("SubID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRefID'))");

                entity.Property(e => e.SubSeqId)
                    .HasColumnName("SubSeqID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldGUID]('NullRefSeqID'))");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.Property(e => e.UweightId)
                    .HasColumnName("UweightID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullUWeightID'))");

                entity.Property(e => e.WarehouseId)
                    .HasColumnName("WarehouseID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('WarehouseID'))");
            });

            modelBuilder.Entity<TblInventory>(entity =>
            {
                entity.HasKey(e => e.InventoryId);

                entity.ToTable("tblInventory");

                entity.HasIndex(e => e.BillVendorId);

                entity.HasIndex(e => e.Coaid);

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.DepartmentId);

                entity.HasIndex(e => e.DivisionId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.InventoryCd);

                entity.HasIndex(e => e.InventoryTypeId);

                entity.HasIndex(e => e.ProjectId);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TaxId);

                entity.HasIndex(e => e.VendorId);

                entity.Property(e => e.InventoryId).HasColumnName("InventoryID");

                entity.Property(e => e.BillVendorId)
                    .HasColumnName("BillVendorID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullVendorID'))");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasDefaultValueSql("create default [defBranchID] as 0");

                entity.Property(e => e.Coaid)
                    .HasColumnName("COAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.ConfirmCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmTime).HasColumnType("datetime");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.CustAddressId).HasColumnName("CustAddressID");

                entity.Property(e => e.CutDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.CutId).HasColumnName("CutID");

                entity.Property(e => e.CutNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EntryCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryTime).HasColumnType("datetime");

                entity.Property(e => e.InvAddCostAmt).HasColumnType("decimal(24, 6)");

                entity.Property(e => e.InvChangeAmt)
                    .HasColumnName("invChangeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvChargeAmt)
                    .HasColumnName("invChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDate)
                    .HasColumnName("invDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.InvDeliverTo)
                    .HasColumnName("invDeliverTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvDisableTrig)
                    .IsRequired()
                    .HasColumnName("invDisableTrig")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDiscAmt)
                    .HasColumnName("invDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDiscFml)
                    .HasColumnName("invDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.InvDiscFmlAmt)
                    .HasColumnName("invDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDiscFmlDisp)
                    .HasColumnName("invDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvDiscPct)
                    .HasColumnName("invDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDiscPctAmt)
                    .HasColumnName("invDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDiscount)
                    .HasColumnName("invDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvDocDate)
                    .HasColumnName("invDocDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.InvDocDate1)
                    .HasColumnName("invDocDate1")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate10)
                    .HasColumnName("invDocDate10")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate2)
                    .HasColumnName("invDocDate2")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate3)
                    .HasColumnName("invDocDate3")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate4)
                    .HasColumnName("invDocDate4")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate5)
                    .HasColumnName("invDocDate5")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate6)
                    .HasColumnName("invDocDate6")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate7)
                    .HasColumnName("invDocDate7")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate8)
                    .HasColumnName("invDocDate8")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocDate9)
                    .HasColumnName("invDocDate9")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvDocNo1)
                    .HasColumnName("invDocNo1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo10)
                    .HasColumnName("invDocNo10")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo2)
                    .HasColumnName("invDocNo2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo3)
                    .HasColumnName("invDocNo3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo4)
                    .HasColumnName("invDocNo4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo5)
                    .HasColumnName("invDocNo5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo6)
                    .HasColumnName("invDocNo6")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo7)
                    .HasColumnName("invDocNo7")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo8)
                    .HasColumnName("invDocNo8")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocNo9)
                    .HasColumnName("invDocNo9")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocument1)
                    .HasColumnName("invDocument1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocument2)
                    .HasColumnName("invDocument2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvDocument3)
                    .HasColumnName("invDocument3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDueDate)
                    .HasColumnName("invDueDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvEarlyDiscAmt)
                    .HasColumnName("invEarlyDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvEarlyDiscDate)
                    .HasColumnName("invEarlyDiscDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvEarlyDiscPct)
                    .HasColumnName("invEarlyDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvGrossAmt)
                    .HasColumnName("invGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvNetAmt)
                    .HasColumnName("invNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvNote)
                    .HasColumnName("invNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.InvPaidAmt)
                    .HasColumnName("invPaidAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvPrepaidAmt)
                    .HasColumnName("invPrepaidAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvReference).HasColumnName("invReference");

                entity.Property(e => e.InvTaxAmt)
                    .HasColumnName("invTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvTaxAmt1st)
                    .HasColumnName("invTaxAmt1st")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvTaxAmt2nd)
                    .HasColumnName("invTaxAmt2nd")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InvTotNetAmt)
                    .HasColumnName("invTotNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.InventoryCd)
                    .IsRequired()
                    .HasColumnName("InventoryCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.InventoryTypeId).HasColumnName("InventoryTypeID");

                entity.Property(e => e.JournalTypeId).HasColumnName("JournalTypeID");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.ProjectId)
                    .HasColumnName("ProjectID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('setNullProjectID'))");

                entity.Property(e => e.RefDocType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RunCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunTime).HasColumnType("datetime");

                entity.Property(e => e.ShipAgentId)
                    .HasColumnName("ShipAgentID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ShipAgentID'))");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.VerifyCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblInvoice>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.ToTable("tblInvoice");

                entity.HasIndex(e => e.BillVendorId);

                entity.HasIndex(e => e.Coaid);

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.DepartmentId);

                entity.HasIndex(e => e.DivisionId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.InvoiceCd);

                entity.HasIndex(e => e.JournalTypeId);

                entity.HasIndex(e => e.ProjectId);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TaxId);

                entity.HasIndex(e => e.VendorId);

                entity.Property(e => e.InvoiceId).HasColumnName("InvoiceID");

                entity.Property(e => e.BillVendorId).HasColumnName("BillVendorID");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasDefaultValueSql("create default [defBranchID] as 0");

                entity.Property(e => e.Coaid)
                    .HasColumnName("COAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.InvoiceCd)
                    .IsRequired()
                    .HasColumnName("InvoiceCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.InvoiceTypeId)
                    .HasColumnName("InvoiceTypeID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('InvoiceTypeID'))");

                entity.Property(e => e.IvoChargeAmt)
                    .HasColumnName("ivoChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDate)
                    .HasColumnName("ivoDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.IvoDeliverTo)
                    .HasColumnName("ivoDeliverTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvoDisableTrig)
                    .IsRequired()
                    .HasColumnName("ivoDisableTrig")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDiscAmt)
                    .HasColumnName("ivoDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDiscFml)
                    .HasColumnName("ivoDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.IvoDiscFmlAmt)
                    .HasColumnName("ivoDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDiscFmlDisp)
                    .HasColumnName("ivoDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvoDiscPct)
                    .HasColumnName("ivoDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDiscPctAmt)
                    .HasColumnName("ivoDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDiscount)
                    .HasColumnName("ivoDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoDocDate)
                    .HasColumnName("ivoDocDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.IvoDocDate1)
                    .HasColumnName("ivoDocDate1")
                    .HasColumnType("datetime");

                entity.Property(e => e.IvoDocDate2)
                    .HasColumnName("ivoDocDate2")
                    .HasColumnType("datetime");

                entity.Property(e => e.IvoDocDate3)
                    .HasColumnName("ivoDocDate3")
                    .HasColumnType("datetime");

                entity.Property(e => e.IvoDocNo1)
                    .HasColumnName("ivoDocNo1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IvoDocNo2)
                    .HasColumnName("ivoDocNo2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IvoDocNo3)
                    .HasColumnName("ivoDocNo3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IvoDueDate)
                    .HasColumnName("ivoDueDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IvoEarlyDiscAmt)
                    .HasColumnName("ivoEarlyDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoEarlyDiscDate)
                    .HasColumnName("ivoEarlyDiscDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IvoEarlyDiscPct)
                    .HasColumnName("ivoEarlyDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoGrossAmt)
                    .HasColumnName("ivoGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoInstallmentAmt)
                    .HasColumnName("ivoInstallmentAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoNetAmt)
                    .HasColumnName("ivoNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoNote)
                    .HasColumnName("ivoNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.IvoPrepaidAmt)
                    .HasColumnName("ivoPrepaidAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoReference).HasColumnName("ivoReference");

                entity.Property(e => e.IvoRequestor)
                    .HasColumnName("ivoRequestor")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvoTaxAmt)
                    .HasColumnName("ivoTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoTaxAmt1st)
                    .HasColumnName("ivoTaxAmt1st")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoTaxAmt2nd)
                    .HasColumnName("ivoTaxAmt2nd")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvoTaxInvoiceDate)
                    .HasColumnName("ivoTaxInvoiceDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.IvoTaxInvoiceNo)
                    .HasColumnName("ivoTaxInvoiceNo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IvoTotNetAmt)
                    .HasColumnName("ivoTotNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.JournalCd)
                    .HasColumnName("JournalCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetJournalCD](2, [InvoiceID]))");

                entity.Property(e => e.JournalTypeId).HasColumnName("JournalTypeID");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.ShipAgentId)
                    .HasColumnName("ShipAgentID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ShipAgentID'))");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.TimeBillId).HasColumnName("TimeBillID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<TblIvoPart>(entity =>
            {
                entity.HasKey(e => new { e.InvoiceId, e.SeqId });

                entity.ToTable("tblIvoPart");

                entity.HasIndex(e => e.Coaid);

                entity.HasIndex(e => e.PartId);

                entity.HasIndex(e => e.RefId)
                    .HasName("IDX_tblIvoPart_RefID");

                entity.HasIndex(e => e.RefSeqId);

                entity.HasIndex(e => e.UnitId);

                entity.HasIndex(e => e.WarehouseId);

                entity.Property(e => e.InvoiceId).HasColumnName("InvoiceID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Coaid)
                    .HasColumnName("COAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.IvopViwPartName)
                    .HasColumnName("ivopViwPartName")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetIvoPartName]([PartID]))");

                entity.Property(e => e.IvpAdjAmt)
                    .HasColumnName("ivpAdjAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpAmount)
                    .HasColumnName("ivpAmount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpBaseQuantity)
                    .HasColumnName("ivpBaseQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpChargeAmt)
                    .HasColumnName("ivpChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpDiscAmt)
                    .HasColumnName("ivpDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpDiscFml)
                    .HasColumnName("ivpDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.IvpDiscFmlAmt)
                    .HasColumnName("ivpDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpDiscFmlDisp)
                    .HasColumnName("ivpDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvpDiscPct)
                    .HasColumnName("ivpDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpDiscPctAmt)
                    .HasColumnName("ivpDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpDiscount)
                    .HasColumnName("ivpDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpExtField1)
                    .HasColumnName("ivpExtField1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvpExtField2)
                    .HasColumnName("ivpExtField2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvpExtField3)
                    .HasColumnName("ivpExtField3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvpExtField4)
                    .HasColumnName("ivpExtField4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvpExtField5)
                    .HasColumnName("ivpExtField5")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IvpGrossAmt)
                    .HasColumnName("ivpGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpNote)
                    .HasColumnName("ivpNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.IvpPortion)
                    .HasColumnName("ivpPortion")
                    .HasColumnType("decimal(5, 4)")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.IvpPrice)
                    .HasColumnName("ivpPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpQuantity)
                    .HasColumnName("ivpQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.IvpSeq).HasColumnName("ivpSeq");

                entity.Property(e => e.IvpTaxAmt)
                    .HasColumnName("ivpTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.LotId)
                    .HasColumnName("LotID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullLotID'))");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RefId)
                    .HasColumnName("RefID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRefID'))");

                entity.Property(e => e.RefSeqId)
                    .HasColumnName("RefSeqID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldGUID]('NullRefSeqID'))");

                entity.Property(e => e.RoutingId)
                    .HasColumnName("RoutingID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRouting'))");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.Property(e => e.UweightId)
                    .HasColumnName("UweightID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullUWeightID'))");

                entity.Property(e => e.WarehouseId)
                    .HasColumnName("WarehouseID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('WarehouseID'))");
            });

            modelBuilder.Entity<TblPart>(entity =>
            {
                entity.HasKey(e => e.PartId);

                entity.ToTable("tblPart");

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.GroupingId);

                entity.HasIndex(e => e.PartCd);

                entity.HasIndex(e => e.PounitId);

                entity.HasIndex(e => e.UnitId);

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ChgCurrencyId).HasColumnName("ChgCurrencyID");

                entity.Property(e => e.ChgUnitId).HasColumnName("ChgUnitID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DrawingId)
                    .HasColumnName("DrawingID")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.FreightChgId).HasColumnName("FreightChgID");

                entity.Property(e => e.GroupingId).HasColumnName("GroupingID");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PartCd)
                    .IsRequired()
                    .HasColumnName("PartCD")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.PartTypeId).HasColumnName("PartTypeID");

                entity.Property(e => e.PounitId).HasColumnName("POUnitID");

                entity.Property(e => e.PrtBomversionId)
                    .HasColumnName("prtBOMVersionID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrtBoxContent)
                    .HasColumnName("prtBoxContent")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtBuyName)
                    .HasColumnName("prtBuyName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtCatId1).HasColumnName("prtCatID1");

                entity.Property(e => e.PrtCatId2).HasColumnName("prtCatID2");

                entity.Property(e => e.PrtCatId3).HasColumnName("prtCatID3");

                entity.Property(e => e.PrtChgRate)
                    .HasColumnName("prtChgRate")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtCogscoaid)
                    .HasColumnName("prtCOGSCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtConsignmentInCoaid)
                    .HasColumnName("prtConsignmentInCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtConsignmentOutCoaid)
                    .HasColumnName("prtConsignmentOutCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtCostMethod).HasColumnName("prtCostMethod");

                entity.Property(e => e.PrtCubic)
                    .HasColumnName("prtCubic")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtDimPitch)
                    .HasColumnName("prtDimPitch")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtDimThick)
                    .HasColumnName("prtDimThick")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtDimWeight)
                    .HasColumnName("prtDimWeight")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtDimWide)
                    .HasColumnName("prtDimWide")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtDimension)
                    .HasColumnName("prtDimension")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.PrtDiscontinue)
                    .IsRequired()
                    .HasColumnName("prtDiscontinue")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtDobitControl)
                    .IsRequired()
                    .HasColumnName("prtDOBitControl")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtExtField1)
                    .HasColumnName("prtExtField1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtExtField2)
                    .HasColumnName("prtExtField2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtExtField3)
                    .HasColumnName("prtExtField3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtExtField4)
                    .HasColumnName("prtExtField4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtExtField5)
                    .HasColumnName("prtExtField5")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtInvCoaid)
                    .HasColumnName("prtInvCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtIssueCoaid)
                    .HasColumnName("prtIssueCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtLeadTime)
                    .HasColumnName("prtLeadTime")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.PrtLot).HasColumnName("prtLot");

                entity.Property(e => e.PrtMaxStock)
                    .HasColumnName("prtMaxStock")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtMinOrder)
                    .HasColumnName("prtMinOrder")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtName)
                    .IsRequired()
                    .HasColumnName("prtName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtNote)
                    .HasColumnName("prtNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PrtNumber)
                    .HasColumnName("prtNumber")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrtPoprice)
                    .HasColumnName("prtPOPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtPurchCoaid)
                    .HasColumnName("prtPurchCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtPurchDiscCoaid)
                    .HasColumnName("prtPurchDiscCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtRating)
                    .HasColumnName("prtRating")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtReceiptCoaid)
                    .HasColumnName("prtReceiptCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtRefId).HasColumnName("prtRefID");

                entity.Property(e => e.PrtRegisteredNo)
                    .HasColumnName("prtRegisteredNo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrtReject)
                    .HasColumnName("prtReject")
                    .HasColumnType("decimal(12, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtSafetyStock)
                    .HasColumnName("prtSafetyStock")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtSalesCoaid)
                    .HasColumnName("prtSalesCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtSalesDiscCoaid)
                    .HasColumnName("prtSalesDiscCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtSalesRetCoaid)
                    .HasColumnName("prtSalesRetCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.PrtSellName)
                    .HasColumnName("prtSellName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrtSerialNo)
                    .HasColumnName("prtSerialNo")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrtSerialize)
                    .IsRequired()
                    .HasColumnName("prtSerialize")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtSpecification)
                    .HasColumnName("prtSpecification")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.PrtTaxable)
                    .IsRequired()
                    .HasColumnName("prtTaxable")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.PrtViewName)
                    .IsRequired()
                    .HasColumnName("prtViewName")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(CONVERT([varchar](100),[PartCD],0),''))");

                entity.Property(e => e.PrtWeight)
                    .HasColumnName("prtWeight")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrtWipcoaid)
                    .HasColumnName("prtWIPCOAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.SounitId).HasColumnName("SOUnitID");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.Property(e => e.ViewUnitId).HasColumnName("ViewUnitID");

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");
            });

            modelBuilder.Entity<TblPartCat>(entity =>
            {
                entity.HasKey(e => e.PartCatId);

                entity.ToTable("tblPartCat");

                entity.Property(e => e.PartCatId).HasColumnName("PartCatID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PacCat1)
                    .IsRequired()
                    .HasColumnName("pacCat1")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PacCat2)
                    .IsRequired()
                    .HasColumnName("pacCat2")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PacCat3)
                    .IsRequired()
                    .HasColumnName("pacCat3")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PacName)
                    .IsRequired()
                    .HasColumnName("pacName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PacViewName)
                    .IsRequired()
                    .HasColumnName("pacViewName")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(CONVERT([varchar](50),[pacName],0),''))");

                entity.Property(e => e.PartCatCd)
                    .IsRequired()
                    .HasColumnName("PartCatCD")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");
            });

            modelBuilder.Entity<TblPo>(entity =>
            {
                entity.HasKey(e => e.Poid);

                entity.ToTable("tblPO");

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.Pocd);

                entity.HasIndex(e => e.ProjectId);

                entity.HasIndex(e => e.PurchQuoId);

                entity.HasIndex(e => e.VendorId);

                entity.Property(e => e.Poid).HasColumnName("POID");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasDefaultValueSql("create default [defBranchID] as 0");

                entity.Property(e => e.Coaid).HasColumnName("COAID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.ConfirmCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmTime).HasColumnType("datetime");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EntryCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryTime).HasColumnType("datetime");

                entity.Property(e => e.JournalTypeId).HasColumnName("JournalTypeID");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PoChargeAmt)
                    .HasColumnName("poChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoContact)
                    .HasColumnName("poContact")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PoDate)
                    .HasColumnName("poDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.PoDeliverTo)
                    .HasColumnName("poDeliverTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PoDisableTrig)
                    .IsRequired()
                    .HasColumnName("poDisableTrig")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoDiscAmt)
                    .HasColumnName("poDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoDiscFml)
                    .HasColumnName("poDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PoDiscFmlAmt)
                    .HasColumnName("poDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoDiscFmlDisp)
                    .HasColumnName("poDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PoDiscPct)
                    .HasColumnName("poDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoDiscPctAmt)
                    .HasColumnName("poDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoDiscount)
                    .HasColumnName("poDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoDocDate1)
                    .HasColumnName("poDocDate1")
                    .HasColumnType("datetime");

                entity.Property(e => e.PoDocDate2)
                    .HasColumnName("poDocDate2")
                    .HasColumnType("datetime");

                entity.Property(e => e.PoDocDate3)
                    .HasColumnName("poDocDate3")
                    .HasColumnType("datetime");

                entity.Property(e => e.PoDocNo1)
                    .HasColumnName("poDocNo1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoDocNo2)
                    .HasColumnName("poDocNo2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoDocNo3)
                    .HasColumnName("poDocNo3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoGrossAmt)
                    .HasColumnName("poGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoNetAmt)
                    .HasColumnName("poNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoNote)
                    .HasColumnName("poNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PoPrepaidAmt)
                    .HasColumnName("poPrepaidAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoReqDate)
                    .HasColumnName("poReqDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.PoReqDesc)
                    .HasColumnName("poReqDesc")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PoTaxAmt)
                    .HasColumnName("poTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoTaxAmt1st)
                    .HasColumnName("poTaxAmt1st")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoTaxAmt2nd)
                    .HasColumnName("poTaxAmt2nd")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PoTotNetAmt)
                    .HasColumnName("poTotNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Pocd)
                    .IsRequired()
                    .HasColumnName("POCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.PotypeId)
                    .HasColumnName("POTypeID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('POTypeID'))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.PurchQuoId)
                    .HasColumnName("PurchQuoID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RequestId).HasColumnName("RequestID");

                entity.Property(e => e.RunCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunTime).HasColumnType("datetime");

                entity.Property(e => e.ShipAgentId)
                    .HasColumnName("ShipAgentID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ShipAgentID'))");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.VerifyCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblPopart>(entity =>
            {
                entity.HasKey(e => new { e.Poid, e.SeqId });

                entity.ToTable("tblPOPart");

                entity.HasIndex(e => e.PartId);

                entity.HasIndex(e => e.RefId);

                entity.HasIndex(e => e.RefSeqId);

                entity.HasIndex(e => e.UnitId);

                entity.Property(e => e.Poid).HasColumnName("POID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Coaid).HasColumnName("COAID");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.Duty1ChgId).HasColumnName("Duty1ChgID");

                entity.Property(e => e.Duty2ChgId).HasColumnName("Duty2ChgID");

                entity.Property(e => e.FreightChgId).HasColumnName("FreightChgID");

                entity.Property(e => e.Insur1ChgId).HasColumnName("Insur1ChgID");

                entity.Property(e => e.Insur2ChgId).HasColumnName("Insur2ChgID");

                entity.Property(e => e.LotId)
                    .HasColumnName("LotID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullLotID'))");

                entity.Property(e => e.Oth1ChgId).HasColumnName("Oth1ChgID");

                entity.Property(e => e.Oth2ChgId).HasColumnName("Oth2ChgID");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.PoViwPartName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetPartName]([PartID]))");

                entity.Property(e => e.PopAmount)
                    .HasColumnName("popAmount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDiscAmt)
                    .HasColumnName("popDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDiscFml)
                    .HasColumnName("popDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PopDiscFmlAmt)
                    .HasColumnName("popDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDiscFmlDisp)
                    .HasColumnName("popDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopDiscPct)
                    .HasColumnName("popDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDiscPctAmt)
                    .HasColumnName("popDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDiscount)
                    .HasColumnName("popDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDuty1ChgAmt)
                    .HasColumnName("popDuty1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopDuty2ChgAmt)
                    .HasColumnName("popDuty2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopExtField1)
                    .HasColumnName("popExtField1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopExtField2)
                    .HasColumnName("popExtField2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopExtField3)
                    .HasColumnName("popExtField3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopExtField4)
                    .HasColumnName("popExtField4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopExtField5)
                    .HasColumnName("popExtField5")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopFreightChgAmt)
                    .HasColumnName("popFreightChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopGrossAmt)
                    .HasColumnName("popGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopInsur1ChgAmt)
                    .HasColumnName("popInsur1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopInsur2ChgAmt)
                    .HasColumnName("popInsur2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopName)
                    .HasColumnName("popName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PopNote)
                    .HasColumnName("popNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PopOth1ChgAmt)
                    .HasColumnName("popOth1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopOth2ChgAmt)
                    .HasColumnName("popOth2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopPrice)
                    .HasColumnName("popPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopQuantity)
                    .HasColumnName("popQuantity")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.PopReqDate)
                    .HasColumnName("popReqDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.PopSeq).HasColumnName("popSeq");

                entity.Property(e => e.PopTax1ChgAmt)
                    .HasColumnName("popTax1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopTax2ChgAmt)
                    .HasColumnName("popTax2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopTaxAmt)
                    .HasColumnName("popTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PopTotChgAmt)
                    .HasColumnName("popTotChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasComputedColumnSql("(convert(decimal(24,6),([popFreightChgAmt] + [popInsur1ChgAmt] + [popInsur2ChgAmt] + [popDuty1ChgAmt] + [popDuty2ChgAmt] + [popTax1ChgAmt] + [popTax2ChgAmt] + [popOth1ChgAmt] + [popOth2ChgAmt])))");

                entity.Property(e => e.ProjectId)
                    .HasColumnName("ProjectID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('setNullProjectID'))");

                entity.Property(e => e.PrrefId).HasColumnName("PRRefID");

                entity.Property(e => e.PrrefSeqId).HasColumnName("PRRefSeqID");

                entity.Property(e => e.RefId)
                    .HasColumnName("RefID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRefID'))");

                entity.Property(e => e.RefSeqId)
                    .HasColumnName("RefSeqID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldGUID]('NullRefSeqID'))");

                entity.Property(e => e.RoutingId)
                    .HasColumnName("RoutingID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRouting'))");

                entity.Property(e => e.Tax1ChgId).HasColumnName("Tax1ChgID");

                entity.Property(e => e.Tax2ChgId).HasColumnName("Tax2ChgID");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.Property(e => e.UweightId)
                    .HasColumnName("UweightID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullUWeightID'))");
            });

            modelBuilder.Entity<TblPotype>(entity =>
            {
                entity.HasKey(e => e.PotypeId);

                entity.ToTable("tblPOType");

                entity.Property(e => e.PotypeId).HasColumnName("POTypeID");

                entity.Property(e => e.AnsDigit)
                    .HasColumnName("ansDigit")
                    .HasDefaultValueSql("((4))");

                entity.Property(e => e.AnsInitType).HasColumnName("ansInitType");

                entity.Property(e => e.AnsLoc).HasColumnName("ansLoc");

                entity.Property(e => e.AnsMode).HasColumnName("ansMode");

                entity.Property(e => e.AnsPrefix)
                    .HasColumnName("ansPrefix")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AnsStart).HasColumnName("ansStart");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PotConsignment)
                    .IsRequired()
                    .HasColumnName("potConsignment")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PotName)
                    .IsRequired()
                    .HasColumnName("potName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PotViewName)
                    .IsRequired()
                    .HasColumnName("potViewName")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(convert(varchar(50),[potName]),''))");

                entity.Property(e => e.PotypeCd)
                    .IsRequired()
                    .HasColumnName("POTypeCD")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");
            });

            modelBuilder.Entity<TblProject>(entity =>
            {
                entity.HasKey(e => e.ProjectId);

                entity.ToTable("tblProject");

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.ProjectCd);

                entity.HasIndex(e => e.StatusId);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.Budget)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PartControl)
                    .HasColumnName("partControl")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.ProCatId1).HasColumnName("proCatID1");

                entity.Property(e => e.ProCatId2).HasColumnName("proCatID2");

                entity.Property(e => e.ProCatId3).HasColumnName("proCatID3");

                entity.Property(e => e.ProDate)
                    .HasColumnName("proDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.ProEndDate)
                    .HasColumnName("proEndDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.ProName)
                    .IsRequired()
                    .HasColumnName("proName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProNote)
                    .HasColumnName("proNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.ProStartDate)
                    .HasColumnName("proStartDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.ProViewName)
                    .IsRequired()
                    .HasColumnName("proViewName")
                    .HasMaxLength(153)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(convert(varchar(50),[ProjectCD]),'') + ' - ' + isnull(convert(varchar(100),[proName]),''))");

                entity.Property(e => e.ProjectCd)
                    .IsRequired()
                    .HasColumnName("ProjectCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndName)
                    .IsRequired()
                    .HasColumnName("vndName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPrtStock>(entity =>
            {
                entity.HasKey(e => e.PrtStockId);

                entity.ToTable("tblPrtStock");

                entity.HasIndex(e => new { e.RefId, e.RefSeqId })
                    .HasName("IX_tblPrtStock");

                entity.Property(e => e.PrtStockId).HasColumnName("PrtStockID");

                entity.Property(e => e.BatchId)
                    .HasColumnName("BatchID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullBatchID'))");

                entity.Property(e => e.Bompart).HasColumnName("BOMPart");

                entity.Property(e => e.Bomrouting).HasColumnName("BOMRouting");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId)
                    .HasColumnName("DivisionID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullDivisionID'))");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LotId).HasColumnName("LotID");

                entity.Property(e => e.MonthId)
                    .HasColumnName("MonthID")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Mpsid).HasColumnName("MPSID");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.PrsBalance)
                    .HasColumnName("prsBalance")
                    .HasColumnType("decimal(26, 6)")
                    .HasComputedColumnSql("(round(([prsIn] + [prsResult] - ([prsOut] + [prsUse])),6))");

                entity.Property(e => e.PrsDate)
                    .HasColumnName("prsDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrsIn)
                    .HasColumnName("prsIn")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrsOut)
                    .HasColumnName("prsOut")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrsRemark)
                    .HasColumnName("prsRemark")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PrsResult)
                    .HasColumnName("prsResult")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrsUse)
                    .HasColumnName("prsUse")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RefId).HasColumnName("RefID");

                entity.Property(e => e.RefSeqId).HasColumnName("RefSeqID");

                entity.Property(e => e.RefTypeId).HasColumnName("RefTypeID");

                entity.Property(e => e.RoutingId).HasColumnName("RoutingID");

                entity.Property(e => e.UweightId).HasColumnName("UweightID");

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");
            });

            modelBuilder.Entity<TblPurchReq>(entity =>
            {
                entity.HasKey(e => e.PurchReqId);

                entity.ToTable("tblPurchReq");

                entity.Property(e => e.PurchReqId).HasColumnName("PurchReqID");

                entity.Property(e => e.CompanyId)
                    .HasColumnName("CompanyID")
                    .HasDefaultValueSql(@"
create default [defCompanyID] as 0
");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.ConfirmCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmTime).HasColumnType("datetime");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EntryCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryTime).HasColumnType("datetime");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PotypeId)
                    .HasColumnName("POTypeID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('POTypeID'))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.PrqDate)
                    .HasColumnName("prqDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PrqNote)
                    .HasColumnName("prqNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PrqPriority).HasColumnName("prqPriority");

                entity.Property(e => e.PrqPurpose)
                    .HasColumnName("prqPurpose")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PrqReqDate)
                    .HasColumnName("prqReqDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.PrqReqDesc)
                    .HasColumnName("prqReqDesc")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PurchReqCd)
                    .IsRequired()
                    .HasColumnName("PurchReqCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.RunCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunTime).HasColumnType("datetime");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.VerifyCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblPurchReqPart>(entity =>
            {
                entity.HasKey(e => new { e.PurchReqId, e.SeqId });

                entity.ToTable("tblPurchReqPart");

                entity.Property(e => e.PurchReqId).HasColumnName("PurchReqID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.PrpAppQty)
                    .HasColumnName("prpAppQty")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrpName)
                    .IsRequired()
                    .HasColumnName("prpName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PrpNote)
                    .HasColumnName("prpNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PrpPrice)
                    .HasColumnName("prpPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrpQuantity)
                    .HasColumnName("prpQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.PrpReqDate)
                    .HasColumnName("prpReqDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrpSeq).HasColumnName("prpSeq");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");
            });

            modelBuilder.Entity<TblRecInvoice>(entity =>
            {
                entity.HasKey(e => e.RecInvoiceId);

                entity.ToTable("tblRecInvoice");

                entity.HasIndex(e => e.BillVendorId);

                entity.HasIndex(e => e.Coaid);

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.DepartmentId);

                entity.HasIndex(e => e.DivisionId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.JournalTypeId);

                entity.HasIndex(e => e.ProjectId);

                entity.HasIndex(e => e.RecInvoiceCd);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TaxId);

                entity.HasIndex(e => e.VendorId);

                entity.Property(e => e.RecInvoiceId).HasColumnName("RecInvoiceID");

                entity.Property(e => e.BillVendorId).HasColumnName("BillVendorID");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasDefaultValueSql("create default [defBranchID] as 0");

                entity.Property(e => e.Coaid)
                    .HasColumnName("COAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.JournalCd)
                    .HasColumnName("JournalCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetJournalCD](6, [RecInvoiceID]))");

                entity.Property(e => e.JournalTypeId).HasColumnName("JournalTypeID");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PphId)
                    .HasColumnName("PphID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('PphID'))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RecInvoiceCd)
                    .IsRequired()
                    .HasColumnName("RecInvoiceCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.RivChargeAmt)
                    .HasColumnName("rivChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivContact)
                    .HasColumnName("rivContact")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RivDate)
                    .HasColumnName("rivDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.RivDeliverTo)
                    .HasColumnName("rivDeliverTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RivDisableTrig)
                    .IsRequired()
                    .HasColumnName("rivDisableTrig")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivDiscAmt)
                    .HasColumnName("rivDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivDiscFml)
                    .HasColumnName("rivDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.RivDiscFmlAmt)
                    .HasColumnName("rivDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivDiscFmlDisp)
                    .HasColumnName("rivDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RivDiscPct)
                    .HasColumnName("rivDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivDiscPctAmt)
                    .HasColumnName("rivDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivDiscount)
                    .HasColumnName("rivDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivDocDate)
                    .HasColumnName("rivDocDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.RivDocDate1)
                    .HasColumnName("rivDocDate1")
                    .HasColumnType("datetime");

                entity.Property(e => e.RivDocDate2)
                    .HasColumnName("rivDocDate2")
                    .HasColumnType("datetime");

                entity.Property(e => e.RivDocDate3)
                    .HasColumnName("rivDocDate3")
                    .HasColumnType("datetime");

                entity.Property(e => e.RivDocNo1)
                    .HasColumnName("rivDocNo1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RivDocNo2)
                    .HasColumnName("rivDocNo2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RivDocNo3)
                    .HasColumnName("rivDocNo3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RivDocument)
                    .IsRequired()
                    .HasColumnName("rivDocument")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RivDueDate)
                    .HasColumnName("rivDueDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RivEarlyDiscAmt)
                    .HasColumnName("rivEarlyDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivEarlyDiscDate)
                    .HasColumnName("rivEarlyDiscDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RivEarlyDiscPct)
                    .HasColumnName("rivEarlyDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivGrossAmt)
                    .HasColumnName("rivGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivNetAmt)
                    .HasColumnName("rivNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivNote)
                    .HasColumnName("rivNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.RivPphAmt)
                    .HasColumnName("rivPphAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivPphAmt1st)
                    .HasColumnName("rivPphAmt1st")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivPphAmt2nd)
                    .HasColumnName("rivPphAmt2nd")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivPrepaidAmt)
                    .HasColumnName("rivPrepaidAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivTaxAmt)
                    .HasColumnName("rivTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivTaxAmt1st)
                    .HasColumnName("rivTaxAmt1st")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivTaxAmt2nd)
                    .HasColumnName("rivTaxAmt2nd")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RivTaxInvoiceDate)
                    .HasColumnName("rivTaxInvoiceDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RivTaxInvoiceNo)
                    .HasColumnName("rivTaxInvoiceNo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RivTotNetAmt)
                    .HasColumnName("rivTotNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.ShipAgentId)
                    .HasColumnName("ShipAgentID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ShipAgentID'))");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<TblRecIvoPart>(entity =>
            {
                entity.HasKey(e => new { e.RecInvoiceId, e.SeqId });

                entity.ToTable("tblRecIvoPart");

                entity.HasIndex(e => e.Coaid);

                entity.HasIndex(e => e.PartId);

                entity.HasIndex(e => e.RefId)
                    .HasName("IDX_tblRecIvoPart_RefID");

                entity.HasIndex(e => e.RefSeqId);

                entity.HasIndex(e => e.WarehouseId);

                entity.Property(e => e.RecInvoiceId).HasColumnName("RecInvoiceID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Coaid)
                    .HasColumnName("COAID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullCOAID'))");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.Duty1ChgId).HasColumnName("Duty1ChgID");

                entity.Property(e => e.Duty2ChgId).HasColumnName("Duty2ChgID");

                entity.Property(e => e.FreightChgId).HasColumnName("FreightChgID");

                entity.Property(e => e.Insur1ChgId).HasColumnName("Insur1ChgID");

                entity.Property(e => e.Insur2ChgId).HasColumnName("Insur2ChgID");

                entity.Property(e => e.LotId)
                    .HasColumnName("LotID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullLotID'))");

                entity.Property(e => e.Oth1ChgId).HasColumnName("Oth1ChgID");

                entity.Property(e => e.Oth2ChgId).HasColumnName("Oth2ChgID");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RefId)
                    .HasColumnName("RefID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRefID'))");

                entity.Property(e => e.RefSeqId)
                    .HasColumnName("RefSeqID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldGUID]('NullRefSeqID'))");

                entity.Property(e => e.RipAddChargeAmt)
                    .HasColumnName("ripAddChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipAdjAmt)
                    .HasColumnName("ripAdjAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipAmount)
                    .HasColumnName("ripAmount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipBaseQuantity)
                    .HasColumnName("ripBaseQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipChargeAmt)
                    .HasColumnName("ripChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDiscAmt)
                    .HasColumnName("ripDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDiscFml)
                    .HasColumnName("ripDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.RipDiscFmlAmt)
                    .HasColumnName("ripDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDiscFmlDisp)
                    .HasColumnName("ripDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RipDiscPct)
                    .HasColumnName("ripDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDiscPctAmt)
                    .HasColumnName("ripDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDiscount)
                    .HasColumnName("ripDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDuty1ChgAmt)
                    .HasColumnName("ripDuty1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipDuty2ChgAmt)
                    .HasColumnName("ripDuty2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipExtField1)
                    .HasColumnName("ripExtField1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RipExtField2)
                    .HasColumnName("ripExtField2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RipExtField3)
                    .HasColumnName("ripExtField3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RipExtField4)
                    .HasColumnName("ripExtField4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RipExtField5)
                    .HasColumnName("ripExtField5")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RipFreightChgAmt)
                    .HasColumnName("ripFreightChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipGrossAmt)
                    .HasColumnName("ripGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipInsur1ChgAmt)
                    .HasColumnName("ripInsur1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipInsur2ChgAmt)
                    .HasColumnName("ripInsur2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipNote)
                    .HasColumnName("ripNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.RipOth1ChgAmt)
                    .HasColumnName("ripOth1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipOth2ChgAmt)
                    .HasColumnName("ripOth2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipPortion)
                    .HasColumnName("ripPortion")
                    .HasColumnType("decimal(5, 4)")
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.RipPrice)
                    .HasColumnName("ripPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipQuantity)
                    .HasColumnName("ripQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipSeq).HasColumnName("ripSeq");

                entity.Property(e => e.RipTax1ChgAmt)
                    .HasColumnName("ripTax1ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipTax2ChgAmt)
                    .HasColumnName("ripTax2ChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipTaxAmt)
                    .HasColumnName("ripTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RipTotChgAmt)
                    .HasColumnName("ripTotChgAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasComputedColumnSql("(convert(decimal(24,6),([ripFreightChgAmt] + [ripInsur1ChgAmt] + [ripInsur2ChgAmt] + [ripDuty1ChgAmt] + [ripDuty2ChgAmt] + [ripTax1ChgAmt] + [ripTax2ChgAmt] + [ripOth1ChgAmt] + [ripOth2ChgAmt])))");

                entity.Property(e => e.RipViwPartName)
                    .HasColumnName("ripViwPartName")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetRecivoPartName]([PartID]))");

                entity.Property(e => e.RoutingId)
                    .HasColumnName("RoutingID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRouting'))");

                entity.Property(e => e.Tax1ChgId).HasColumnName("Tax1ChgID");

                entity.Property(e => e.Tax2ChgId).HasColumnName("Tax2ChgID");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.Property(e => e.UweightId).HasColumnName("UweightID");

                entity.Property(e => e.WarehouseId)
                    .HasColumnName("WarehouseID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('WarehouseID'))");
            });

            modelBuilder.Entity<TblSalesQuo>(entity =>
            {
                entity.HasKey(e => e.SalesQuoId);

                entity.ToTable("tblSalesQuo");

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.SalesQuoCd);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TaxId);

                entity.HasIndex(e => e.VendorId);

                entity.Property(e => e.SalesQuoId).HasColumnName("SalesQuoID");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasDefaultValueSql("create default [defBranchID] as 0");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),(30)))");

                entity.Property(e => e.ConfirmCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmTime).HasColumnType("datetime");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('EmployeeID'))");

                entity.Property(e => e.EntryCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryTime).HasColumnType("datetime");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),(30)))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RunCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunTime).HasColumnType("datetime");

                entity.Property(e => e.SalesQuoCd)
                    .IsRequired()
                    .HasColumnName("SalesQuoCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.SqtAttn)
                    .HasColumnName("sqtAttn")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SqtCc)
                    .HasColumnName("sqtCc")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SqtChargeAmt)
                    .HasColumnName("sqtChargeAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtDate)
                    .HasColumnName("sqtDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.SqtDisableTrig).HasColumnName("sqtDisableTrig");

                entity.Property(e => e.SqtDiscAmt)
                    .HasColumnName("sqtDiscAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtDiscFml)
                    .HasColumnName("sqtDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SqtDiscFmlAmt)
                    .HasColumnName("sqtDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtDiscFmlDisp)
                    .HasColumnName("sqtDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SqtDiscPct)
                    .HasColumnName("sqtDiscPct")
                    .HasColumnType("decimal(9, 8)");

                entity.Property(e => e.SqtDiscPctAmt)
                    .HasColumnName("sqtDiscPctAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtDiscount)
                    .HasColumnName("sqtDiscount")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtDocDate1)
                    .HasColumnName("sqtDocDate1")
                    .HasColumnType("datetime");

                entity.Property(e => e.SqtDocDate2)
                    .HasColumnName("sqtDocDate2")
                    .HasColumnType("datetime");

                entity.Property(e => e.SqtDocDate3)
                    .HasColumnName("sqtDocDate3")
                    .HasColumnType("datetime");

                entity.Property(e => e.SqtDocNo1)
                    .HasColumnName("sqtDocNo1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SqtDocNo2)
                    .HasColumnName("sqtDocNo2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SqtDocNo3)
                    .HasColumnName("sqtDocNo3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SqtGrossAmt)
                    .HasColumnName("sqtGrossAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtLeadTime)
                    .HasColumnName("sqtLeadTime")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SqtLeadTimeType).HasColumnName("sqtLeadTimeType");

                entity.Property(e => e.SqtNetAmt)
                    .HasColumnName("sqtNetAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtNote)
                    .HasColumnName("sqtNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SqtTaxAmt)
                    .HasColumnName("sqtTaxAmt")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtTaxAmt1st)
                    .HasColumnName("sqtTaxAmt1st")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtTaxAmt2nd)
                    .HasColumnName("sqtTaxAmt2nd")
                    .HasColumnType("decimal(24, 6)");

                entity.Property(e => e.SqtValidity)
                    .HasColumnName("sqtValidity")
                    .HasMaxLength(20);

                entity.Property(e => e.SqtValidityType).HasColumnName("sqtValidityType");

                entity.Property(e => e.SqtWarranty)
                    .HasColumnName("sqtWarranty")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.VerifyCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblSalesQuoPart>(entity =>
            {
                entity.HasKey(e => new { e.SalesQuoId, e.SeqId });

                entity.ToTable("tblSalesQuoPart");

                entity.HasIndex(e => e.GroupingId);

                entity.HasIndex(e => e.PartId);

                entity.HasIndex(e => e.UnitId);

                entity.Property(e => e.SalesQuoId).HasColumnName("SalesQuoID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.GroupingId).HasColumnName("GroupingID");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.SqpAmount)
                    .HasColumnName("sqpAmount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpDiscAmt)
                    .HasColumnName("sqpDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpDiscFml)
                    .HasColumnName("sqpDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SqpDiscFmlAmt)
                    .HasColumnName("sqpDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpDiscFmlDisp)
                    .HasColumnName("sqpDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SqpDiscPct)
                    .HasColumnName("sqpDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpDiscPctAmt)
                    .HasColumnName("sqpDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpDiscount)
                    .HasColumnName("sqpDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpGrossAmt)
                    .HasColumnName("sqpGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpName)
                    .IsRequired()
                    .HasColumnName("sqpName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SqpNote)
                    .HasColumnName("sqpNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SqpPrice)
                    .HasColumnName("sqpPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpQuantity)
                    .HasColumnName("sqpQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpReason)
                    .HasColumnName("sqpReason")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SqpSeq).HasColumnName("sqpSeq");

                entity.Property(e => e.SqpStatus)
                    .HasColumnName("sqpStatus")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SqpTaxAmt)
                    .HasColumnName("sqpTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");
            });

            modelBuilder.Entity<TblSo>(entity =>
            {
                entity.HasKey(e => e.Soid);

                entity.ToTable("tblSO");

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.DivisionId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.ProjectId);

                entity.HasIndex(e => e.Socd);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TaxId);

                entity.HasIndex(e => e.VendorId);

                entity.Property(e => e.Soid).HasColumnName("SOID");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasDefaultValueSql("create default [defBranchID] as 0");

                entity.Property(e => e.Coaid).HasColumnName("COAID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.ConfirmCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ConfirmTime).HasColumnType("datetime");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.EntryCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EntryTime).HasColumnType("datetime");

                entity.Property(e => e.JournalTypeId).HasColumnName("JournalTypeID");

                entity.Property(e => e.LastStatusId).HasColumnName("LastStatusID");

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RunCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RunTime).HasColumnType("datetime");

                entity.Property(e => e.ShipAgentId)
                    .HasColumnName("ShipAgentID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ShipAgentID'))");

                entity.Property(e => e.SoChargeAmt)
                    .HasColumnName("soChargeAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDate)
                    .HasColumnName("soDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.SoDeliverTo)
                    .HasColumnName("soDeliverTo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SoDisableTrig)
                    .IsRequired()
                    .HasColumnName("soDisableTrig")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDiscAmt)
                    .HasColumnName("soDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDiscFml)
                    .HasColumnName("soDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SoDiscFmlAmt)
                    .HasColumnName("soDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDiscFmlDisp)
                    .HasColumnName("soDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SoDiscPct)
                    .HasColumnName("soDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDiscPctAmt)
                    .HasColumnName("soDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDiscount)
                    .HasColumnName("soDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoDocDate1)
                    .HasColumnName("soDocDate1")
                    .HasColumnType("datetime");

                entity.Property(e => e.SoDocDate2)
                    .HasColumnName("soDocDate2")
                    .HasColumnType("datetime");

                entity.Property(e => e.SoDocDate3)
                    .HasColumnName("soDocDate3")
                    .HasColumnType("datetime");

                entity.Property(e => e.SoDocNo1)
                    .HasColumnName("soDocNo1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoDocNo2)
                    .HasColumnName("soDocNo2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoDocNo3)
                    .HasColumnName("soDocNo3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoGrossAmt)
                    .HasColumnName("soGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoNetAmt)
                    .HasColumnName("soNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoNote)
                    .HasColumnName("soNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SoPo)
                    .HasColumnName("soPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoPodate)
                    .HasColumnName("soPODate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.SoPrepaidAmt)
                    .HasColumnName("soPrepaidAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoReqDate)
                    .HasColumnName("soReqDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.SoReqDesc)
                    .HasColumnName("soReqDesc")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SoRequestor)
                    .HasColumnName("soRequestor")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SoTaxAmt)
                    .HasColumnName("soTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoTaxAmt1st)
                    .HasColumnName("soTaxAmt1st")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoTaxAmt2nd)
                    .HasColumnName("soTaxAmt2nd")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SoTotNetAmt)
                    .HasColumnName("soTotNetAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.Socd)
                    .IsRequired()
                    .HasColumnName("SOCD")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.VerifyCompName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblSopart>(entity =>
            {
                entity.HasKey(e => new { e.Soid, e.SeqId });

                entity.ToTable("tblSOPart");

                entity.HasIndex(e => e.PartId);

                entity.HasIndex(e => e.RefId);

                entity.HasIndex(e => e.RefSeqId);

                entity.HasIndex(e => e.UnitId);

                entity.Property(e => e.Soid).HasColumnName("SOID");

                entity.Property(e => e.SeqId)
                    .HasColumnName("SeqID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Coaid).HasColumnName("COAID");

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.LotId)
                    .HasColumnName("LotID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullLotID'))");

                entity.Property(e => e.PartId).HasColumnName("PartID");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.RefId)
                    .HasColumnName("RefID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRefID'))");

                entity.Property(e => e.RefSeqId)
                    .HasColumnName("RefSeqID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldGUID]('NullRefSeqID'))");

                entity.Property(e => e.RoutingId)
                    .HasColumnName("RoutingID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullRouting'))");

                entity.Property(e => e.SopAmount)
                    .HasColumnName("sopAmount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopDiscAmt)
                    .HasColumnName("sopDiscAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopDiscFml)
                    .HasColumnName("sopDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SopDiscFmlAmt)
                    .HasColumnName("sopDiscFmlAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopDiscFmlDisp)
                    .HasColumnName("sopDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SopDiscPct)
                    .HasColumnName("sopDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopDiscPctAmt)
                    .HasColumnName("sopDiscPctAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopDiscount)
                    .HasColumnName("sopDiscount")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopGrossAmt)
                    .HasColumnName("sopGrossAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopNote)
                    .HasColumnName("sopNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.SopPrice)
                    .HasColumnName("sopPrice")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopQuantity)
                    .HasColumnName("sopQuantity")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopReqDate)
                    .HasColumnName("sopReqDate")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("([dbo].[sfnGetDate](getdate()))");

                entity.Property(e => e.SopSeq).HasColumnName("sopSeq");

                entity.Property(e => e.SopTaxAmt)
                    .HasColumnName("sopTaxAmt")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.SopUsrLog)
                    .HasColumnName("sopUsrLog")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoviwPartName)
                    .HasColumnName("SOViwPartName")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([dbo].[sfnGetSOPartName]([PartID]))");

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.Property(e => e.UweightId)
                    .HasColumnName("UweightID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('NullUWeightID'))");
            });

            modelBuilder.Entity<TblStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("tblStatus");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.StaName)
                    .IsRequired()
                    .HasColumnName("staName")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StaPrint).HasColumnName("staPrint");
            });

            modelBuilder.Entity<TblVendor>(entity =>
            {
                entity.HasKey(e => e.VendorId);

                entity.ToTable("tblVendor");

                entity.HasIndex(e => e.CityId);

                entity.HasIndex(e => e.CountryId);

                entity.HasIndex(e => e.CurrencyId);

                entity.HasIndex(e => e.EmployeeId);

                entity.HasIndex(e => e.ProvinceId);

                entity.HasIndex(e => e.VendorCd);

                entity.HasIndex(e => e.ZoneId);

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.BankId).HasColumnName("BankID");

                entity.Property(e => e.CityId)
                    .HasColumnName("CityID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CityID'))");

                entity.Property(e => e.Computer)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(host_name(),30))");

                entity.Property(e => e.CountryId)
                    .HasColumnName("CountryID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CountryID'))");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('CurrencyID'))");

                entity.Property(e => e.Deleted)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LastTimeStamp).IsRowVersion();

                entity.Property(e => e.LastUpdate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(left(suser_sname(),30))");

                entity.Property(e => e.PmtTypeId).HasColumnName("PmtTypeID");

                entity.Property(e => e.PriceCatId).HasColumnName("PriceCatID");

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("ProvinceID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ProvinceID'))");

                entity.Property(e => e.ShipAgentId).HasColumnName("ShipAgentID");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TaxID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TaxID'))");

                entity.Property(e => e.TermDelId)
                    .HasColumnName("TermDelID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermDelID'))");

                entity.Property(e => e.TermPmtId)
                    .HasColumnName("TermPmtID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('TermPmtID'))");

                entity.Property(e => e.VendorCd)
                    .IsRequired()
                    .HasColumnName("VendorCD")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"
create default [defCode] as ' '
");

                entity.Property(e => e.VndAddress)
                    .IsRequired()
                    .HasColumnName("vndAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndAttn)
                    .HasColumnName("vndAttn")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndBankAccNo)
                    .HasColumnName("vndBankAccNo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndBillAddress)
                    .HasColumnName("vndBillAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndBitCodePrefix)
                    .HasColumnName("vndBitCodePrefix")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.VndCatId1).HasColumnName("vndCatID1");

                entity.Property(e => e.VndCatId2).HasColumnName("vndCatID2");

                entity.Property(e => e.VndCatId3).HasColumnName("vndCatID3");

                entity.Property(e => e.VndCatId4).HasColumnName("VndCatID4");

                entity.Property(e => e.VndCatId5).HasColumnName("VndCatID5");

                entity.Property(e => e.VndCc)
                    .HasColumnName("vndCc")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndContact)
                    .HasColumnName("vndContact")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndCreditLimit)
                    .HasColumnName("vndCreditLimit")
                    .HasColumnType("decimal(24, 6)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndCreditLimitType)
                    .HasColumnName("vndCreditLimitType")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndCreditTerm)
                    .IsRequired()
                    .HasColumnName("vndCreditTerm")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndDiscFml)
                    .HasColumnName("vndDiscFml")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.VndDiscFmlDisp)
                    .HasColumnName("vndDiscFmlDisp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndDiscPct)
                    .HasColumnName("vndDiscPct")
                    .HasColumnType("decimal(9, 8)")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndDiscontinue)
                    .IsRequired()
                    .HasColumnName("vndDiscontinue")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndDob)
                    .HasColumnName("vndDOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.VndDobalert)
                    .HasColumnName("vndDOBAlert")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndEmail)
                    .HasColumnName("vndEmail")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndExtField1)
                    .HasColumnName("vndExtField1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndExtField2)
                    .HasColumnName("vndExtField2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndExtField3)
                    .HasColumnName("vndExtField3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndExtField4)
                    .HasColumnName("vndExtField4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndExtField5)
                    .HasColumnName("vndExtField5")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndFax)
                    .HasColumnName("vndFax")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndIdentityNo)
                    .HasColumnName("vndIdentityNo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndIsWapu)
                    .IsRequired()
                    .HasColumnName("vndIsWapu")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndMailAddress)
                    .HasColumnName("vndMailAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndMemberType).HasColumnName("vndMemberType");

                entity.Property(e => e.VndMobile)
                    .HasColumnName("vndMobile")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VndName)
                    .IsRequired()
                    .HasColumnName("vndName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndNote)
                    .HasColumnName("vndNote")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.VndNotifyByEmail)
                    .HasColumnName("vndNotifyByEmail")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndNpp)
                    .HasColumnName("vndNPP")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndNpwp)
                    .HasColumnName("vndNPWP")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndOccupation)
                    .HasColumnName("vndOccupation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndOrderAddress)
                    .HasColumnName("vndOrderAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndPartLimit)
                    .IsRequired()
                    .HasColumnName("vndPartLimit")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndPassword)
                    .HasColumnName("vndPassword")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndPhZone)
                    .HasColumnName("vndPhZone")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.VndPhone)
                    .HasColumnName("vndPhone")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VndPhoto)
                    .HasColumnName("vndPhoto")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndPlaceBirth)
                    .HasColumnName("vndPlaceBirth")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VndPurchAutoMarkup)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndQuoteAddress)
                    .HasColumnName("vndQuoteAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndRating)
                    .HasColumnName("vndRating")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndSalesPriceControl)
                    .HasColumnName("vndSalesPriceControl")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.VndSex).HasColumnName("vndSex");

                entity.Property(e => e.VndShipAddress)
                    .HasColumnName("vndShipAddress")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VndTypeId)
                    .HasColumnName("VndTypeID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('VndTypeID'))");

                entity.Property(e => e.VndViewName)
                    .IsRequired()
                    .HasColumnName("vndViewName")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(convert(varchar(50),[vndName]),''))");

                entity.Property(e => e.VndWebSite)
                    .HasColumnName("vndWebSite")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VndZip)
                    .HasColumnName("vndZip")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.ZoneId)
                    .HasColumnName("ZoneID")
                    .HasDefaultValueSql("([dbo].[sfnGetSetupFieldValue]('ZoneID'))");
            });
        }
    }
}
