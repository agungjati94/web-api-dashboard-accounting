﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblSopart
    {
        public long Soid { get; set; }
        public Guid SeqId { get; set; }
        public int? SopSeq { get; set; }
        public long RefId { get; set; }
        public Guid RefSeqId { get; set; }
        public long LotId { get; set; }
        public int PartId { get; set; }
        public int? Coaid { get; set; }
        public short UnitId { get; set; }
        public int RoutingId { get; set; }
        public DateTime SopReqDate { get; set; }
        public decimal SopQuantity { get; set; }
        public decimal SopPrice { get; set; }
        public decimal SopDiscPct { get; set; }
        public decimal SopDiscPctAmt { get; set; }
        public decimal SopDiscAmt { get; set; }
        public string SopDiscFml { get; set; }
        public string SopDiscFmlDisp { get; set; }
        public decimal SopDiscFmlAmt { get; set; }
        public decimal SopTaxAmt { get; set; }
        public decimal SopDiscount { get; set; }
        public decimal SopAmount { get; set; }
        public decimal SopGrossAmt { get; set; }
        public string SopNote { get; set; }
        public string SopUsrLog { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
        public long? UweightId { get; set; }
        public string SoviwPartName { get; set; }
    }
}
