﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPopart
    {
        public long Poid { get; set; }
        public Guid SeqId { get; set; }
        public int? PopSeq { get; set; }
        public long RefId { get; set; }
        public Guid RefSeqId { get; set; }
        public long LotId { get; set; }
        public int PartId { get; set; }
        public int? Coaid { get; set; }
        public short UnitId { get; set; }
        public int RoutingId { get; set; }
        public DateTime PopReqDate { get; set; }
        public decimal PopQuantity { get; set; }
        public decimal PopPrice { get; set; }
        public string PopExtField1 { get; set; }
        public string PopExtField2 { get; set; }
        public string PopExtField3 { get; set; }
        public string PopExtField4 { get; set; }
        public string PopExtField5 { get; set; }
        public decimal PopDiscPct { get; set; }
        public decimal PopDiscPctAmt { get; set; }
        public decimal PopDiscAmt { get; set; }
        public string PopDiscFml { get; set; }
        public string PopDiscFmlDisp { get; set; }
        public decimal PopDiscFmlAmt { get; set; }
        public decimal PopTaxAmt { get; set; }
        public decimal PopDiscount { get; set; }
        public decimal PopAmount { get; set; }
        public decimal PopGrossAmt { get; set; }
        public short? FreightChgId { get; set; }
        public decimal PopFreightChgAmt { get; set; }
        public short? Insur1ChgId { get; set; }
        public decimal PopInsur1ChgAmt { get; set; }
        public short? Insur2ChgId { get; set; }
        public decimal PopInsur2ChgAmt { get; set; }
        public short? Duty1ChgId { get; set; }
        public decimal PopDuty1ChgAmt { get; set; }
        public short? Duty2ChgId { get; set; }
        public decimal PopDuty2ChgAmt { get; set; }
        public short? Tax1ChgId { get; set; }
        public decimal PopTax1ChgAmt { get; set; }
        public short? Tax2ChgId { get; set; }
        public decimal PopTax2ChgAmt { get; set; }
        public short? Oth1ChgId { get; set; }
        public decimal PopOth1ChgAmt { get; set; }
        public short? Oth2ChgId { get; set; }
        public decimal PopOth2ChgAmt { get; set; }
        public decimal? PopTotChgAmt { get; set; }
        public string PopNote { get; set; }
        public string PopName { get; set; }
        public long? PrrefId { get; set; }
        public Guid? PrrefSeqId { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
        public long? UweightId { get; set; }
        public string PoViwPartName { get; set; }
    }
}
