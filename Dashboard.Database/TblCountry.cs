﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblCountry
    {
        public short CountryId { get; set; }
        public string CountryCd { get; set; }
        public string CtrName { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string CtrViewName { get; set; }
    }
}
