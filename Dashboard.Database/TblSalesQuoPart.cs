﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblSalesQuoPart
    {
        public long SalesQuoId { get; set; }
        public Guid SeqId { get; set; }
        public short? SqpSeq { get; set; }
        public int PartId { get; set; }
        public string SqpName { get; set; }
        public short GroupingId { get; set; }
        public short UnitId { get; set; }
        public decimal SqpQuantity { get; set; }
        public decimal SqpPrice { get; set; }
        public decimal SqpDiscPct { get; set; }
        public decimal SqpDiscPctAmt { get; set; }
        public decimal SqpDiscAmt { get; set; }
        public string SqpDiscFml { get; set; }
        public string SqpDiscFmlDisp { get; set; }
        public decimal SqpDiscFmlAmt { get; set; }
        public decimal SqpTaxAmt { get; set; }
        public decimal SqpDiscount { get; set; }
        public decimal SqpAmount { get; set; }
        public decimal SqpGrossAmt { get; set; }
        public byte SqpStatus { get; set; }
        public string SqpReason { get; set; }
        public string SqpNote { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
    }
}
