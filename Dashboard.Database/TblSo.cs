﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblSo
    {
        public long Soid { get; set; }
        public string Socd { get; set; }
        public short? JournalTypeId { get; set; }
        public short BranchId { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int? DepartmentId { get; set; }
        public int VendorId { get; set; }
        public int? Coaid { get; set; }
        public int EmployeeId { get; set; }
        public short CurrencyId { get; set; }
        public short TermPmtId { get; set; }
        public short TermDelId { get; set; }
        public short ShipAgentId { get; set; }
        public string SoDeliverTo { get; set; }
        public string SoPo { get; set; }
        public DateTime? SoPodate { get; set; }
        public string SoRequestor { get; set; }
        public DateTime SoDate { get; set; }
        public DateTime SoReqDate { get; set; }
        public string SoReqDesc { get; set; }
        public short TaxId { get; set; }
        public decimal SoGrossAmt { get; set; }
        public decimal SoDiscPct { get; set; }
        public decimal SoDiscPctAmt { get; set; }
        public decimal SoDiscAmt { get; set; }
        public string SoDiscFml { get; set; }
        public string SoDiscFmlDisp { get; set; }
        public decimal SoDiscFmlAmt { get; set; }
        public decimal SoDiscount { get; set; }
        public decimal SoTaxAmt1st { get; set; }
        public decimal SoTaxAmt2nd { get; set; }
        public decimal SoTaxAmt { get; set; }
        public decimal SoChargeAmt { get; set; }
        public decimal SoPrepaidAmt { get; set; }
        public decimal SoNetAmt { get; set; }
        public decimal SoTotNetAmt { get; set; }
        public string SoNote { get; set; }
        public string SoDocNo1 { get; set; }
        public string SoDocNo2 { get; set; }
        public string SoDocNo3 { get; set; }
        public DateTime? SoDocDate1 { get; set; }
        public DateTime? SoDocDate2 { get; set; }
        public DateTime? SoDocDate3 { get; set; }
        public bool? SoDisableTrig { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public string EntryName { get; set; }
        public string ConfirmName { get; set; }
        public string VerifyName { get; set; }
        public string RunName { get; set; }
        public DateTime? EntryTime { get; set; }
        public DateTime? ConfirmTime { get; set; }
        public DateTime? VerifyTime { get; set; }
        public DateTime? RunTime { get; set; }
        public string EntryCompName { get; set; }
        public string ConfirmCompName { get; set; }
        public string VerifyCompName { get; set; }
        public string RunCompName { get; set; }
    }
}
