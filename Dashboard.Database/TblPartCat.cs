﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPartCat
    {
        public int PartCatId { get; set; }
        public string PartCatCd { get; set; }
        public string PacName { get; set; }
        public bool? PacCat1 { get; set; }
        public bool? PacCat2 { get; set; }
        public bool? PacCat3 { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool? Deleted { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public string PacViewName { get; set; }
    }
}
