﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblRecInvoice
    {
        public long RecInvoiceId { get; set; }
        public short BranchId { get; set; }
        public string RecInvoiceCd { get; set; }
        public short? JournalTypeId { get; set; }
        public string JournalCd { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int DepartmentId { get; set; }
        public int VendorId { get; set; }
        public int BillVendorId { get; set; }
        public int Coaid { get; set; }
        public int EmployeeId { get; set; }
        public short CurrencyId { get; set; }
        public short TermPmtId { get; set; }
        public short TermDelId { get; set; }
        public short ShipAgentId { get; set; }
        public string RivDeliverTo { get; set; }
        public string RivContact { get; set; }
        public DateTime RivDate { get; set; }
        public string RivDocument { get; set; }
        public DateTime RivDocDate { get; set; }
        public DateTime? RivDueDate { get; set; }
        public DateTime? RivEarlyDiscDate { get; set; }
        public decimal RivEarlyDiscPct { get; set; }
        public decimal RivEarlyDiscAmt { get; set; }
        public decimal RivGrossAmt { get; set; }
        public string RivTaxInvoiceNo { get; set; }
        public DateTime? RivTaxInvoiceDate { get; set; }
        public decimal RivDiscPct { get; set; }
        public decimal RivDiscPctAmt { get; set; }
        public decimal RivDiscAmt { get; set; }
        public string RivDiscFml { get; set; }
        public string RivDiscFmlDisp { get; set; }
        public decimal RivDiscFmlAmt { get; set; }
        public decimal RivDiscount { get; set; }
        public decimal RivTaxAmt1st { get; set; }
        public decimal RivTaxAmt2nd { get; set; }
        public decimal RivTaxAmt { get; set; }
        public decimal RivChargeAmt { get; set; }
        public decimal RivPrepaidAmt { get; set; }
        public decimal RivNetAmt { get; set; }
        public decimal RivTotNetAmt { get; set; }
        public short TaxId { get; set; }
        public string RivNote { get; set; }
        public string RivDocNo1 { get; set; }
        public string RivDocNo2 { get; set; }
        public string RivDocNo3 { get; set; }
        public DateTime? RivDocDate1 { get; set; }
        public DateTime? RivDocDate2 { get; set; }
        public DateTime? RivDocDate3 { get; set; }
        public bool? RivDisableTrig { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public bool? Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public short? PphId { get; set; }
        public decimal? RivPphAmt { get; set; }
        public decimal? RivPphAmt1st { get; set; }
        public decimal? RivPphAmt2nd { get; set; }
    }
}
