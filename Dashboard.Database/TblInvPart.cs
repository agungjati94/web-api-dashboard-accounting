﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblInvPart
    {
        public long InventoryId { get; set; }
        public Guid SeqId { get; set; }
        public int? InpSeq { get; set; }
        public long RefId { get; set; }
        public Guid RefSeqId { get; set; }
        public long SubId { get; set; }
        public Guid SubSeqId { get; set; }
        public long LotId { get; set; }
        public long BatchId { get; set; }
        public int PartId { get; set; }
        public short UnitId { get; set; }
        public int RoutingId { get; set; }
        public short WarehouseId { get; set; }
        public int Coaid { get; set; }
        public decimal InpQuantity { get; set; }
        public decimal InpBaseQuantity { get; set; }
        public decimal InpPrice { get; set; }
        public decimal InpDiscPct { get; set; }
        public decimal InpDiscPctAmt { get; set; }
        public decimal InpDiscAmt { get; set; }
        public string InpDiscFml { get; set; }
        public string InpDiscFmlDisp { get; set; }
        public decimal InpDiscFmlAmt { get; set; }
        public decimal InpTaxAmt { get; set; }
        public decimal InpDiscount { get; set; }
        public decimal InpChargeAmt { get; set; }
        public decimal InpAddChargeAmt { get; set; }
        public decimal InpPortion { get; set; }
        public decimal InpAdjAmt { get; set; }
        public decimal InpAmount { get; set; }
        public decimal InpGrossAmt { get; set; }
        public string InpExtField1 { get; set; }
        public string InpExtField2 { get; set; }
        public string InpExtField3 { get; set; }
        public string InpExtField4 { get; set; }
        public string InpExtField5 { get; set; }
        public string InpExtField6 { get; set; }
        public string InpExtField7 { get; set; }
        public string InpExtField8 { get; set; }
        public string InpExtField9 { get; set; }
        public string InpExtField10 { get; set; }
        public string InpNote { get; set; }
        public short? InpDivisionSub { get; set; }
        public int? DivisionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ProjectId { get; set; }
        public long? UweightId { get; set; }
        public long? FragmenPartLot { get; set; }
        public short? MachineId { get; set; }
        public string InvViwPartName { get; set; }
    }
}
