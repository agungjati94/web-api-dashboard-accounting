﻿using System;
using System.Collections.Generic;

namespace Dashboard.Database
{
    public partial class TblPurchReq
    {
        public long PurchReqId { get; set; }
        public string PurchReqCd { get; set; }
        public short CompanyId { get; set; }
        public DateTime PrqDate { get; set; }
        public int EmployeeId { get; set; }
        public short DivisionId { get; set; }
        public long ProjectId { get; set; }
        public int DepartmentId { get; set; }
        public string PrqPurpose { get; set; }
        public byte PrqPriority { get; set; }
        public DateTime PrqReqDate { get; set; }
        public string PrqReqDesc { get; set; }
        public string PrqNote { get; set; }
        public short CurrencyId { get; set; }
        public short? LastStatusId { get; set; }
        public short StatusId { get; set; }
        public bool? Deleted { get; set; }
        public string Operator { get; set; }
        public string Computer { get; set; }
        public DateTime LastUpdate { get; set; }
        public byte[] LastTimeStamp { get; set; }
        public short? PotypeId { get; set; }
        public int? VendorId { get; set; }
        public string EntryName { get; set; }
        public string ConfirmName { get; set; }
        public string VerifyName { get; set; }
        public string RunName { get; set; }
        public DateTime? EntryTime { get; set; }
        public DateTime? ConfirmTime { get; set; }
        public DateTime? VerifyTime { get; set; }
        public DateTime? RunTime { get; set; }
        public string EntryCompName { get; set; }
        public string ConfirmCompName { get; set; }
        public string VerifyCompName { get; set; }
        public string RunCompName { get; set; }
    }
}
